<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * VehicleSearch represents the model behind the search form of `common\models\Vehicle`.
 */
class SupportTicketsSearch extends SupportTickets
{
    public function rules()
    {
        return [
            [["ticket_title", "ticket_dept"], "string", "max" => 255]
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $queryGrp = SupportTickets::find();
        $ids = $queryGrp->select(['MAX(id) id'])->groupBy(['created_by', 'ticket_dept'])->all();
        $idsData = [];
        array_walk($ids, function($value) use (&$idsData){
            $idsData[] = $value->id;
        });

        $query = SupportTickets::find();
        $query->select([
            'id',
            'ticket_title',
            'ticket_dept',
            'ticket_description',
            'ticket_response',
            'ticket_status',
            'created_by',
            'action_by',
            'action_time',
            'createdOn',
            'modifiedOn'
        ]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }




        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'createdOn' => $this->createdOn,
            'modifiedOn' => $this->modifiedOn,
        ]);

        $query
            ->andFilterWhere(['like', 'ticket_title', $this->ticket_title]);
        $query
            ->andFilterWhere(['in', 'id', $idsData]);




        return $dataProvider;
    }
}
