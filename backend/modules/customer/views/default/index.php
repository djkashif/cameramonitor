<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Dashboard';
?>

<section class="content">


    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">

                <!-- /.box-header -->
                <div class="box-body">
                    <?php $form = ActiveForm::begin(['method' => 'get']); ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?php
                            // DateRangePicker in a dropdown format (uneditable/hidden input) and uses the preset dropdown.
                            echo '<label class="control-label">Date Range</label>';
                            echo '<div class="drp-container">';
                            echo \kartik\daterange\DateRangePicker::widget([
                                'model' => $filterModel,
                                'attribute' => 'date_range',
                                'startAttribute' => 'date_from',
                                'endAttribute' => 'date_to',
                                //'presetDropdown'=>true,
                                'hideInput' => true,
                                'pluginOptions' => [
                                    'locale' => ['format' => 'Y-M-D'],
                                ]
                            ]);
                            echo '</div>';
                            ?>
                        </div>
                        <div class="col-md-8">
                            <?php echo $form->field($filterModel, "view_type")->radioList([
                                'summary' => "Summary",
                                'detailed' => "Detailed",
                            ])->label('Select One'); ?>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= Html::submitButton('Filter', ['class' => 'btn btn-info pull-right']) ?>
                            <a href="<?= \yii\helpers\Url::to('/customer/default/index') ?>"
                               class="btn btn-default pull-right">Reset</a>
                        </div>
                    </div>


                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?= ($filterModel->view_type == 'summary') ? 'Summary Report' : 'Detailed Report' ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                        <?php if ($filterModel->view_type == 'detailed'): ?>
                            <tr>
                                <th>Serve Date</th>
                                <th>Meal Type</th>
                                <th>Meal</th>
                                <th>Nutrition Facts</th>
                            </tr>
                            <?php if (!empty($queryReportGridData)): ?>
                                <?php foreach ($queryReportGridData as $queryReportGridD): ?>
                                    <tr>
                                        <td><?= $queryReportGridD['serve_date'] ?></td>
                                        <td><?= $queryReportGridD['meal_title'] ?></td>
                                        <td><?= $queryReportGridD['product_title'] ?></td>
                                        <td>
                                            <span class="badge bg-aqu">Calories : <?= (int)$queryReportGridD['calories'] ?></span>
                                            <span class="badge bg-red">FAT : <?= (int)$queryReportGridD['fat'] ?></span>
                                            <span class="badge bg-green">CARBOHYDRATES : <?= (int)$queryReportGridD['carbohydrates'] ?></span>
                                            <span class="badge bg-yellow">PROTEINS : <?= (int)$queryReportGridD['protiens'] ?></span>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="100">No record found...</td>
                                </tr>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if ($filterModel->view_type == 'summary'): ?>
                            <tr>
                                <th>Meal Type</th>
                                <th>Nutrition Facts</th>
                            </tr>
                            <?php if (!empty($queryReportGridData)): ?>
                                <?php foreach ($queryReportGridData as $queryReportGridD): ?>
                                    <tr>
                                        <td><?= $queryReportGridD['meal_title'] ?></td>
                                        <td>
                                            <span class="badge bg-aqu">Calories : <?= (int)$queryReportGridD['calories'] ?></span>
                                            <span class="badge bg-red">FAT : <?= (int)$queryReportGridD['fat'] ?></span>
                                            <span class="badge bg-green">CARBOHYDRATES : <?= (int)$queryReportGridD['carbohydrates'] ?></span>
                                            <span class="badge bg-yellow">PROTEINS : <?= (int)$queryReportGridD['protiens'] ?></span>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="100">No record found...</td>
                                </tr>
                            <?php endif; ?>
                        <?php endif; ?>


                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>


    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">CALORIES</span>
                    <span class="info-box-number"><?= number_format($totalEnergyConsumption['calories']) ?>
                        <small></small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">FAT</span>
                    <span class="info-box-number"><?= number_format($totalEnergyConsumption['fat']) ?>
                        <small></small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-gear-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">CARBOHYDRATES</span>
                    <span class="info-box-number"><?= number_format($totalEnergyConsumption['carbohydrates']) ?>
                        <small></small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-gear-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">PROTIENS</span>
                    <span class="info-box-number"><?= number_format($totalEnergyConsumption['protiens']) ?>
                        <small></small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Recap Report</h3>


                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">

                            </p>

                            <div class="chart">
                                <!-- Sales Chart Canvas -->
                                <canvas id="areaChart" style="height: 380px;"></canvas>
                            </div>
                            <!-- /.chart-responsive -->
                        </div>
                        <!-- /.col -->

                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->

                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>


</section>
<?php
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/Chart.js', ['depends' => [\backend\assets\AppAsset::className()]]);

$this->registerJs("
    var areaChartCanvas = $('#areaChart').get(0).getContext('2d');
    var areaChart       = new Chart(areaChartCanvas);
    var areaChartData = {$areaChartData};
    var areaChartOptions = {
      
      showScale               : true,
      
      scaleShowGridLines      : false,
      
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      
      scaleGridLineWidth      : 1,
      
      scaleShowHorizontalLines: true,
      
      scaleShowVerticalLines  : true,
      
      bezierCurve             : true,
      
      bezierCurveTension      : 0.3,
      
      pointDot                : false,
      
      pointDotRadius          : 4,
      
      pointDotStrokeWidth     : 1,
      
      pointHitDetectionRadius : 20,
      
      datasetStroke           : true,
      
      datasetStrokeWidth      : 2,
      
      datasetFill             : false,
      
     legendTemplate          : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
      
      maintainAspectRatio     : true,
      
      responsive              : true
    }
    areaChart.Line(areaChartData, areaChartOptions)
    
");


?>
