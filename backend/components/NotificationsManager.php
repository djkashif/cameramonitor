<?php

namespace backend\components;

use common\models\NotificationLog;
use common\models\Package;

class NotificationsManager
{
    public static $customer = null;
    public static $customerPkg = null;


    public static $placeHolders = [
        '[CustomerName]' => 'getCustomerName',
        '[CustomerCity]' => 'getCustomerCity',
        '[SystemDate]' => 'getSystemDate',
        '[PkgExpiryDate]' => 'getPkgExpiryDate',
        '[PkgExpiryDays]' => 'getPkgExpiryDays',
    ];
    public static $output = null;

    public static function parseTemplate($templateStr, $customer = null)
    {
        self::$customer = $customer;
        self::$customerPkg = self::_getCustomerPkgDetails($customer);

        preg_match_all("/\[[A-Za-z]{0,}\]/", $templateStr, $placeHolders);
        if (isset($placeHolders[0]) && count($placeHolders[0])) {
            $placeHolders = $placeHolders[0];
            $availablePlaceHolders = array_keys(self::$placeHolders);
            $commonPlaceHolders = array_intersect($availablePlaceHolders, $placeHolders);

            $results = [];
            foreach ($commonPlaceHolders as $commonPlaceHolder) {
                self::$output = null;
                $functionName = self::$placeHolders[$commonPlaceHolder] . "";
                $functionNameCmd = "self::" . self::$placeHolders[$commonPlaceHolder] . "();";
                if (!method_exists(get_called_class(), $functionName)) {
                    $results[$commonPlaceHolder] = self::$output;
                } else {
                    eval($functionNameCmd);
                    $results[$commonPlaceHolder] = self::$output;
                }
            }

            foreach ($results as $placeHolderKey => $placeHolderValue) {
                $templateStr = str_replace($placeHolderKey, $placeHolderValue, $templateStr);
            }
        }

        return $templateStr;
    }

    public static function getCustomerName()
    {
        self::$output = trim(self::$customer->user->firstName . " " . self::$customer->user->lastName);
    }

    public static function getCustomerCity()
    {

        self::$output = trim((isset(self::$customer->city->name)) ? self::$customer->city->name : 'N/A');
    }

    private static function getSystemDate()
    {
        self::$output = date('d/m/Y');
    }

    private static function getPkgExpiryDays()
    {

        if (!is_null(self::$customerPkg)) {
            $date1 = date_create(date('Y-m-d'));
            $date2 = date_create(self::$customerPkg->package_expiration);
            $diff = date_diff($date1, $date2);
            $days = $diff->format("%R%a");
            $days = str_replace('+', '', $days);
            $days = str_replace('-', '', $days);
            self::$output = $days;
        }
    }

    private static function getPkgExpiryDate()
    {
        if (!is_null(self::$customerPkg)) {
            self::$output = date('d/m/Y', strtotime(self::$customerPkg->package_expiration));
        }
    }


    private static function _getCustomerPkgDetails($customer)
    {
        $pkgOfTheDay = Package::find()->where([
            'package_status' => 1,
            'customer_id' => $customer->id
        ])
            ->andWhere([
                '<=', 'package_start_date', date('Y-m-d')
            ])
            ->andWhere([
                '>', 'package_expiration', date('Y-m-d')
            ])->one();

        if (!empty($pkgOfTheDay)) {
            return $pkgOfTheDay;
        }
        return null;
    }

    public static function _logNotification($customer_id, $notification, $status, $response)
    {
        $notificationLog = new NotificationLog();
        $notificationLog->customer_id = $customer_id;
        $notificationLog->notification = $notification;
        $notificationLog->status = $status;
        $notificationLog->response = $response;
        $notificationLog->save();
    }


}

