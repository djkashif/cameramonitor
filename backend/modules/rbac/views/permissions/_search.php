<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php $form = ActiveForm::begin([
    "id" => "search-form",
    "action" => ["index"],
    "method" => "get"
]);
?>
<div class="modal fade" id="searchModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><?php echo Yii::t("app", "Advanced Search"); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <?php echo $form->field($searchModel, "name")->textInput(); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo Html::a(Yii::t("app", "Close"), "javascript:;", ["class" => "btn btn-default", "data" => ["dismiss" => "modal"]]); ?>
                <?php echo Html::a(Yii::t("app", "Reset"), Url::to(["/rbac/permissions"]), ["class" => "btn btn-primary"]); ?>
                <?php echo Html::submitButton(Yii::t("app", "Search"), ["class" => "btn btn-success"]); ?>
            </div>
        </div>
    </div>
</div>
<?php Activeform::end(); ?>