(function ($) {
    $.createRandomString = function ($length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < $length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    },

        $.fn.block = function ($options) {
            if ($options === undefined || typeof $options !== "object") {
                $options = {};
            }

            $(this).unblock();

            if ($options.elementOptions === undefined) {
                if ($(this).is("body")) {
                    $(this).css("overflow", "hidden");
                } else {
                    $(this).css("position", "relative");
                }
            } else {
                $(this).css($options.elementOptions);
            }

            var $isWidget = ($options.isWidget === true);
            var $loaderWrapperClass = ($isWidget) ? "loader-wrapper-widget" : "loader-wrapper";

            var $html = "<div class=\"" + $loaderWrapperClass + "\">" +
                "<div class=\"loader\"></div>" +
                "</div>" +
                "<div class=\"loader-overlay\"></div>";

            $(this).append($html);

            if ($(this).is("body")) {
                $(this).find("." + $loaderWrapperClass).css("z-index", 600);
                $(this).find(".loader-overlay").css("z-index", 500);
            } else {
                $(this).find("." + $loaderWrapperClass).css("z-index", 300);
                $(this).find(".loader-overlay").css("z-index", 200);
            }

            if ($options.blockText !== undefined) {
                $(this).find("." + $loaderWrapperClass).css({
                    width: "360px",
                    height: "100px",
                    margin: "-50px 0 0 -180px"
                });

                $(this).find(".loader").css("margin", "0 0 0 160px");
                $(this).find(".loader").after("<div class=\"loading-text\">" + $options.blockText + "</div>");
            }
        },

        $.fn.unblock = function ($options) {
            if ($options === undefined || typeof $options !== "object") {
                $options = {};
            }

            $(this).find(".loader-wrapper").remove();
            $(this).find(".loader-wrapper-widget").remove();
            $(this).find(".loader-overlay").remove();
            $(this).css("overflow", "");
            $(this).css("position", "");
        },

        $.createUrlFromString = function ($text) {
            // trim string
            $text = $text.replace(/^\s+|\s+$/g, "");

            // convert string to lower case
            $text = $text.toLowerCase();

            // replace all spaces with - for url
            $text = $text.replace(/ /g, "-");

            // replace all special characters with empty string for url
            $text = $text.replace(/[^\w-]+/g, "");

            return $text;
        },

        $.processAjaxRequest = function ($options) {
            if ($options === undefined || typeof $options !== "object") {
                console.error("INVALID PROPERTY EXCEPTION: No properties found");
                return;
            }

            var $url = $options.url,
                $type = ($options.type !== undefined) ? $options.type : "post",
                $data = ($options.data !== undefined) ? $options.data : {},
                $csrfParam = yii.getCsrfParam(),
                $csrfToken = yii.getCsrfToken(),
                $ajaxParams = ($options.ajaxParams !== undefined) ? $options.ajaxParams : {};

            if ($url === undefined) {
                console.error("INVALID PROPERTY EXCEPTION: url not found.");
                return;
            }

            if (typeof $data === "object") {
                $data[$csrfParam] = $csrfToken;
            } else {
                $data += "&" + $csrfParam + "=" + $csrfToken;
            }

            var $defaults = {
                url: $url,
                type : $type,
                data: $data,
                cache: false,
                success: function ($response, $textStatus, $xhr) {
                    if ($options.modal !== undefined) {
                        $options.modal.body = $response;
                        $.showModal($options.modal);
                    }

                    if ($options.successcallback !== undefined) {
                        $options.successcallback.call(this, $response, $textStatus, $xhr);
                    }
                },
                error: function ($jqXHR, $textStatus, $errorThrown) {
                    if ($options.errorcallback !== undefined) {
                        $options.errorcallback.call(this, $jqXHR, $textStatus, $errorThrown);
                    }
                },
                complete: function ($jqXHR, $textStatus) {
                    if ($options.completecallback !== undefined) {
                        $options.completecallback.call(this, $jqXHR, $textStatus);
                    }
                }
            };

            $.extend($defaults, $ajaxParams);
            var $xhr = $.ajax($defaults);

            return $xhr;
        };

    $.processModalAjaxRequest = function ($options) {
        $ajaxRequestParams = $options;
        $ajaxRequestParams.ajaxParams = {
            dataType: "html"
        };
        $ajaxRequestParams.modal = ($options.hasOwnProperty("modalConfig")) ? $options.modalConfig : {};

        $.processAjaxRequest($ajaxRequestParams);
    };

    $.showModal = function ($options) {
        if ($options === undefined || typeof $options !== "object") {
            console.error("options object is not defined");
            return;
        }

        var $wrapper = ($options.wrapper !== undefined) ? $options.wrapper : $.createRandomString(10),
            $body = ($options.body !== undefined) ? "<div class=\"modal-section\">" + $options.body + "</div>" : "",
            $type = $options.type;

        $("body").append("<div id=\"" + $wrapper + "\" class=\"modal fade\" role=\"dialog\" data-backdrop=\"static\" data-keyboard=\"false\" style=\"display: none;\"></div>");

        if ($body.search("modal-dialog") === -1) {
            var $dialogClass = ($options.dialogClass !== undefined) ? "modal-dialog " + $options.dialogClass : "modal-dialog",
                $dialogStyle = ($options.dialogStyle !== undefined) ? $options.dialogStyle : "",
                $fullWidth = $options.fullWidth,
                $title = ($options.title !== undefined) ? $options.title : "",
                $buttons = ($options.buttons !== undefined) ? $options.buttons : "";

            if ($fullWidth === true) {
                $dialogStyle += "width: calc(100vw - 60px);";
            }

            $("#" + $wrapper).html("<div class=\"" + $dialogClass + "\" role=\"document\" style=\"" + $dialogStyle + "\">" +
                "<div class=\"modal-content\">" +
                "<div class=\"modal-header\">" +
                "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">" +
                "<span aria-hidden=\"true\">&times;</span>" +
                "</button>" +
                "<h4 class=\"modal-title\">" + $title + "</h4>" +
                "</div>" +
                $body +
                "<div class=\"modal-footer\">" +
                "<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>" +
                $buttons +
                "</div>" +
                "</div>" +
                "</div>");

            $("#" + $wrapper).find(".modal-header").nextUntil($("#" + $wrapper).find(".modal-footer")).wrapAll("<div class=\"modal-body\"></div>");
        } else {
            $("#" + $wrapper).html($body);
        }

        var $backdrop = "static";

        // we will not show fade background if modal is displayed inside another modal
        if ($(".modal").hasClass("in")) {
            $backdrop = false;
            $(".modal.in").css("filter", "blur(5px)");
        }

        if ($type === "success") {
            $("#" + $wrapper).find(".btn-default").addClass("btn-primary").removeClass(".btn-default");
        } else if ($type === "error") {
            $("#" + $wrapper).find(".btn-default").addClass("btn-danger").removeClass(".btn-default");
        }

        var $shownObject = {
            wrapper: $("#" + $wrapper)
        };
        var $returnModal = {};

        // trigger event before the modal is displayed
        $("#" + $wrapper).on("show.bs.modal", function (e) {
            if ($options.beforeShown !== undefined && typeof $options.beforeShown === "function") {
                $options.beforeShown.call(this, $shownObject);
            }

            $returnModal.beforeShownEvent = e;
        });

        $("#" + $wrapper).modal({
            backdrop: $backdrop
        });

        // trigger event after the modal is displayed
        $("#" + $wrapper).on("shown.bs.modal", function (e) {
            if ($options.afterShown !== undefined && typeof $options.afterShown === "function") {
                $options.afterShown.call(this, $shownObject);
            }
        });

        // if modal is closed, then remove the HTML from DOM
        $("#" + $wrapper).on("hide.bs.modal", function () {
            var $modal = $(this);
            $(".modal.in").css("filter", "");

            setTimeout(function () {
                $modal.remove();
            }, 400);
        });

        return $returnModal;
    };
})(jQuery);