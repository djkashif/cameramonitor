var PackageManager = {
    callInProcess: false,
    hot: '',
    scannedInformation: {
        customer: null,
        package: null,
    },
    totalScannedPrice: 0,
    totalScannedCustomers: 0,
    verifiedOrders: [],
    scannedOrders: {},
    selectedMeals: [],
    draftTransactionSaved: null,
    ordersGenerated: null,
    getData: function () {
        return [
            /*{
                customer_id: 10001,
                customer_name: 'Kamran Latif',
                meal_item: 'Test'

            }*/
        ];
    },

    prepareHandsOnTableForNewOrders() {
        var container = document.getElementById('example1');

        this.hot = new Handsontable(container, {
            data: PackageManager.getData(),
            licenseKey: 'non-commercial-and-evaluation',
            colHeaders: ['ID', 'CUSTOMER', 'MEAL ITEMS'],
            //minSpareRows: 1,
            stretchH: 'last',
            rowHeaders: true,

            width: '100%',
            height: 320,
            colWidths: [100, 200, 160],
            //manualColumnResize: true,
            //manualRowResize: true,

            columns: [
                {
                    data: 'customer_id',
                    readOnly: false
                },
                {
                    data: 'customer_name',
                    readOnly: true
                },
                {
                    data: 'meal_item',
                    readOnly: true,
                    renderer: "html"
                },
            ],
            readOnly: true
        });

        //this.hot.populateFromArray(0, 0,[[1,1,1]]);
    },
    prepareHandsOnTableForGeneratedOrders() {
        var container = document.getElementById('example1');

        this.hot = new Handsontable(container, {
            data: PackageManager.getData(),
            licenseKey: 'non-commercial-and-evaluation',
            colHeaders: ['ID', 'CUSTOMER', 'MEAL ITEMS'],
            //minSpareRows: 1,
            stretchH: 'last',
            rowHeaders: true,
            width: '100%',
            height: 320,
            colWidths: [100, 200, 160],
            //manualColumnResize: true,
            //manualRowResize: true,

            columns: [
                {
                    data: 'customer_id',
                    readOnly: false
                },
                {
                    data: 'customer_name',
                    readOnly: true
                },
                {
                    data: 'meal_item',
                    readOnly: true,
                    renderer: "html"
                },
            ],
            readOnly: true
        });

        //this.hot.populateFromArray(0, 0,[[1,1,1]]);
    },

    updateHandsOnTableWidth: function () {
        this.hot.updateSettings({
            stretchH: 'last',
            rowHeaders: true,

            width: '100%',
            height: 320,
            colWidths: [100, 200, 160]
        })
    },

    resetScanForm: function () {
        this.resetCustomerScanFormInput();
        $('input[name="customer_barcode"]').show();
        this.resetItemScanFormInput();
        $('input[name="item_barcode"]').hide();
    },
    resetScanFormInput: function () {
        this.callInProcess = false;
        $('input[name="barcode"]').val('');
    },
    resetCustomerScanFormInput: function () {
        this.callInProcess = false;
        this.scannedInformation.customer = null;
        $('input[name="customer_barcode"]').val('');
    },
    resetPackageScanFormInput: function () {
        this.callInProcess = false;
        this.scannedInformation.package = null;
        $('input[name="package_barcode"]').val('');
    },
    resetItemScanFormInput: function () {
        this.callInProcess = false;
        $('input[name="item_barcode"]').val('');
    },
    toggleScanFormForPackage() {
        this.callInProcess = false;
        $('input[name="customer_barcode"]').hide();
        $('input[name="item_barcode"]').show();
    },

    addNewOrderRow: function () {
        //this.hot.alter('insert_row', this.hot.countRows());
        this.resetScanForm();
    },
    addNewCustomerDataInTable: function () {
        //this.hot.alter('insert_row', this.hot.countRows());

        if (this.scannedInformation.customer !== null) {
            var rowNumber = this.hot.countRows();
            this.hot.setDataAtCell(rowNumber, 0, this.scannedInformation.customer.customer_account);
            this.hot.setDataAtCell(rowNumber, 1, this.scannedInformation.customer.customer_name);
            var self = this;

            if (parseInt(this.scannedInformation.package.hasPkg) && typeof this.scannedInformation.package.meals_list != 'undefined') {
                if (Object.keys(this.scannedInformation.package.meals_list.meals).length > 0) {
                    //if(this.scannedInformation.package.meals_count > 0){
                    var listHtml = "";
                    var priceSum = 0;
                    $.each(this.scannedInformation.package.meals_list.meals, function (k, v) {
                        //listHtml+='<li class="label label-warning" data-item-id="'+v.pid+'">'+v.title+'</li>';
                        if (Object.keys(v.items).length) {
                            $.each(v.items, function (k2, v2) {
                                priceSum += parseInt(v2.price);
                                listHtml += '<li data-verified="0" data-meal-id="' + v.meal_type + '" data-item-id="' + v2.pid + '"><a href="#"><span class="pull-left badge bg-default">' + v.title + '</span> &nbsp;' + v2.title + ' <span class="pull-right badge verification-badge bg-red">Unverified</span></a></li>';
                            });
                        }
                    });
                    listHtml = "<ul data-cst='" + self.scannedInformation.customer.customer_account + "' class='nav nav-stacked gridListItems'>" + listHtml + "</ul>";
                    this.hot.setDataAtCell(rowNumber, 2, listHtml);

                    this.totalScannedPrice += priceSum;
                    this.totalScannedCustomers += 1;
                    this.updateTotals();
                }
            }


            //this.hot.mergeCells = [{row: rowNumber, col: 0, rowspan: 1, colspan: 3} ];
        }

    },
    scanCustomerBarcodeOnKeyEnter: function (eve) {
        if (eve.keyCode != 13) {
            return true;
        }

        console.log('before condition');
        console.log(this.callInProcess);
        if (this.callInProcess) {
            return;
        }
        this.callInProcess = true;
        this.scanBarcodeCustomer();
    },
    scanPackageBarcodeOnKeyEnter: function (eve) {
        if (eve.keyCode != 13) {
            return true;
        }
        this.scanBarcodePackage();
    },

    parseBarCodeData: function (code) {
        var type = code.substr(0, 3);
        var id = code.substr(3);
        return {
            'type': type,
            'id': id
        };
    },
    scanBarcodeCustomer: function () {
        console.log('scanBarcodeCustomer');
        console.log('after condition');
        console.log(this.callInProcess);


        if (!this.selectedMeals.length) {
            swal('', 'Please select meal type.', 'info');
            this.resetScanForm();
            return;
        }
        if (this.hot.countEmptyRows() > 0) {
            swal('', 'Please use existing empty row first', 'info');
            this.resetScanForm();
            return;
        }

        var self = this;
        var code = $('input[name="customer_barcode"]').val()
        if (code.trim() == "") {
            return;
        }

        code = this.parseBarCodeData(code);
        if (typeof code.type == 'undefined' || $.inArray(code.type, ['CST']) == -1) {
            swal('', 'Invalid Code. Scanned barcode should be for customer.', 'error');
            this.resetScanForm();
            return;
        }
        this.showBarcodeScannigLoader();
        code.meals = this.selectedMeals;
        $.ajax({
            url: '/order/get-barcode-data',
            type: 'post',
            data: code,
            success: function (response) {
                self.callInProcess = false;
                self.hideBarcodeScannigLoader();
                //response = $.parseJSON(response);
                if (typeof response.status != 'undefined') {
                    if (response.status == 1) {

                        console.log('^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
                        var existingCustomers = Object.keys(self.scannedOrders);
                        if ($.inArray(response.data.customer_account, existingCustomers) != -1) {
                            swal('', 'Already Scanned Customer', 'error');
                            self.resetScanForm();
                            return;
                        }

                        self.scannedOrders[response.data.customer_account] = response.data;
                        self.scannedInformation.customer = {
                            customer_name: response.data.customer_name,
                            customer_account: response.data.customer_account,
                            customer_address: response.data.customer_address,
                        };
                        self.scannedInformation.package = response.data.package_of_the_day;
                        $('.customer_name').html(response.data.customer_name)
                        $('.customer_account').html(response.data.customer_account);
                        $('.customer_contact').html(response.data.customer_contact);
                        $('.customer_address').html(response.data.customer_address);


                        $('.pkg_of_day').html('N/A');
                        $('div.meal_item_list').html('');
                        if (response.data.package_of_the_day.hasPkg) {
                            $('.pkg_of_day').html(response.data.package_of_the_day.title);
                            $('.pkg_of_day').html(response.data.package_of_the_day.title);

                            var mealListItemsHtml = "";
                            console.log('************************')
                            console.log(response.data.package_of_the_day.meals_list.meals)
                            console.log(Object.keys(response.data.package_of_the_day.meals_list.meals))

                            if (Object.keys(response.data.package_of_the_day.meals_list.meals).length > 0) {
                                $.each(response.data.package_of_the_day.meals_list.meals, function (k, v) {
                                    mealListItemsHtml +=
                                        '<div class="box box-warning box-solid">' +
                                        '            <div class="box-header with-border">' +
                                        '              <h3 class="box-title">' + v.title + '</h3>' +
                                        '            </div>' +
                                        '            <div class="box-body" style="">' +
                                        '              <ul>';
                                    $.each(v.items, function (k2, v2) {
                                        console.log('***********');
                                        console.log(v2);
                                        mealListItemsHtml += '<li>' + v2.title + '</li>';
                                    });
                                    mealListItemsHtml += '</ul>' +
                                        '            </div>' +
                                        '          </div>'


                                });
                                console.log(mealListItemsHtml)

                            }
                            $('div.meal_item_list').html(mealListItemsHtml);

                            /*if(response.data.package_of_the_day.meals_count > 0){
                                $.each(response.data.package_of_the_day.meals_list, function(k, v){
                                    $('.items_list').append('<li data-item-id="'+v.pid+'">'+v.title+'</li>');
                                });
                            }*/
                        }
                        $('div.customer-information').show();


                        self.addNewCustomerDataInTable();
                        self.toggleScanFormForPackage();
                    } else {
                        swal('', response.msg, 'error');
                        self.resetScanForm();
                    }
                } else {
                    swal('', 'Failed to fetch informatio. Please try again.', 'error');
                    self.resetScanForm();
                }
            }

        });
    },
    scanItemBarcode: function () {
        console.log(this.scannedInformation.package);
        var code = $('input[name="item_barcode"]').val()
        if (code.trim() == "") {
            return;
        }

        code = this.parseBarCodeData(code);
        console.log(code);
        if (typeof code.type == 'undefined' || $.inArray(code.type, ['ITM']) == -1) {
            swal('', 'Invalid Code. Scanned barcode should be for item.', 'error');
            this.resetItemScanFormInput();
            return;
        }

        if (this.scannedInformation.package.hasPkg) {
            //
            var itemFound = false;
            if (Object.keys(this.scannedInformation.package.meals_list.meals).length > 0) {
                $.each(this.scannedInformation.package.meals_list.meals, function (k, v) {
                    if (typeof v.items[code.id] != 'undefined') {
                        itemFound = true;
                    }

                });
            }
            if (!itemFound) {
                swal('', 'Invalid item is scanned for the customer.', 'error');
                this.resetItemScanFormInput();
                return;
            } else {
                var rowNumber = this.hot.countRows();
                //alert('rowNumber : '+rowNumber );
                rowNumber = rowNumber - 1;

                var cloneHtml = $('ul[data-cst="' + this.scannedInformation.customer.customer_account + '"].gridListItems');

                var mealId = cloneHtml.find('li[data-verified="0"][data-item-id="' + code.id + '"]:first').attr('data-meal-id');
                var itemId = cloneHtml.find('li[data-verified="0"][data-item-id="' + code.id + '"]:first').attr('data-item-id');

                this.scannedOrders[this.scannedInformation.customer.customer_account].package_of_the_day.meals_list.meals[mealId].items[itemId].status = 'verified';
                this.scannedOrders[this.scannedInformation.customer.customer_account].package_of_the_day.meals_list.meals[mealId].items[itemId].status = 'verified';
                this.verifiedOrders.push({
                    'customer_account': this.scannedInformation.customer.customer_account,
                    'package_id': this.scannedInformation.package.package_id,
                    'meal_id': cloneHtml.find('li[data-verified="0"][data-item-id="' + code.id + '"]:first').attr('data-meal-id'),
                    'item_id': cloneHtml.find('li[data-verified="0"][data-item-id="' + code.id + '"]:first').attr('data-item-id'),
                });

                cloneHtml.find('li[data-verified="0"][data-item-id="' + code.id + '"]:first').attr('data-verified', '1').find('span.verification-badge').removeClass('bg-red').addClass('bg-green').html('Verified');
                var listHtml = "<ul data-cst='" + this.scannedInformation.customer.customer_account + "' class='nav nav-stacked gridListItems'>" + cloneHtml.html() + "</ul>";
                this.hot.setDataAtCell(rowNumber, 2, listHtml);


                this.resetItemScanFormInput();
            }
        } else {
            console.log('Customer has no item in Package');
        }
    },
    scanBarcodePackage: function () {
        if (this.callInProcess) {
            return;
        }
        this.callInProcess = true;
        if (this.hot.countEmptyRows() > 0) {
            swal('', 'Please use existing empty row first', 'info');
            this.resetPackageScanFormInput();
            return;
        }

        var self = this;
        var code = $('input[name="package_barcode"]').val()
        if (code.trim() == "") {
            return;
        }

        code = this.parseBarCodeData(code);
        if (typeof code.type == 'undefined' || $.inArray(code.type, ['PKG']) == -1) {
            swal('', 'Invalid Code. Scanned barcode should be for package.', 'error');
            this.resetPackageScanFormInput();
            return;
        }
        this.showBarcodeScannigLoader();
        code.meals = this.selectedMeals;
        $.ajax({
            url: '/order/get-barcode-data',
            type: 'post',
            data: code,
            success: function (response) {
                self.callInProcess = false;
                self.hideBarcodeScannigLoader();
                //response = $.parseJSON(response);
                if (typeof response.status != 'undefined') {
                    if (response.status == 1) {
                        $('.customer_name').html(response.data.customer_name)
                        $('.customer_account').html(response.data.customer_account);
                        $('.customer_contact').html(response.data.customer_contact);
                        $('.customer_address').html(response.data.customer_address);
                        $('div.customer-information').show();
                    } else {
                        swal('', response.msg, 'error');
                        self.resetPackageScanFormInput();
                    }
                } else {
                    swal('', 'Failed to fetch information. Please try again.', 'error');
                    self.resetScanForm();
                }
            }

        });
    },
    showBarcodeScannigLoader() {
        $('div.customer-information').hide();
        $('#barcode-loader').show();
    },
    hideBarcodeScannigLoader() {
        $('#barcode-loader').hide();
    },
    IsJsonString: function (str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    },
    selectMeal: function (obj) {
        var self = this;
        this.selectedMeals = [];
        $(obj).toggleClass('btn-info')
        $(obj).toggleClass('btn-default')
        $('.meal-btns').each(function (k, v) {
            if ($(v).hasClass('btn-info')) {
                self.selectedMeals.push($(v).attr('data-meal-type'))
            }
        });
        console.log(self.selectedMeals);

        /*this.selectedMeal = $(obj).attr('data-meal-type');
        $('.meal-btns').removeClass('btn-info');
        $('.meal-btns').addClass('btn-default');
        $(obj).addClass('btn-info');*/
    },
    completeOrder: function () {
        var self = this;
        //console.log(this.verifiedOrders);
        if ($('li[data-verified="0"]').length > 0) {
            swal({
                title: "Are you sure?",
                text: "Items which are not verified will not be processed in order. Do you want to proceed?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!"
            }).then(
                function (isConfirm) {
                    if (typeof isConfirm.value != 'undefined' && isConfirm.value == true) {
                        self.processCompleteOrder();
                    } else {
                        return false;
                    }
                    console.log(isConfirm)

                });
            return false;
        }
        self.processCompleteOrder();
    },
    processCompleteOrder: function () {
        var self = this;
        console.log(self.verifiedOrders)
        if (this.verifiedOrders.length > 0) {
            swal({
                title: "Please wait",
                text: "Submitting....",
                //imageUrl: "images/page_loader1.gif",
                showConfirmButton: false,
                allowOutsideClick: false
            });
            $.ajax({
                url: '/order/save-transaction',
                type: 'post',
                data: {
                    'verifiedOrders': self.verifiedOrders
                },
                success: function (response) {
                    self.callInProcess = false;
                    if (response.status == 1) {
                        swal('', response.msg, 'success');
                        window.location.reload();
                    } else {
                        swal('', response.msg, 'error');
                    }
                }
            });
        } else {
            swal('', 'No single item is verified to process.', 'info');
        }
    },

    draftTransaction() {
        var self = this;
        if (Object.keys(self.scannedOrders).length > 0) {
            $.ajax({
                url: '/order/draft-transaction',
                type: 'post',
                data: {
                    'scannedOrders': self.scannedOrders,
                    'verifiedOrders': self.verifiedOrders,
                    'selectedMeals': self.selectedMeals,
                },
                success: function (response) {

                }
            });
        } else {
            console.log('Nothing To draft.............');
        }

    },

    initDraftTransactions() {
        var self = this;
        setInterval(function () {
            self.draftTransaction();
        }, 10000)
    },
    loadTransactionsFromDraft: function () {
        console.log(this.draftTransactionSaved);
        var self = this;
        var counter = 0;
        console.log(this.draftTransactionSaved);

        var selectedMeals = (typeof this.draftTransactionSaved.selectedMeals != "undefined") ? this.draftTransactionSaved.selectedMeals : [];
        //self.selectedMeals = selectedMeals;

        $.each(selectedMeals, function (k, selMeal) {
            $('button[data-meal-type="' + selMeal + '"]').click();
        });


        var scannedOrders = (typeof this.draftTransactionSaved.scannedOrders != "undefined") ? this.draftTransactionSaved.scannedOrders : [];
        var verifiedOrders = (typeof this.draftTransactionSaved.verifiedOrders != "undefined") ? this.draftTransactionSaved.verifiedOrders : [];
        var verifiedOrdersGrouped = {};
        if (verifiedOrders.length) {
            $.each(verifiedOrders, function (k, v2) {
                if (typeof verifiedOrdersGrouped[v2.customer_account] == 'undefined') {
                    verifiedOrdersGrouped[v2.customer_account] = [v2];
                } else {
                    verifiedOrdersGrouped[v2.customer_account].push(v2);
                }

            });
            console.log(verifiedOrdersGrouped);
        }


        $.each(scannedOrders, function (k, v) {
            counter++;
            self.scannedOrders[v.customer_account] = v;
            self.scannedInformation.customer = {
                customer_name: v.customer_name,
                customer_account: v.customer_account,
                customer_address: v.customer_address,
            };
            self.scannedInformation.package = v.package_of_the_day;
            self.addNewCustomerDataInTable();

            if (typeof verifiedOrdersGrouped[v.customer_account] != 'undefined' && verifiedOrdersGrouped[v.customer_account].length > 0) {
                $.each(verifiedOrdersGrouped[v.customer_account], function (k, verifiedItemDetail) {
                    //var verifiedItemDetail = verifiedOrdersGrouped[v.customer_account];
                    var rowNumber = self.hot.countRows();
                    //alert('rowNumber : '+rowNumber );
                    rowNumber = rowNumber - 1;

                    var cloneHtml = $('ul[data-cst="' + self.scannedInformation.customer.customer_account + '"].gridListItems');

                    var mealId = cloneHtml.find('li[data-verified="0"][data-item-id="' + verifiedItemDetail.item_id + '"]:first').attr('data-meal-id');
                    var itemId = cloneHtml.find('li[data-verified="0"][data-item-id="' + verifiedItemDetail.item_id + '"]:first').attr('data-item-id');

                    self.scannedOrders[self.scannedInformation.customer.customer_account].package_of_the_day.meals_list.meals[mealId].items[itemId].status = 'verified';
                    self.scannedOrders[self.scannedInformation.customer.customer_account].package_of_the_day.meals_list.meals[mealId].items[itemId].status = 'verified';
                    self.verifiedOrders.push({
                        'customer_account': self.scannedInformation.customer.customer_account,
                        'package_id': self.scannedInformation.package.package_id,
                        'meal_id': cloneHtml.find('li[data-verified="0"][data-item-id="' + verifiedItemDetail.item_id + '"]:first').attr('data-meal-id'),
                        'item_id': cloneHtml.find('li[data-verified="0"][data-item-id="' + verifiedItemDetail.item_id + '"]:first').attr('data-item-id'),
                    });

                    cloneHtml.find('li[data-verified="0"][data-item-id="' + verifiedItemDetail.item_id + '"]:first').attr('data-verified', '1').find('span.verification-badge').removeClass('bg-red').addClass('bg-green').html('Verified');
                    var listHtml = "<ul data-cst='" + self.scannedInformation.customer.customer_account + "' class='nav nav-stacked gridListItems'>" + cloneHtml.html() + "</ul>";
                    self.hot.setDataAtCell(rowNumber, 2, listHtml);
                });

            }
        });
        if (counter > 0) {
            self.toggleScanFormForPackage();
        }
    },
    loadOrderGenerated: function () {
        console.log(this.ordersGenerated);
        var self = this;
        var counter = 0;
        console.log(this.ordersGenerated);

        var selectedMeals = (typeof this.ordersGenerated.selectedMeals != "undefined") ? this.ordersGenerated.selectedMeals : [];
        //self.selectedMeals = selectedMeals;

        $.each(selectedMeals, function (k, selMeal) {
            $('button[data-meal-type="' + selMeal + '"]').click();
        });


        var scannedOrders = (typeof this.ordersGenerated.scannedOrders != "undefined") ? this.ordersGenerated.scannedOrders : [];
        var verifiedOrders = (typeof this.ordersGenerated.verifiedOrders != "undefined") ? this.draftTransactionSaved.verifiedOrders : [];
        var verifiedOrdersGrouped = {};
        if (verifiedOrders.length) {
            $.each(verifiedOrders, function (k, v2) {
                if (typeof verifiedOrdersGrouped[v2.customer_account] == 'undefined') {
                    verifiedOrdersGrouped[v2.customer_account] = [v2];
                } else {
                    verifiedOrdersGrouped[v2.customer_account].push(v2);
                }

            });
            console.log(verifiedOrdersGrouped);
        }


        $.each(scannedOrders, function (k, v) {
            counter++;
            self.scannedOrders[v.customer_account] = v;
            self.scannedInformation.customer = {
                customer_name: v.customer_name,
                customer_account: v.customer_account,
                customer_address: v.customer_address,
            };
            self.scannedInformation.package = v.package_of_the_day;
            self.addNewCustomerDataInTable();

            if (typeof verifiedOrdersGrouped[v.customer_account] != 'undefined' && verifiedOrdersGrouped[v.customer_account].length > 0) {
                $.each(verifiedOrdersGrouped[v.customer_account], function (k, verifiedItemDetail) {
                    //var verifiedItemDetail = verifiedOrdersGrouped[v.customer_account];
                    var rowNumber = self.hot.countRows();
                    //alert('rowNumber : '+rowNumber );
                    rowNumber = rowNumber - 1;

                    var cloneHtml = $('ul[data-cst="' + self.scannedInformation.customer.customer_account + '"].gridListItems');

                    var mealId = cloneHtml.find('li[data-verified="0"][data-item-id="' + verifiedItemDetail.item_id + '"]:first').attr('data-meal-id');
                    var itemId = cloneHtml.find('li[data-verified="0"][data-item-id="' + verifiedItemDetail.item_id + '"]:first').attr('data-item-id');

                    self.scannedOrders[self.scannedInformation.customer.customer_account].package_of_the_day.meals_list.meals[mealId].items[itemId].status = 'verified';
                    self.scannedOrders[self.scannedInformation.customer.customer_account].package_of_the_day.meals_list.meals[mealId].items[itemId].status = 'verified';
                    self.verifiedOrders.push({
                        'customer_account': self.scannedInformation.customer.customer_account,
                        'package_id': self.scannedInformation.package.package_id,
                        'meal_id': cloneHtml.find('li[data-verified="0"][data-item-id="' + verifiedItemDetail.item_id + '"]:first').attr('data-meal-id'),
                        'item_id': cloneHtml.find('li[data-verified="0"][data-item-id="' + verifiedItemDetail.item_id + '"]:first').attr('data-item-id'),
                    });

                    cloneHtml.find('li[data-verified="0"][data-item-id="' + verifiedItemDetail.item_id + '"]:first').attr('data-verified', '1').find('span.verification-badge').removeClass('bg-red').addClass('bg-green').html('Verified');
                    var listHtml = "<ul data-cst='" + self.scannedInformation.customer.customer_account + "' class='nav nav-stacked gridListItems'>" + cloneHtml.html() + "</ul>";
                    self.hot.setDataAtCell(rowNumber, 2, listHtml);
                });

            }
        });
        if (counter > 0) {
            self.toggleScanFormForPackage();
        }
    },
    updateTotals: function () {
        $('span.totalCustomers').html(this.totalScannedCustomers)
        $('span.totalPrice').html(this.totalScannedPrice)
    },

    generateLabels(obj) {
        var self = this;


        $('#iframe-wrapper').hide();
        if (self.selectedMeals.length < 1) {
            swal('', 'Please select Meal Type.', 'error');
            return;
        }
        if ($('#dynamicmodel-packagedate').val().trim() == "") {
            swal('', 'Please select Date.', 'error');
            return;
        }


        swal({
            title: "Please wait",
            text: "System is generating labels....",
            //imageUrl: "images/page_loader1.gif",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        $('#iframe-wrapper').find('iframe').on('load', function () {
            swal({
                title: "Done",
                text: "Labels generated successfully",

                showConfirmButton: true,
                allowOutsideClick: true
            });
            $('#iframe-wrapper').show();
        });
        setTimeout(function () {
            $('#iframe-wrapper').find('iframe').attr('src', '/barcode/prepare-barcode-pdf?date=' + $('#dynamicmodel-packagedate').val().trim() + '&meals=' + self.selectedMeals.join(',') + '&customer=' + $('select[id="dynamicmodel-customer_id"]').val()+ '&city=' + $('select[id="dynamicmodel-city_id"]').val())
        }, 0)
    },
    generatePackingList(obj) {
        var self = this;
        if (self.selectedMeals.length < 1) {
            swal('', 'Please select Meal Type.', 'error');
            return;
        }
        if ($('#dynamicmodel-packagedate').val().trim() == "") {
            swal('', 'Please select Date.', 'error');
            return;
        }
        var form = $(obj).closest('form');
        form.append('<input type="hidden" name="DynamicModel[meals]" value="' + self.selectedMeals.join(',') + '">');
        form.submit();

        //console.log(self.selectedMeals)


    },

    scanBarcode: function () {
        var self = this;
        var code = $('input[name="barcode"]').val()
        $('input[name="barcode"]').val('');
        if (code.trim() == "") {
            return;
        }
        code = this.parseBarCodeData(code);
        if (typeof code.type == 'undefined' || $.inArray(code.type, ['CST', 'ITM']) == -1) {
            swal('', 'Invalid Code. Scanned barcode should be for customer / product.', 'error');
            this.resetScanFormInput();
            return;
        }
        if (self.scannedInformation.customer == null && code.type == 'ITM') {
            swal('', 'Please scan customer barcode first.', 'error');
            this.resetScanFormInput();
            return;
        }
        switch (code.type) {
            case 'CST':
                if ($('tr[data-customer-id="' + code.id + '"]').length) {
                    self.scannedInformation.customer = code.id;
                    $('tr[data-customer-id="' + code.id + '"]').click();
                } else {
                    var toast = swal.mixin({
                        toast: true,
                        position: 'center',
                        showConfirmButton: false,
                        timer: 2000
                    });

                    toast({
                        type: 'error',
                        title: 'Invalid Customer'
                    })
                }

                break;
            case 'ITM':
                //
                var customerRows = $('tr[data-customer-id="' + self.scannedInformation.customer + '"]');

                var filterObj = $('tr[data-customer-id="' + self.scannedInformation.customer + '"]').find('td[data-item-verified=\'0\'][data-item-id="' + code.id + '"]').first();
                var found = filterObj.length;
                if (found > 0) {

                    /*
                    * Check if scanned item is not in pending status
                    * */
                    if (filterObj.attr('data-item-value') != '1') {
                        var toast = swal.mixin({
                            toast: true,
                            position: 'center',
                            showConfirmButton: false,
                            timer: 2000
                        });

                        toast({
                            type: 'error',
                            title: 'Item should be in PENDING status'
                        })
                        return;
                    }

                    var trObj = filterObj.closest('tr');
                    trObj.find('td[data-item-verified=\'0\']').attr('data-item-verified', 1);
                    var order_meal_item_id = trObj.find('td[data-item-cell="status"]').attr('data-order-meal-item-id')
                    var newLabel = '<label class="label label-info ajax-loader">Verified </label> &nbsp;&nbsp; <i class="fa fa-refresh fa-spin ajax-loader ajax-loader-' + order_meal_item_id + '"></i>';
                    trObj.find('td[data-item-cell="status"]').html(newLabel);
                    self.markGeneratedOrderItemAsVerified(order_meal_item_id);


                    return;
                } else {
                    var toast = swal.mixin({
                        toast: true,
                        position: 'center',
                        showConfirmButton: false,
                        timer: 2000
                    });

                    toast({
                        type: 'error',
                        title: 'Invalid Item'
                    })
                    /*swal('', 'Invalid Item', 'error');*/
                    this.resetScanFormInput();
                    return;
                }
                break;
        }
    },
    markGeneratedOrderItemAsVerified(orderMealItemId) {
        $.ajax({
            url: '/order/verify-order-meal-item',
            type: 'POST',
            data: {
                orderMealItemId: orderMealItemId
            },
            success: function (response) {
                if (typeof response.status != 'undefined' && response.status == 1) {
                    $('i.ajax-loader-' + orderMealItemId).remove();
                    var itemName = $('td[data-order-meal-item-id="' + orderMealItemId + '"]').closest('tr').find('td[data-item-cell="title"]').text();

                    var toast = swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 2000
                    });

                    toast({
                        type: 'success',
                        title: 'Item ' + itemName + ' marked as verified'
                    })
                }
            }
        });
    },
    completeGeneratedOrder: function () {
        var self = this;
        //console.log(this.verifiedOrders);
        if ($('td[data-item-verified="0"]').length > 0) {
            swal({
                title: "Are you sure?",
                text: "Items which are not verified will not be processed in order. Do you want to proceed?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!"
            }).then(
                function (isConfirm) {
                    if (typeof isConfirm.value != 'undefined' && isConfirm.value == true) {
                        self.processCompleteGeneratedOrder();
                    } else {
                        return false;
                    }
                    console.log(isConfirm)

                });
            return false;
        }
        self.processCompleteGeneratedOrder();
    },
    processCompleteGeneratedOrder: function () {
        var self = this;
        console.log(self.verifiedOrders)
        if ($('td[data-item-verified="1"]').length > 0) {
            swal({
                title: "Please wait",
                text: "Submitting....",
                //imageUrl: "images/page_loader1.gif",
                showConfirmButton: false,
                allowOutsideClick: false
            });

            var orderMealItems = [];
            $('td[data-item-verified="1"]').each(function (i, v) {
                orderMealItems.push({
                    'item_id': $(v).attr('data-order-meal-item-id'),
                    'transaction_id': $(v).attr('data-transaction-id'),
                    'qty': $(v).closest('tr').find('td[data-item-cell="qty"]').find('input.qty_input').val(),
                })
            });
            orderMealItems = Base64.encode(JSON.stringify(orderMealItems));
            console.log(orderMealItems);
            //return;


            $.ajax({
                url: '/order/complete-order-meal-items',
                type: 'post',
                data: {
                    'orderMealItems': orderMealItems
                },
                success: function (response) {
                    if (typeof response.status != 'undefined' && response.status == 1) {
                        swal('', response.msg, 'success');
                        window.location.reload();
                    } else {
                        swal('', response.msg, 'error');
                    }
                }
            });
        } else {
            swal('', 'No single item is verified to process.', 'info');
        }
    },

    exportGeneratedOrder: function () {
        var self = this;
        console.log(self.verifiedOrders)
        if ($('td[data-item-verified="1"]').length > 0) {
            var toast = swal.mixin({
                toast: true,
                position: 'center',
                showConfirmButton: false,
                timer: 2000
            });

            toast({
                type: 'success',
                title: 'Exporting Orders...'
            })

            var orderMealItems = [];
            $('td[data-item-verified="1"]').each(function (i, v) {
                orderMealItems.push($(v).attr('data-order-meal-item-id'))
            });


            let objJsonB64 = btoa(orderMealItems.join(','));
            window.location.href = "/order/export-order-meal-items?items=" + objJsonB64;
        } else {
            swal('', 'No single item is verified to process.', 'info');
        }
    },
    importGeneratedOrder: function () {
        $('input[type="file"][name="DynamicModel[import_orders]"]').click();
    },
    enableImportOrdersSubmit: function () {
        $('button.upload-imported-btn').hide();
        if ($('input[type="file"][name="DynamicModel[import_orders]"]').val() != "") {
            $('button.upload-imported-btn').show();
        }
    },
    uploadImportedOrders: function (obj) {
        console.log($(obj).html());
        $(obj).find('i').removeClass('fa-upload').addClass('fa-refresh fa-spin')
        $(obj).find('span').html('Uploading...')
        $(obj).closest('form').submit();
    },
    updateOrderItemQty: function (obj) {
        var val = $(obj).val();
        var old_val = $(obj).attr('data-old-value');
        if (isNaN(val) || val <=0) {
            var toast = swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 2000
            });

            toast({
                type: 'error',
                title: 'Invalid qty'
            });
            $(obj).val(old_val);
            return;
        }
        var orderMealItemId = $(obj).closest('td').attr('data-order-item-id');
        $.ajax({
            url: '/order/update-order-meal-item-qty',
            type: 'POST',
            data: {
                orderMealItemId: orderMealItemId,
                qty: val
            },
            success: function (response) {
                if (typeof response.status != 'undefined' && response.status == 1) {
                    var toast = swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    toast({
                        type: 'success',
                        title: 'Qty updated successfully'
                    })
                }else{
                    var toast = swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 2000
                    });

                    toast({
                        type: 'error',
                        title: 'failed to update qty'
                    });
                }
            }
        });

    },


    OrderGeneration:
        {
            fetchOrders: function (obj) {
                var self = this;
                if (PackageManager.selectedMeals.length < 1) {
                    swal('', 'Please select Meal Type.', 'error');
                    return;
                }

                $('input[name="DynamicModel[selectedMeals]"]').val(PackageManager.selectedMeals.join(','));
                $(obj).closest('form').submit();
            }
            ,

            onloadPage: function () {
                var selectedMeals = $('input[name="DynamicModel[selectedMeals]"]').val().split(',');
                $.each(selectedMeals, function (k, selMeal) {
                    $('button[data-meal-type="' + selMeal + '"]').click();
                });

            }
            ,
            generateOrder: function () {
                swal({
                    title: "Are you sure?",
                    text: "Order will be generated with pending status. Want to proceed?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes',
                    cancelButtonText: "No, cancel it!"
                }).then(
                    function (isConfirm) {
                        if (typeof isConfirm.value != 'undefined' && isConfirm.value == true) {
                            if ($('input[name="order-content"]').length) {
                                swal({
                                    title: "Please wait",
                                    text: "Submitting....",
                                    //imageUrl: "images/page_loader1.gif",
                                    showConfirmButton: false,
                                    allowOutsideClick: false
                                });
                                $.ajax({
                                    url: '/order/save-generate-order',
                                    type: 'post',
                                    data: {
                                        'data': $('input[name="order-content"]').val()
                                    },
                                    success: function (response) {
                                        self.callInProcess = false;
                                        if (response.status == 1) {
                                            swal('', response.msg, 'success');
                                            window.location.href = '/order/generate-order';
                                        } else {
                                            swal('', response.msg, 'error');
                                        }
                                    }
                                });
                            } else {
                                swal('', 'Failed to process. Please refresh page and try again', 'info');
                            }
                        } else {
                            return false;
                        }
                    });
                return false;
            }
            ,

            customFocusOnSelectedRow: function (obj) {
                PackageManager.scannedInformation.customer = $(obj).attr('data-customer-id');
                $('table.orders-table').find('tr').removeClass('selected-customer-row');
                $('tr[data-customer-id="' + $(obj).attr('data-customer-id') + '"]').addClass('selected-customer-row');
            }
            ,

            toggleHoldOrderItem: function (obj, orderMealItemId) {
                $(obj).closest('td').find('span.hold-loader-span').html('<i class="fa fa-refresh fa-spin ajax-loader ajax-loader-\'+order_meal_item_id+\'"></i>');
                $.ajax({
                    url: '/order/hold-order-meal-item',
                    type: 'POST',
                    data: {
                        orderMealItemId: orderMealItemId,
                        hold: ($(obj).is(':checked')) ? 1 : 0
                    },
                    success: function (response) {
                        if (typeof response.status != 'undefined' && response.status == 1) {
                            $(obj).closest('tr').find('td[data-item-cell="status"]').attr('data-item-verified', 0);
                            $(obj).closest('tr').find('td[data-item-cell="status"]').attr('data-item-value', $(obj).is(':checked') ? 4 : 1);
                            $(obj).closest('tr').find('td[data-item-cell="status"]').html($(obj).is(':checked') ? '<label class="label label-warning">on Hold</label>' : '<label class="label label-default">Pending</label>');
                            $(obj).closest('td').find('span.hold-loader-span').html('');
                            var itemName = $('td[data-order-meal-item-id="' + orderMealItemId + '"]').closest('tr').find('td[data-item-cell="title"]').text();

                            var toast = swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2000
                            });
                            toast({
                                type: 'success',
                                title: 'Item is ' + ($(obj).is(':checked') ? 'On Hold' : 'Unhold')
                            })
                        }
                    }
                });
            }

        }
    ,
    Notifications: {
        fetchCustomers: function (obj) {
            swal({
                title: "Please wait",
                text: "Fetching....",
                //imageUrl: "images/page_loader1.gif",
                showConfirmButton: false,
                allowOutsideClick: false
            });
            $(obj).closest('form').submit();
        }
        ,
        sendNotification: function () {
            if ($('input[type="checkbox"][name="customers[]"]:checked').length < 1) {
                swal('', 'No customer is selected.', 'error');
                return;
            }

            if ($('select[name="notification_template"] option:selected').val() == "") {
                swal('', 'Please select notification template.', 'error');
                return;
            }
            swal({
                title: "Processing, Please wait...",
                html: '<div class="row">' +
                '       <div class="col-md-offset-3 col-md-6">' +
                '       <div class="c100 p0 green percCounterClass">\n' +
                '                    <span class="percCounter">0%</span>\n' +
                '                    <div class="slice">\n' +
                '                        <div class="bar"></div>\n' +
                '                        <div class="fill"></div>\n' +
                '                    </div>\n' +
                '                </div>' +
                '       </div>' +
                '</div>'
                ,
                //imageUrl: "images/page_loader1.gif",
                showConfirmButton: false,
                allowOutsideClick: false
            });

            var selectedCustomersCount = $('input[type="checkbox"][name="customers[]"]:checked').length;
            var totalHits = Math.ceil(selectedCustomersCount / 5);
            var currentHit = 1;
            var allCustomerList = [];
            $('input[type="checkbox"][name="customers[]"]:checked').each(function (i, v) {
                allCustomerList.push($(v).val());
            });
            console.log(allCustomerList)

            this.processNotification(allCustomerList, totalHits, currentHit, 0, 5);

        }
        ,
        processNotification: function (allCustomerList, totalHits, currentHit, indexStart, indexEnd) {
            var nextIndexStart = indexStart + 5;
            var nextIndexEnd = indexEnd + 5;
            var nextCurrentHit = currentHit + 1;
            var customers = allCustomerList.slice(indexStart, indexEnd);
            var perc = Math.ceil((currentHit / totalHits) * 100);
            console.log(customers);
            console.log(perc);
            console.log('totalHits' + totalHits);
            console.log('currentHit' + currentHit);
            console.log('indexStart' + indexStart);
            console.log('indexEnd' + indexEnd);
            var self = this;
            if (currentHit <= totalHits) {
                $.ajax({
                    url: '/notifications/send-notification',
                    type: 'post',
                    data: {
                        customer: customers,
                        template: $('select[name="notification_template"] option:selected').val(),
                    },
                    success: function (response) {
                        if (typeof response.status != 'undefined') {
                            if (response.status == 1) {
                                $('.percCounter').html(perc + '%')
                                $('.percCounterClass').addClass('p' + perc)
                                self.processNotification(allCustomerList, totalHits, nextCurrentHit, nextIndexStart, nextIndexEnd)
                            } else {
                                swal('', response.msg, 'error');
                            }
                        } else {
                            swal('', 'Failed to send notifications. Please try again.', 'error');
                        }
                    },
                    failed: function () {
                        swal('', 'Failed to send notifications. Please try again.', 'error');
                    }

                });
            } else {
                swal('', 'Notifications sent successfully', 'success');
            }

        }

    },


};
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/++[++^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
