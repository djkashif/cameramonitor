<?php
namespace backend\controllers;
use backend\components\GeneralHelper;
use backend\components\TicketsManager;
use common\models\SupportTickets;
use common\models\SupportTicketsSearch;
use Yii;
use yii\base\DynamicModel;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class CustomerSupportController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Admin"],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['viewCustomerSupportTickets']
                    ],
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication");
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {


        $searchModel = new SupportTicketsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionCloseTicket($id){
        if(Yii::$app->request->post()){
            $model = $this->findModel($id);
            $model->ticket_status = 3;
            $model->save();
            GeneralHelper::showSuccessMsg('Ticket closed successfully');
            return $this->redirect('/customer-support');
        }
        return $this->goBack();

    }

    public function actionViewTicket($id, $dept=null){

        $dynamicModel = new DynamicModel([
                'ticket_response'
        ]);
        $dynamicModel->addRule([
            'ticket_response'
        ], 'required');
        $model = $this->findModel($id);

        //echo "<pre>";print_r(Yii::$app->user);echo "</pre>";die('Call');
        if ($dynamicModel->load(Yii::$app->request->post())) {

            $model->ticket_response = $dynamicModel->ticket_response;
            //$model->ticket_status = 3;
            $model->action_by = Yii::$app->user->id;
            $model->action_time = date('Y-m-d H:i:s');
            $model->save();

            $customerName = trim($model->createdBy->user->firstName . " " . $model->createdBy->user->lastName);
            $emailContent = "<p>Dear {$customerName},</p>";
            $emailContent .= "<p>{$model->ticket_response}</p>";
            $emailContent .= "<p>Regards</p>";
            $emailContent .= "<p>Customer Support, Fitnessfood.com.pk</p>";
            /*$sent= GeneralHelper::sendEmail([
                'fromEmail' => Yii::$app->params['supportEmail'],
                'fromName' => Yii::$app->params['supportName'],
                'toEmail' => $model->createdBy->user->email,
                'toName' => trim($model->createdBy->user->firstName . " " . $model->createdBy->user->lastName),
                'subject' => 'Ticket Closed',
                'htmlBody' => $emailContent,
            ]);*/
            $sent = 1;

            if($sent){
                GeneralHelper::showSuccessMsg('Response recorded successfully');
            }else{
                GeneralHelper::showSuccessMsg('Response recorded successfully. But customer was not notified via email');
            }

            return $this->redirect('/customer-support');
        }



        $SupportTicketsQuery = SupportTickets::find()->where(['created_by' => $model->created_by]);
        if($dept){
            $SupportTicketsQuery->andWhere(['ticket_dept' => $dept]);
        }

        $SupportTickets =     $SupportTicketsQuery->orderBy('createdOn asc, modifiedOn asc')
            ->asArray()->all();
        $SupportTicketList = [];
        foreach ($SupportTickets as $SupportTicket){
            $SupportTicketList[] = [
                'msg' => $SupportTicket['ticket_dept'].": ".$SupportTicket['ticket_description'],
                'time' => $SupportTicket['createdOn'],
                'msg_type' => 'user',
            ];
            if(!empty($SupportTicket['ticket_response'])){
                $SupportTicketList[] = [
                    'msg' => $SupportTicket['ticket_response'],
                    'time' => $SupportTicket['modifiedOn'],
                    'msg_type' => 'admin',
                ];
            }
        }
        //echo "<pre>";print_r($SupportTicketList);echo "</pre>";die('Call');

        return $this->render('view-ticket', [
            'model' => $model,
            'dynamicModel' => $dynamicModel,
            'supportTicketList' => $SupportTicketList,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        GeneralHelper::showSuccessMsg('Ticket deleted successfully');
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = SupportTickets::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
