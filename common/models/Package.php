<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "package".
 *
 * @property int $id
 * @property int $customer_id
 * @property string $package_title
 * @property int $number_of_meals
 * @property string $package_start_date
 * @property int $package_duration
 * @property string $package_expiration
 * @property int $package_status
 * @property string $barcode
 * @property string $createdOn
 * @property string $modifiedOn
 */
class Package extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $actualStartDate;
    public $actualExpirationDate;
    public static function tableName()
    {
        return 'package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'package_title', 'package_start_date', 'package_duration', 'package_expiration'], 'required'],
            [['customer_id', 'number_of_meals', 'package_duration', 'package_status'], 'integer'],
            [['package_start_date', 'package_expiration', 'createdOn', 'modifiedOn'], 'safe'],
            [['package_title', 'barcode'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'package_title' => 'Package Title',
            'number_of_meals' => 'Number Of Meals',
            'package_start_date' => 'Package Start Date',
            'package_duration' => 'Package Duration',
            'package_expiration' => 'Package Expiration',
            'package_status' => 'Package Status',
            'barcode' => 'Barcode',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }


    public function getProducts()
    {
        return $this->hasMany(PackageProducts::className(), ["package_id" => "id"]);
    }

    public function getMeals()
    {
        return $this->hasMany(PackageMeals::className(), ["package_id" => "id"]);
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ["id" => "customer_id"]);
    }
}
