<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@fitnessfood.com.pk',
    'supportName' => 'Customer Support',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
];
