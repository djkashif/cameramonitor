<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t("app", "Create Role");

?>
<div class="box">
    <?php $form = ActiveForm::begin(); ?>
        <div class="box-body with-border">
            <div class="row">
                <div class="col-md-3">
                    <?php echo $form->field($model, "title")->textInput(); ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <?php echo Html::a(Yii::t("app", "Back"), Url::to(["index"]), ["class" => "btn btn-default"]); ?>
            <?php echo Html::submitButton(Yii::t("app", "Submit"), ["class" => "btn btn-primary pull-right"]); ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>