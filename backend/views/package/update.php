<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Vehicle */

$this->title = 'Update Package: ' . $model->package_title;
if (Yii::$app->user->can("exportPackage")) {
    $this->params["headerButtons"][] = Html::a(Yii::t("app", "Export"), Url::to(["export-package?id={$model->id}"]), ["class" => "btn btn-primary"]);
}
?>
<div class="customer-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dynamicModel' => $dynamicModel,
        'products' => $products,
        //'productsOptions' => $productsOptions,
        'customers' => $customers,
        'packageMealDetails' => $packageMealDetails,
        'packageDateList' => $packageDateList,
        'meals' => $meals,
    ]) ?>

</div>
