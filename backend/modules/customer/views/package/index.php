<?php

use yii\helpers\Html;

use yii\widgets\ActiveForm;

$this->title = 'My Package';
?>
<div class="row">
    <!-- left column -->
    <div class="col-md-4">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Package Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <tr>
                            <th>Package Title</th>
                            <td><?= $pkgOfTheDay->package_title ?></td>
                        </tr>
                        <tr>
                            <th>Meals</th>
                            <td>
                                <ul>
                                    <?php
                                    foreach ($packageMeals as $packageMeal){
                                        ?>
                                        <li><?= (isset($meals[$packageMeal])) ? $meals[$packageMeal] : 'N/A' ?></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right">
                                <div class="form-group">
                                    <a class="btn btn-app">
                                        <span class="badge bg-green">Expiry Date</span>
                                        <i class="fa fa-expeditedssl"></i> <?= date('M d, Y', strtotime($pkgOfTheDay->package_expiration)) ?>
                                    </a>
                                    <a class="btn btn-app">
                                        <span class="badge bg-green">Duration (in days)</span>
                                        <i class="fa fa-gg-circle"></i> <?= $pkgOfTheDay->package_duration ?>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Package Status</th>
                            <td><?= (isset($pkgStatusList[$pkgOfTheDay->package_status])) ? $pkgStatusList[$pkgOfTheDay->package_status] : 'N/A' ?></td>
                        </tr>
                    </table>
                </div>


            </div>


        </div>
        <!-- /.box -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-8">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Meal Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php $form = ActiveForm::begin(['action' => '/customer/package/meals']); ?>
            <input type="hidden" value="<?= $pkgOfTheDay->id ?>" name="package">
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Date</th>
                        <?php
                        if (!empty($pkgOfTheDay->meals)) {
                            foreach ($pkgOfTheDay->meals as $meal) {

                                ?>
                                <th>
                                    <?= $meal->mealDetail->meal_title ?>
                                </th>
                                <?php
                            }
                        }
                        ?>
                        <th style="width: 40px">Action</th>
                    </tr>
                    <?php

                    foreach ($packageDateList as $indexPkgDate => $packageDate): ?>
                        <tr>
                            <td><?= ($indexPkgDate + 1) ?>.</td>
                            <td><?= $packageDate ?></td>
                            <?php
                            if (!empty($pkgOfTheDay->meals)) {
                                foreach ($pkgOfTheDay->meals as $meal) {
                                    ?>
                                    <td>
                                        <?php
                                        echo \kartik\select2\Select2::widget([
                                            'name' => 'package_date_meals[products][' . $packageDate . '][' . $meal->meal_id . ']',
                                            'value' => (isset($packageMealDetails['products'][$packageDate][$meal->meal_id])) ? $packageMealDetails['products'][$packageDate][$meal->meal_id] : '',
                                            'data' => $products,
                                            'options' => ['multiple' => true, 'placeholder' => 'Select item']
                                        ]);
                                        ?>
                                    </td>
                                    <?php
                                }
                            }
                            ?>
                            <td>
                                <select name="package_date_meals[status][<?= $packageDate ?>]" id="">
                                    <option value="1" <?= (isset($packageMealDetails['status'][$packageDate]) && $packageMealDetails['status'][$packageDate] == 1) ? 'selected="selected"' : '' ?>>
                                        Active
                                    </option>
                                    <option value="2" <?= (isset($packageMealDetails['status'][$packageDate]) && $packageMealDetails['status'][$packageDate] == 2) ? 'selected="selected"' : '' ?>>
                                        Inactive
                                    </option>
                                </select>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-default">Reset</button>
                <?= Html::submitButton('Save', ['class' => 'btn btn-info pull-right']) ?>

            </div>
            <!-- /.box-footer -->
            <?php ActiveForm::end(); ?>
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
</div>
<?php
$this->registerJs("$('a.sidebar-toggle').click();");
//$this->registerJs("swal('Deleted!','Your file has been deleted.','confirm')");
?>