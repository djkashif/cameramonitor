<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "draft_transactions".
 *
 * @property int $id
 * @property int $user_id
 * @property string $transaction_data
 * @property string $createdOn
 * @property string $modifiedOn
 */
class DraftTransactions extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'draft_transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'transaction_data'], 'required'],
            [['user_id'], 'integer'],
            [['transaction_data'], 'string'],
            [['createdOn', 'modifiedOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'transaction_data' => 'Transaction Data',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }
}
