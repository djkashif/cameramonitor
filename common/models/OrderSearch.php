<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;



/**
 * VehicleSearch represents the model behind the search form of `common\models\Vehicle`.
 */
class OrderSearch extends Orders
{
    public $customer;
    public $transaction;
    public $package;
    public $account;
    public function rules()
    {
        return [
            [['account','id','customer', 'transaction'], 'safe']

        ];
    }


    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find()
        ->innerJoinWith(["customer", "package", "transaction", "orderMeals"])
        ->join('INNER JOIN', 'users', 'customer.user_id = users.id');


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            "sort" => ["defaultOrder" => ["id" => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([

            'createdOn' => $this->createdOn,
            'modifiedOn' => $this->modifiedOn,
        ]);

        $query
            ->andFilterWhere(['like', 'users.firstName', $this->customer])
            ->orFilterWhere(['like', 'users.lastName', $this->customer])
            ->andFilterWhere(['like', 'customer.accountNumber', $this->account])
            ->andFilterWhere(['like', 'orders.transaction_id', $this->transaction])
            ->andFilterWhere(['like', 'package.package_title', $this->package]);


        return $dataProvider;
    }
}
