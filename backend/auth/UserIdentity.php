<?php
namespace backend\auth;

use common\models\ActiveRecord;
use Yii;
use yii\web\IdentityInterface;

class UserIdentity extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return "{{%users}}";
    }

    public static function findIdentity($id)
    {
        return static::findOne(["id" => $id, "active" => 1]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        
    }

    public static function findByUsername($email)
    {
        return static::findOne(["email" => $email]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function validatePassword($password, $passwordHash)
    {
        return Yii::$app->security->validatePassword($password, $passwordHash);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }   
}