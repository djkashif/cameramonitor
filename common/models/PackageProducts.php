<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "package_products".
 *
 * @property int $id
 * @property int $package_id
 * @property int $meal_id
 * @property int $product_id
 * @property string $serve_date
 * @property int $is_selected
 * @property int $status
 * @property string|null $createdOn
 * @property string|null $modifiedOn
 */
class PackageProducts extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package_products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['package_id', 'meal_id', 'product_id', 'serve_date'], 'required'],
            [['package_id', 'meal_id', 'product_id', 'is_selected', 'status'], 'integer'],
            [['serve_date', 'createdOn', 'modifiedOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'package_id' => 'Package ID',
            'meal_id' => 'Meal ID',
            'product_id' => 'Product ID',
            'serve_date' => 'Serve Date',
            'is_selected' => 'Is Selected',
            'status' => 'Status',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }

    public function getPackage()
    {
        return $this->hasOne(Package::className(), ["id" => "package_id"]);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ["id" => "product_id"]);
    }

    public function getMealDetail()
    {
        return $this->hasOne(Meals::className(), ["id" => "meal_id"]);
    }
}
