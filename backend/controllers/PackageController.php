<?php

namespace backend\controllers;


use backend\components\BarcodeGenerator;
use backend\components\GeneralHelper;
use common\models\Cities;
use common\models\Customer;
use common\models\Meals;
use common\models\Package;
use common\models\PackageMeals;
use common\models\PackageProducts;
use common\models\PackageSearch;

use common\models\Product;
use kartik\mpdf\Pdf;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;

use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 *  implements the CRUD actions.
 */
class PackageController extends Controller
{
    /**
     * {@inheritdoc}
     */


    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Admin"],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['viewPackages']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['viewPackages']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['createPackage']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create-meal-details'],
                        'roles' => ['createPackage']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'meals'],
                        'roles' => ['updatePackage']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deletePackage']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['export-package', 'export-all-packages'],
                        'roles' => ['exportPackage']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['bulk-update'],
                        'roles' => ['bulkUpdatePackage']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['packing-list', 'generate-packing-list'],
                        'roles' => ['packingList']
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication");
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all packages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PackageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $cityList = ArrayHelper::map(Cities::find()->Where(['active'=>'1'])->all(), "id", "name");

        return $this->render('index', [
            'searchModel' => $searchModel,
            'cityList' => $cityList,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single packages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new packages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Package();
        $dynamicModel = new DynamicModel([
            'package_meals',
            'customer_id'
        ]);
        $dynamicModel->addRule(["customer_id", "package_meals"], "required");
        if ($model->load(Yii::$app->request->post()) && $dynamicModel->load(Yii::$app->request->post())) {
            $model->customer_id = $dynamicModel->customer_id;
            $startDate = date('Y-m-d', strtotime($model->package_start_date));
            $startDateForCheck = date('Y-m-d', strtotime($model->package_start_date));
            $expirationDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($model->package_start_date)) . " + " . $model->package_duration . " day"));
            $expirationDateForCheck = date('Y-m-d', strtotime(date('Y-m-d', strtotime($model->package_start_date)) . " + " . ($model->package_duration - 1) . " day"));

            $availableCustomerPackages = Package::find()
                ->where(['customer_id' => $model->customer_id])
                ->andWhere([
                    'or',
                    [
                        'and',
                        [
                            '<=', 'package_start_date', $startDate
                        ],
                        [
                            '>', 'package_expiration', $startDate
                        ]
                    ],
                    [
                        'and',
                        [
                            '<', 'package_start_date', $expirationDate
                        ],
                        [
                            '>=', 'package_expiration', $expirationDate
                        ]
                    ],

                ])
                ->all();
            if(count($availableCustomerPackages)){
                GeneralHelper::showErrorMsg('['.$availableCustomerPackages[0]->package_title.'] Package already exits in between '.date('M d, Y', strtotime($startDateForCheck)) .' AND '.date('M d, Y', strtotime($expirationDateForCheck)) );
                goto FAILED_CHECKPOINT;
                //return $this->redirect('/package/create');
            }
            $model->package_start_date = $startDate;
            $model->package_expiration = $expirationDate;
            if ($model->save()) {
                $barCodeTitle = 'pkg_' . $model->id . '_' . time() . '.png';
                /*$barcodeData = json_encode([
                    'type' => 'PKG',
                    'id' => $model->id,
                ]);*/
                $barcodeData = "PKG" . $model->id;
                BarcodeGenerator::createPngBarcode($barcodeData, 'barcodes/' . $barCodeTitle);
                $model->barcode = $barCodeTitle;
                $model->save();

                foreach ($dynamicModel->package_meals as $meal) {
                    $package_meal = new PackageMeals();
                    $package_meal->package_id = $model->id;
                    $package_meal->meal_id = $meal;
                    $package_meal->save();
                }
                GeneralHelper::showSuccessMsg('Package details saved successfully. Please add meal items now.');
                return $this->redirect('/package/create-meal-details?id=' . $model->id);
            } else {
                /*echo "<pre>";
                print_r($model->getErrors());
                echo "</pre>";
                die('Call');*/
                GeneralHelper::showErrorMsg('Error occurred while saving data');
                //Yii::$app->session->setFlash("error", Yii::t("app", "Error occurred while saving data"));
                return $this->redirect('/package');
            }
        }

        FAILED_CHECKPOINT:

        $packageMealDetails = [];
        $products = ArrayHelper::map(Product::find()->all(), "id", "product_title");
        $customers = ArrayHelper::map(Customer::find()->innerJoinWith(['user' => function($query){
            return $query->where(['active' => 1]);

        }])->all(), "id", function ($value) {
            return trim($value->user->firstName . " " . $value->user->lastName);
        });
        $meals = ArrayHelper::map(Meals::find()->all(), "id", "meal_title");


        return $this->render('create', [
            'model' => $model,
            'products' => $products,
            'customers' => $customers,
            'meals' => $meals,
            'dynamicModel' => $dynamicModel,
            'packageMealDetails' => $packageMealDetails,

        ]);
    }

    /**
     * Updates an existing packages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCreateMealDetails($id)
    {
        //echo "<pre>";print_r(Yii::$app->request->post());echo "</pre>";die('Call');
        $model = $this->findModel($id);
        //echo "<pre>";print_r($model);echo "</pre>";die('Call');
        $startDate = date('Y-m-d', strtotime($model->package_start_date));
        $expirationDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($model->package_start_date)) . " + " . $model->package_duration . " day"));
        $model->package_start_date = $startDate;
        $model->package_expiration = $expirationDate;

        $dynamicModel = new DynamicModel([
            'package_meals',
            'customer_id',
            'extend_package_duration'
        ]);

        $dynamicModel->addRule(["customer_id", "package_meals"], "required");
        $dynamicModel->addRule(["extend_package_duration"], "safe");
        $packageMeals = [];
        if ($model->meals) {
            foreach ($model->meals as $product) {
                $packageMeals[] = $product->meal_id;
            }
        }

        $dynamicModel->package_meals = $packageMeals;
        $dynamicModel->customer_id = $model->customer_id;
        //echo "<pre>";print_r($dynamicModel);echo "</pre>";die('Call');


        if ($model->load(Yii::$app->request->post()) && $dynamicModel->load(Yii::$app->request->post())) {
            $model->customer_id = $dynamicModel->customer_id;
            $startDate = date('Y-m-d', strtotime($model->package_start_date));
            $startDateForCheck = date('Y-m-d', strtotime($model->package_start_date));

            $model->package_duration = (!empty($dynamicModel->extend_package_duration) && is_numeric($dynamicModel->extend_package_duration)) ? $model->package_duration + $dynamicModel->extend_package_duration : $model->package_duration;
            $expirationDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($model->package_start_date)) . " + " . $model->package_duration . " day"));
            $expirationDateForCheck = date('Y-m-d', strtotime(date('Y-m-d', strtotime($model->package_start_date)) . " + " . ($model->package_duration - 1) . " day"));


            $availableCustomerPackages = Package::find()
                ->where(['<>', 'id', $model->id])
                ->andWhere(['package_status' => 1, 'customer_id' => $model->customer_id])
                ->andWhere([
                    'or',
                    [
                        'and',
                        [
                            '<=', 'package_start_date', $startDate
                        ],
                        [
                            '>', 'package_expiration', $startDate
                        ]
                    ],
                    [
                        'and',
                        [
                            '<', 'package_start_date', $expirationDate
                        ],
                        [
                            '>=', 'package_expiration', $expirationDate
                        ]
                    ],

                ])
                ->all();
            if(count($availableCustomerPackages)){
                GeneralHelper::showErrorMsg('['.$availableCustomerPackages[0]->package_title.'] Package already exits in between '.date('M d, Y', strtotime($startDateForCheck)) .' AND '.date('M d, Y', strtotime($expirationDateForCheck)) );
                goto UDPATE_FAILED_CHECKPOINT;
                //return $this->redirect('/package/create');
            }




            $model->package_start_date = $startDate;
            $model->package_expiration = $expirationDate;

            if ($model->save()) {

                if (empty($model->barcode)) {
                    $barCodeTitle = 'pkg_' . $model->id . '_' . time() . '.png';
                    /*$barcodeData = json_encode([
                        'type' => 'PKG',
                        'id' => $model->id,
                    ]);*/
                    $barcodeData = "PKG" . $model->id;
                    BarcodeGenerator::createPngBarcode($barcodeData, 'barcodes/' . $barCodeTitle);
                    $model->barcode = $barCodeTitle;
                    $model->save();
                }


                PackageMeals::deleteAll(['package_id' => $model->id]);
                foreach ($dynamicModel->package_meals as $meal) {
                    $package_meal = new PackageMeals();
                    $package_meal->package_id = $model->id;
                    $package_meal->meal_id = $meal;
                    $package_meal->save();
                }
                GeneralHelper::showSuccessMsg('Package saved successfully');
                //Yii::$app->session->setFlash("success", Yii::t("app", "Data saved successfully"));
                return $this->redirect('/package');
            } else {
                GeneralHelper::showErrorMsg('Error occurred while saving data');
                //Yii::$app->session->setFlash("error", Yii::t("app", "Error occurred while saving data"));
                return $this->redirect('/package');
            }

        }

        UDPATE_FAILED_CHECKPOINT:
        $products = ArrayHelper::map(Product::find()->all(), "id", "product_title");
        $customers = ArrayHelper::map(Customer::find()->innerJoinWith(['user' => function($query){
            return $query->where(['active' => 1]);

        }])->all(), "id", function ($value) {
            return trim($value->user->firstName . " " . $value->user->lastName);
        });
        $meals = ArrayHelper::map(Meals::find()->all(), "id", "meal_title");

        $packageMealDetails = [];
        $packageProducts = PackageProducts::find()->where(['package_id' => $model->id])->all();
        if (!empty($packageProducts)) {
            foreach ($packageProducts as $packageProduct) {
                $packageMealDetails['products'][date('d-M-Y', strtotime($packageProduct->serve_date))][$packageProduct->meal_id][] = $packageProduct->product_id;
                $packageMealDetails['status'][date('d-M-Y', strtotime($packageProduct->serve_date))] = $packageProduct->status;
            }
        }



        $packageDateList = [];
        $period = new \DatePeriod(
            new \DateTime(date('Y-m-d', strtotime($model->package_start_date))),
            new \DateInterval('P1D'),
            new \DateTime(date('Y-m-d', strtotime($model->package_expiration)))
        );
        foreach ($period as $key => $value) {
            $packageDateList[] = $value->format('d-M-Y');
        }
        return $this->render('update', [
            'model' => $model,
            'products' => $products,
            'customers' => $customers,
            'dynamicModel' => $dynamicModel,
            'packageMealDetails' => $packageMealDetails,
            'packageDateList' => $packageDateList,
            'meals' => $meals,
        ]);
    }
    public function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);
        $interval = date_diff($datetime1, $datetime2);
        return $interval->format($differenceFormat);
    }

    public function actionUpdate($id)
    {
        //echo "<pre>";print_r(Yii::$app->request->post());echo "</pre>";die('Call');
        $model = $this->findModel($id);
        //echo "<pre>";print_r($model);echo "</pre>";die('Call');
        $startDate = date('Y-m-d', strtotime($model->package_start_date));
        $todayDate = date('Y-m-d');
        $daysDiff = $this->dateDifference($startDate, $todayDate);
        $newStartDate = ($daysDiff>3) ? date('Y-m-d',strtotime($todayDate." - 3 days")) : $startDate;
        // $newStartDate = $startDate;

        $expirationDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($model->package_start_date)) . " + " . $model->package_duration . " day"));
        $daysDiff = $this->dateDifference($expirationDate, $todayDate);
        $newExpirationDate = ($daysDiff>10) ? date('Y-m-d',strtotime($todayDate." + 10 days")) : $expirationDate;

        $model->package_start_date =   $newStartDate;// $startDate;-
        $model->package_expiration = $expirationDate; // $newExpirationDate;
        $model->actualStartDate = $startDate;
        $model->actualExpirationDate = $expirationDate;

        $dynamicModel = new DynamicModel([
            'package_meals',
            'customer_id',
            'extend_package_duration'
        ]);

        $dynamicModel->addRule(["customer_id", "package_meals"], "required");
        $dynamicModel->addRule(["extend_package_duration"], "safe");
        $packageMeals = [];
        if ($model->meals) {
            foreach ($model->meals as $product) {
                $packageMeals[] = $product->meal_id;
            }
        }

        $dynamicModel->package_meals = $packageMeals;
        $dynamicModel->customer_id = $model->customer_id;
        //echo "<pre>";print_r($dynamicModel);echo "</pre>";die('Call');


        if ($model->load(Yii::$app->request->post()) && $dynamicModel->load(Yii::$app->request->post())) {
            $model->customer_id = $dynamicModel->customer_id;
            $startDate = date('Y-m-d', strtotime($model->package_start_date));
            $startDateForCheck = date('Y-m-d', strtotime($model->package_start_date));

            $model->package_duration = (!empty($dynamicModel->extend_package_duration) && is_numeric($dynamicModel->extend_package_duration)) ? $model->package_duration + $dynamicModel->extend_package_duration : $model->package_duration;
            $expirationDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($model->package_start_date)) . " + " . $model->package_duration . " day"));
            $expirationDateForCheck = date('Y-m-d', strtotime(date('Y-m-d', strtotime($model->package_start_date)) . " + " . ($model->package_duration - 1) . " day"));


            $availableCustomerPackages = Package::find()
                ->where(['<>', 'id', $model->id])
                ->andWhere(['customer_id' => $model->customer_id])
                ->andWhere([
                    'or',
                    [
                        'and',
                        [
                            '<=', 'package_start_date', $startDate
                        ],
                        [
                            '>', 'package_expiration', $startDate
                        ]
                    ],
                    [
                        'and',
                        [
                            '<', 'package_start_date', $expirationDate
                        ],
                        [
                            '>=', 'package_expiration', $expirationDate
                        ]
                    ],

                ])
                ->all();
            if(count($availableCustomerPackages)){
                GeneralHelper::showErrorMsg('['.$availableCustomerPackages[0]->package_title.'] Package already exits in between '.date('M d, Y', strtotime($startDateForCheck)) .' AND '.date('M d, Y', strtotime($expirationDateForCheck)) );
                goto UDPATE_FAILED_CHECKPOINT;
                //return $this->redirect('/package/create');
            }


            $model->package_start_date = $startDate;
            $model->package_expiration = $expirationDate;

            if ($model->save()) {

                if (empty($model->barcode)) {
                    $barCodeTitle = 'pkg_' . $model->id . '_' . time() . '.png';
                    /*$barcodeData = json_encode([
                        'type' => 'PKG',
                        'id' => $model->id,
                    ]);*/
                    $barcodeData = "PKG" . $model->id;
                    BarcodeGenerator::createPngBarcode($barcodeData, 'barcodes/' . $barCodeTitle);
                    $model->barcode = $barCodeTitle;
                    $model->save();
                }

                PackageMeals::deleteAll(['package_id' => $model->id]);
                foreach ($dynamicModel->package_meals as $meal) {
                    $package_meal = new PackageMeals();
                    $package_meal->package_id = $model->id;
                    $package_meal->meal_id = $meal;
                    $package_meal->save();
                }
                GeneralHelper::showSuccessMsg('Package saved successfully');
                //Yii::$app->session->setFlash("success", Yii::t("app", "Data saved successfully"));
                // return $this->redirect('/package');
                return $this->refresh();
            } else {
                GeneralHelper::showErrorMsg('Error occurred while saving data');
                //Yii::$app->session->setFlash("error", Yii::t("app", "Error occurred while saving data"));
                // return $this->redirect('/package');
                return $this->refresh();
            }

        }

        UDPATE_FAILED_CHECKPOINT:
        $products = ArrayHelper::map(Product::find()->all(), "id", "product_title");
        $productsOptions = [];
        foreach ($products as $id => $product_title){
            $productsOptions[$id] = [
                'product-id' => $id
            ];

        }


        $customers = ArrayHelper::map(Customer::find()->innerJoinWith(['user' => function($query){
            return $query->where(['active' => 1]);

        }])->all(), "id", function ($value) {
            return trim($value->user->firstName . " " . $value->user->lastName);
        });
        $meals = ArrayHelper::map(Meals::find()->all(), "id", "meal_title");

        $packageMealDetails = [];
        $packageProducts = PackageProducts::find()->where(['package_id' => $model->id])->all();
        if (!empty($packageProducts)) {
            foreach ($packageProducts as $packageProduct) {
                if($packageProduct->is_selected && $packageProduct->is_selected == 1){
                    $packageMealDetails['selected_products'][date('d-M-Y', strtotime($packageProduct->serve_date))][$packageProduct->meal_id][] = $packageProduct->product_id;
                }else if($packageProduct->is_selected && $packageProduct->is_selected == 2){
                    $packageMealDetails['permanent_products'][date('d-M-Y', strtotime($packageProduct->serve_date))][$packageProduct->meal_id][] = $packageProduct->product_id;
                }

                $packageMealDetails['products'][date('d-M-Y', strtotime($packageProduct->serve_date))][$packageProduct->meal_id][] = $packageProduct->product_id;
                $packageMealDetails['status'][date('d-M-Y', strtotime($packageProduct->serve_date))] = $packageProduct->status;
            }
        }
        //echo "<pre>";print_r($packageMealDetails);echo "</pre>";die('Call');



        $packageDateList = [];
        $period = new \DatePeriod(
            new \DateTime(date('Y-m-d', strtotime($model->package_start_date))),
            new \DateInterval('P1D'),
            new \DateTime(date('Y-m-d', strtotime($model->package_expiration)))
        );
        foreach ($period as $key => $value) {
            $packageDateList[] = $value->format('d-M-Y');
        }
        //echo "<pre>";print_r($products);echo "</pre>";die('Call');
        return $this->render('update', [
            'model' => $model,
            'products' => $products,
            //'productsOptions' => $productsOptions,
            'customers' => $customers,
            'dynamicModel' => $dynamicModel,
            'packageMealDetails' => $packageMealDetails,
            'packageDateList' => $packageDateList,
            'meals' => $meals,
        ]);
    }

    /**
     * Deletes an existing packages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        PackageProducts::deleteAll(['package_id' => $id]);
        GeneralHelper::showSuccessMsg('Package deleted successfully');

        return $this->redirect(['index']);
    }

    /**
     * Finds the packages model based on its     primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return packages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Package::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMeals()
    {

        if (!Yii::$app->request->post()) {
            GeneralHelper::showErrorMsg('Invalid request');
            return $this->redirect('/packages');
        }
        $package_id = Yii::$app->request->post('package');
        $package = Package::findOne($package_id);
        if ($package == null) {
            GeneralHelper::showErrorMsg('Invalid package request');
            return $this->redirect('/packages');
        }

        PackageProducts::deleteAll(['package_id' => $package_id]);

        $package_date_meals = Yii::$app->request->post('package_date_meals');

        $selected_products = $package_date_meals['selected_products'];
        $permanent_products = $package_date_meals['permanent_products'];
        $products = $package_date_meals['products'];
        $statuses = $package_date_meals['status'];

        foreach ($products as $date => $date_products) {
            foreach ($date_products as $meal_id => $date_products) {
                if (!empty($date_products)) {
                    foreach($date_products as $date_product){
                        $selectedItemsArr =  (isset($selected_products[$date][$meal_id])) ? explode(',', $selected_products[$date][$meal_id]) : [];
                        $permanentItemsArr =  (isset($permanent_products[$date][$meal_id])) ? explode(',', $permanent_products[$date][$meal_id]) : [];




                        $packageProduct = new PackageProducts();
                        $packageProduct->package_id = $package_id;
                        $packageProduct->meal_id = $meal_id;
                        $packageProduct->product_id = $date_product;
                        $packageProduct->serve_date = date('Y-m-d', strtotime($date));
                        $packageProduct->is_selected = (in_array($date_product, $selectedItemsArr)) ? 1 : ((in_array($date_product, $permanentItemsArr)) ? 2 : 0);
                        $packageProduct->status = $statuses[$date];
                        $packageProduct->save();
                    }

                }
            }
        }


        GeneralHelper::showSuccessMsg('Meals saved successfully');
        return $this->redirect('/package/update/' . $package_id);
    }

    public function actionTest()
    {
        $generator = new BarcodeGenerator();
        /*$img = $generator->getBarcode('PKG-12345678', $generator::TYPE_UPC_A, 2);
        echo '<img src="data:image/png;base64,' . base64_encode($img) . '">';
        echo "<br />";*/

        $img = $generator->getBarcode('PKG10001001', $generator::TYPE_CODE_128, 2, 60, 20);
        echo '<img src="data:image/png;base64,' . base64_encode($img) . '">';
        echo "<br />";

        /*$img = $generator->getBarcode('PKG-12345678', $generator::TYPE_CODE_128, 2);
        echo '<img src="data:image/png;base64,' . base64_encode($img) . '">';
        echo "<br />";*/
    }

    public function actionExportPackage($id){
        //Export Package: Customer Name, Package Name, Meal Type, Date, Item
        $model = $this->findModel($id);

        //echo "<pre>";print_r($model);echo "</pre>";die('Call');
        //$customer = trim($model->customer->user->firstName." ".$model->customer->user->lastName);
        //$pkgTitle = $model->package_title;
        $customer = 'CST'.$model->customer->id;
        $pkgTitle = 'PKG'.$model->id;
        $packageProducts = PackageProducts::find()->where(['package_id' => $model->id])->all();


        $data = [];
        if(!empty($packageProducts)){
            foreach ($packageProducts as $packageProduct){
                switch ($packageProduct->mealDetail->id){
                    case '1':
                        $mealType = 'B';
                        break;
                    case '2':
                        $mealType = 'L';
                        break;
                    case '3':
                        $mealType = 'D';
                        break;
                }
                $data[] = [
                    $customer, $pkgTitle, 'ITM'.$packageProduct->product->id,$packageProduct->product->product_title, $mealType, $packageProduct->serve_date, ((!empty($packageProduct->is_selected)) ? (string)$packageProduct->is_selected : '')
                ];
                /*
                $data[] = [
                    $customer, $pkgTitle, $packageProduct->product->product_title, $packageProduct->mealDetail->meal_title, $packageProduct->serve_date
                ]; */
            }
        }

/// print '<pre>';
        // var_dump($packageProducts);
//        print_r($data); die;



        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                'Package Details' => [   // Name of the excel sheet
                    'data' => $data,
                    'titles' => [
                        'Customer',
                        'Package',
                        'ItemCode',
                        'Item',
                        'Meal Type',
                        'Serve Date',
                        'Selection Type'
                    ],

                    /*'formats' => [
                        // Either column name or 0-based column index can be used
                        'C' => '#,##0.00',
                        3 => 'dd/mm/yyyy hh:mm:ss',
                    ],*/

                    /*'formatters' => [
                        // Dates and datetimes must be converted to Excel format
                        3 => function ($value, $row, $data) {
                            return \PHPExcel_Shared_Date::PHPToExcel(strtotime($value));
                        },
                    ],*/
                ],


            ]
        ]);
        //echo "<pre>";print_r($file);echo "</pre>";die('Call');
        //$file->send('Test.xlsx');
        $file->send(str_replace(' ', '_', $pkgTitle).'_'.rand(1000,20000).'.xlsx');
        die;
    }

    public function actionExportAllPackages(){
        // max_execution_time = 300;
        set_time_limit(500);

        //Export Package: Customer Name, Package Name, Meal Type, Date, Item
        $pkgsOfTheDay = Package::find()->where([
                '<=', 'package_start_date', date('Y-m-d')
            ])
            ->andWhere([
                '>', 'package_expiration', date('Y-m-d')
            ]) // ->all();
            ->andWhere(['package_status' => 1])->all();

        $data = [];
        //echo "<pre>";print_r($pkgsOfTheDay);echo "</pre>";die('Call');
        foreach ($pkgsOfTheDay as $pkgOfTheDay){
            //$customer = trim($pkgOfTheDay->customer->user->firstName." ".$pkgOfTheDay->customer->user->lastName);
            $customer = 'CST'.$pkgOfTheDay->customer->id;
            //$pkgTitle = $pkgOfTheDay->package_title;
            $pkgTitle = 'PKG'.$pkgOfTheDay->id;
            $packageProducts = PackageProducts::find()->where(['package_id' => $pkgOfTheDay->id])->all();

            if(!empty($packageProducts)){
                foreach ($packageProducts as $packageProduct){
                    switch ($packageProduct->mealDetail->id){
                        case '1':
                            $mealType = 'B';
                            break;
                        case '2':
                            $mealType = 'L';
                            break;
                        case '3':
                            $mealType = 'D';
                            break;
                    }

                    $data[] = [
                        $customer, $pkgTitle, 'ITM'.$packageProduct->product->id,$packageProduct->product->product_title, $mealType, $packageProduct->serve_date, ((!empty($packageProduct->is_selected)) ? (string)$packageProduct->is_selected : '')
                    ];
                }
            }
        }

        // echo "<pre>";print_r($data);echo "</pre>";die('Call');

        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                'Package Details' => [   // Name of the excel sheet
                    'data' => $data,

                    // Set to `false` to suppress the title row
                    'titles' => [
                        'Customer',
                        'Package',
                        'ItemCode',
                        'Item',
                        'Meal Type',
                        'Serve Date',
                        'Selection Type'
                    ],

                    /*'formats' => [
                        // Either column name or 0-based column index can be used
                        'C' => '#,##0.00',
                        3 => 'dd/mm/yyyy hh:mm:ss',
                    ],*/

                    /*'formatters' => [
                        // Dates and datetimes must be converted to Excel format
                        3 => function ($value, $row, $data) {
                            return \PHPExcel_Shared_Date::PHPToExcel(strtotime($value));
                        },
                    ],*/
                ],


            ]
        ]);
        //echo "<pre>";print_r($file);echo "</pre>";die('Call');
        //$file->send('Test.xlsx');
        $fileTitle = 'PackageExport_'.date('d-m-Y');

        $file->send(str_replace(' ', '_', $fileTitle).'_'.rand(1000,20000).'.xlsx');
        die;
    }


    public function actionBulkUpdate(){
        $fileModel = new DynamicModel(["packages"]);
        $fileModel->addRule(["packages"], "file", ["maxSize" => 1024 * 1024, "skipOnEmpty" => false, "extensions" => "xlsx"]);

        $post = Yii::$app->request->post();
        if ($fileModel->load($post)) {
            $requiredHeadings = [
                'Customer',
                'Package',
                'ItemCode',
                'Item',
                'Meal Type',
                'Serve Date',
                'Selection Type',
            ];

            $inputFileType = 'Xlsx';
            $path = Yii::getAlias("@backend") . "/web/uploads/bulkuploads/packages/new/".date("Y")."/".date("m")."/".date("d")."/";

            if(!FileHelper::createDirectory($path, $mode = 0777, $recursive = true))
            {
                GeneralHelper::showErrorMsg('Permission issue while creating a directory');
                return $this->refresh();
            }

            $path = $path."user-" . Yii::$app->user->id . "-" . date('YmdHis');
            $sheetname = 'Package Details';
            $fileUpload = $this->uploadFile($fileModel, $path, "packages");
            if(!$fileUpload){
                GeneralHelper::showErrorMsg('An error occurred while uploading file');
                return $this->refresh();
            }


            try {
                $reader = IOFactory::createReader($inputFileType);

                $spreadsheet = $reader->load($path . ".xlsx");
                $worksheet = $spreadsheet->getSheetByName($sheetname);
                $headings = [];
                $data = [];
                $errors = [];
                $allAttributes = [];

                $templateAttributes = [];

                $dataSize = $worksheet->getHighestRowAndColumn();
                $highestColumnIndex = Coordinate::columnIndexFromString($dataSize['column']);




            } catch (\Exception $e) {
                GeneralHelper::showErrorMsg('Error loading file: ' . $e->getMessage());
                return $this->refresh();
            }

            /*
             * Check headings
             * */

            for ($col = 1; $col <= $highestColumnIndex; ++$col) {
                $cellValue = mb_convert_encoding($worksheet->getCellByColumnAndRow($col, 1)->getValue(), "UTF-8");
                if(isset($requiredHeadings[$col-1]) && $requiredHeadings[$col-1] == $cellValue){
                    $headings[] = $cellValue;
                }else{
                    GeneralHelper::showErrorMsg('Invalid file pattern');
                    return $this->refresh();
                    break;
                }




            }
            if(count(array_diff($requiredHeadings, $headings))){
                GeneralHelper::showErrorMsg('Invalid file pattern');
                return $this->refresh();
            }



            $dataSet = [];
            for ($row = 2; $row <= $dataSize['row']; ++$row) {
                $customerCode = mb_convert_encoding($worksheet->getCellByColumnAndRow(1, $row)->getValue(), "UTF-8");
                $pkgCode = mb_convert_encoding($worksheet->getCellByColumnAndRow(2, $row)->getValue(), "UTF-8");
                $itemCode = mb_convert_encoding($worksheet->getCellByColumnAndRow(3, $row)->getValue(), "UTF-8");
                $mealCode = mb_convert_encoding($worksheet->getCellByColumnAndRow(5, $row)->getValue(), "UTF-8");
                $serveDate = mb_convert_encoding($worksheet->getCellByColumnAndRow(6, $row)->getValue(), "UTF-8");
                $selectionType = mb_convert_encoding($worksheet->getCellByColumnAndRow(7, $row)->getValue(), "UTF-8");
                $dataSet[$pkgCode][$customerCode][$serveDate][$mealCode][] = [
                    'itemCode' => $itemCode,
                    'selectionType' => $selectionType
                ];
            }


            foreach ($dataSet as $pkg => $pkgDetails){
                /*PackageProducts::deleteAll(['package_id' => $pkg]);*/
                $package_id = str_replace('PKG', '', $pkg);
                foreach ($pkgDetails as $customer => $pkgDetail){
                    foreach ($pkgDetail as $serveDate => $meals){
                        foreach ($meals as $mealType => $items){
                            switch ($mealType){
                                case 'B':
                                    $meal_id = 1;
                                    break;
                                case 'L':
                                    $meal_id = 2;
                                    break;
                                case 'D':
                                    $meal_id = 3;
                                    break;
                            }
                            PackageProducts::deleteAll(
                                [
                                    'package_id' => $package_id,
                                    'serve_date' => $serveDate,
                                    'meal_id' => $meal_id
                                ]
                            );

                            foreach ($items as $item){
                                $product_id = str_replace('ITM', '', $item['itemCode']);
                                $packageProduct = new PackageProducts();
                                $packageProduct->package_id = $package_id;
                                $packageProduct->meal_id = $meal_id;
                                $packageProduct->is_selected = (!empty($item['selectionType'])) ? $item['selectionType'] : 0;
                                $packageProduct->serve_date = date('Y-m-d', strtotime($serveDate));
                                $packageProduct->status = 1;
                                $packageProduct->product_id = $product_id;
                                $packageProduct->save();
                            }
                        }
                    }
                }
            }


            GeneralHelper::showSuccessMsg('Packages updated successfully');
            return $this->refresh();
        }

        return $this->render('bulk-update', ['fileModel' => $fileModel]);

    }

    private function uploadFile($mediaModel, $path, $type){
        try {

            $mediaModel = UploadedFile::getInstance($mediaModel, $type);

            return $mediaModel->saveAs($path . '.' . $mediaModel->extension);
        } catch (\Exception $e) {
            return false;
        }
    }

    public function actionPackingList(){
        $dynamicModel = new DynamicModel([
            'packageDate',
            'customer_id'
        ]);
        $dynamicModel->addRule([
            'packageDate'
        ], 'required');
        $dynamicModel->addRule([
            'customer_id'
        ], 'safe');
        $dynamicModel->packageDate = date('d-M-Y');

        $customers = Customer::find()->innerJoinWith(['user' => function($query){
            return $query->where(['active' => 1]);

        }])->all();
        //echo "<pre>";print_r($customers);echo "</pre>";die('Call');

        $customersList = ArrayHelper::map($customers, 'id', function($model){
            return trim($model->user->firstName." ".$model->user->lastName);
        });





        return $this->render('packing-list', [
            'dynamicModel' => $dynamicModel,
            'customersList' => $customersList
        ]);
    }

    public function actionGeneratePackingList(){
        if(Yii::$app->request->isPost){
            $dynamicModel = new DynamicModel([
                'meals',
                'packageDate',
                'customer_id'
            ]);
            $dynamicModel->addRule([
                'meals',
                'packageDate',
            ], 'required');
            $dynamicModel->addRule([
                'customer_id'
            ], 'safe');
            if($dynamicModel->load(Yii::$app->request->post())){
                $selectedMeals = explode(',', $dynamicModel->meals);
                $pkgsOfTheDay = Package::find()->where([
                    '<=', 'package_start_date', date('Y-m-d', strtotime($dynamicModel->packageDate))
                ])
                    ->andWhere([
                        '>', 'package_expiration', date('Y-m-d', strtotime($dynamicModel->packageDate))
                    ])
                    ->andWhere(['package_status' => 1]);

                    //->asArray()
                if(!empty($dynamicModel->customer_id)){

                    $pkgsOfTheDay = $pkgsOfTheDay->andWhere(['in', 'customer_id' , $dynamicModel->customer_id]);
                }
                $pkgsOfTheDay = $pkgsOfTheDay->all();


                $data = [];
                foreach ($pkgsOfTheDay as $pkgOfTheDay){
                    $customer = trim($pkgOfTheDay->customer->user->firstName." ".$pkgOfTheDay->customer->user->lastName);
                    //$customer = 'CST'.$pkgOfTheDay->customer->id;
                    //$pkgTitle = $pkgOfTheDay->package_title;
                    $pkgTitle = 'PKG'.$pkgOfTheDay->id;
                    $packageProducts = PackageProducts::find()->where([

                        'package_id' => $pkgOfTheDay->id

                    ])
                        ->andWhere(['in', 'is_selected', [1,2]])
                        ->andWhere(['in', 'meal_id', $selectedMeals])
                        ->andWhere(['serve_date'=> date('Y-m-d', strtotime($dynamicModel->packageDate))])
                        ->all();
                    //echo "<pre>";print_r($packageProducts);echo "</pre>";die('Call');

                    if(!empty($packageProducts)){
                        foreach ($packageProducts as $packageProduct){
                            switch ($packageProduct->mealDetail->id){
                                case '1':
                                    $mealType = 'Breakfast';
                                    break;
                                case '2':
                                    $mealType = 'Lunch';
                                    break;
                                case '3':
                                    $mealType = 'Dinner';
                                    break;
                            }

                            $data[] = [
                                $customer,  $mealType, $packageProduct->product->product_title, $packageProduct->serve_date
                            ];
                        }
                    }
                }

                $file = \Yii::createObject([
                    'class' => 'codemix\excelexport\ExcelFile',
                    'sheets' => [
                        'Package Details' => [   // Name of the excel sheet
                            'data' => $data,

                            // Set to `false` to suppress the title row
                            'titles' => [
                                'Customer',
                                'Meal Type',
                                'Item',
                                'Serve Date',
                            ],

                            /*'formats' => [
                                // Either column name or 0-based column index can be used
                                'C' => '#,##0.00',
                                3 => 'dd/mm/yyyy hh:mm:ss',
                            ],*/

                            /*'formatters' => [
                                // Dates and datetimes must be converted to Excel format
                                3 => function ($value, $row, $data) {
                                    return \PHPExcel_Shared_Date::PHPToExcel(strtotime($value));
                                },
                            ],*/
                        ],


                    ]
                ]);
                //echo "<pre>";print_r($file);echo "</pre>";die('Call');
                //$file->send('Test.xlsx');
                $fileTitle = 'PackingListExport_'.date('d-m-Y');

                $file->send(str_replace(' ', '_', $fileTitle).'_'.rand(1000,20000).'.xlsx');
                die;


            }else{
                GeneralHelper::showErrorMsg('Invalid request. Please try again');
                return $this->goBack();
            }
        }else{
            GeneralHelper::showErrorMsg('Invalid request. Please try again');
            return $this->goBack();
        }
    }
    
}
