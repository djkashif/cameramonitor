<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VehicleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notification Logs';
$this->params['breadcrumbs'][] = $this->title;
$this->params["headerButtons"][] = Html::a(Yii::t("app", "Advance Search"), "javascript:;", ["class" => "btn btn-primary", "data" => ["toggle" => "modal", "target" => "#searchModal"]]);
echo $this->render("_search_notification_log", [
    "searchModel" => $searchModel,
    "customersList" => $customersList,
]);
?>
<div class="vehicle-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                "label" => Yii::t("app", "Customer"),
                "format" => "raw",
                "value" => function ($data) {
                    return trim($data->customer->user->firstName . " " . $data->customer->user->lastName);
                }
            ],
            [
                "label" => Yii::t("app", "Notification"),
                "format" => "raw",
                "value" => function ($data) {
                    return trim(substr($data->notification, 0, 50))."...";
                }
            ],
            //'notification',
            [
                "attribute" => "status",
                "label" => Yii::t("app", "Status"),
                "format" => "raw",
                "headerOptions" => ["style" => "width: 8%"],
                "contentOptions" => ["class" => "text-center"],
                "value" => function ($data) {
                    $status = "success";
                    $label = Yii::t("app", "Sent");

                    if ($data->status !== 1) {
                        $status = "danger";
                        $label = Yii::t("app", "Not Sent");
                    }
                    $html = "<span class=\"label label-sm label-{$status}\">{$label}</span>";

                    return $html;
                }
            ],
            'response',
            'createdOn'
        ],
    ]); ?>


</div>
