<?php
$this->title = 'Realtime Notifications';

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <?php $form = ActiveForm::begin(['method' => 'get']); ?>
                    <div class="row">
                        <div class="col-md-1">
                            <a href="/notifications/realtime-notification" class="btn btn-default"><i
                                        class="fa fa-refresh"></i>Reset</a>
                        </div>
                        <div class="col-md-3">
                            <?=
                            $form->field($dynamicModel, 'city_id')->widget(\kartik\select2\Select2::classname(), [
                                'data' => $cityList,
                                'options' => ['placeholder' => 'All Cities', 'multiple' => false],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label(false);
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?=
                            $form->field($dynamicModel, 'customer_id')->widget(\kartik\select2\Select2::classname(), [
                                'data' => $customersList,
                                'options' => ['placeholder' => 'All Active Customers', 'multiple' => false],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label(false);
                            ?>

                        </div>
                        <div class="col-md-3">
                            <?=
                            $form->field($dynamicModel, 'pkg_expiry_days')->textInput(['maxlength' => true, 'placeholder' => 'Package Expiry Days'])->label(false);

                            ?>
                        </div>

                        <div class="col-md-1">
                            <button type="button" class="btn btn-info"
                                    onclick="PackageManager.Notifications.fetchCustomers(this)"><i
                                        class="fa fa-check-square">&nbsp;</i>Fetch Customers
                            </button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
<?php
if ($filteredCustomers !== null):
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary direct-chat direct-chat-primary">
                <div class="row box-header with-border">
                    <div class="col-md-6">
                        <h3 class="box-title">Filtered Customers</h3>
                    </div>
                    <div class="col-md-3">
                        <?=
                        \kartik\select2\Select2::widget([
                            'name' => 'notification_template',
                            'data' => $notificationTemplates,
                            'options' => ['placeholder' => 'Select Notification Template', 'multiple' => false],
                            'pluginOptions' => [
                                'allowClear' => false
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-success pull-right"
                                onclick="PackageManager.Notifications.sendNotification()"><i
                                    class="fa fa-plus-square">&nbsp;</i>Send Notification
                        </button>
                    </div>





                    <input type="hidden" name="order-content" value="<?= $filteredCustomersEncoded ?>">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Send Notification</th>


                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($filteredCustomers != null && count($filteredCustomers)) {
                            foreach ($filteredCustomers as $filteredCustomer) {

                                ?>
                                <tr>
                                    <td><?= trim($filteredCustomer['firstName']." ".$filteredCustomer['lastName']) ?></td>
                                    <td><input type="checkbox" name="customers[]" value="<?= $filteredCustomer['id'] ?>" checked></td>

                                </tr>
                                <?php
                            }
                        }
                        ?>


                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>
                <!-- /.box-footer-->
            </div>
        </div>

    </div>

<?php
endif;
?>
<?php
$this->registerCssFile("@web/css/dataTables.bootstrap.min.css", [
    'depends' => [
        \backend\assets\AppAsset::className()
    ]
]);
$this->registerCssFile("@web/css/circle.css", [
    'depends' => [
        \backend\assets\AppAsset::className()
    ]
]);
$this->registerJsFile("@web/js/jquery.dataTables.min.js", [
    'depends' => [
        \backend\assets\AppAsset::className()
    ]
]);
$this->registerJsFile("@web/js/dataTables.bootstrap.min.js", [
    'depends' => [
        \backend\assets\AppAsset::className()
    ]
]);

$this->registerJs("
        $('#example').DataTable();
       ");

?>
