<?php

namespace backend\components;

use common\models\Cities;
use common\models\Customer;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class GeneralHelper
{

    public static $unitOfMeasurement = [
        'mg' => 'Miligram (mg)',
        'g' => 'Gram (g)',
        'kg' => 'Kilogram (kg)',
        'u' => 'Unit',
        'p' => 'Piece',

    ];


    public static function showSidebarMenu($sidebarMenu, $html = "")
    {
        $module = Yii::$app->controller->module->id;
        $controller = Yii::$app->controller->id;
        $permissions = ArrayHelper::map(Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->id), 'name', 'name');

        foreach ($sidebarMenu as $menu) {
            $liAttributes = [];
            $commonPermissions = array_intersect((isset($menu['permissions'])) ? $menu['permissions'] : [], $permissions);
            if (!Yii::$app->user->can('Admin') && !Yii::$app->user->can('Customer') && !count($commonPermissions)) {
                continue;
            }
            if (empty($menu["children"])) {
                if ($module === $menu["module"] && $controller === $menu["controller"]) {
                    $liAttributes["class"] = "active";
                }

                $html .= Html::beginTag("li", $liAttributes);
                $html .= Html::a("
                    " . (!empty($menu["fontawesomeIcon"]) ? "<i class=\"fa fa-{$menu["fontawesomeIcon"]}\"></i>" : "") . "
                    <span>{$menu["title"]}</span>
                ", Url::to(["/{$menu["module"]}/{$menu["controller"]}/{$menu["action"]}"]));
            } else {
                $liAttributes["class"] = "treeview";

                if ($module === $menu["module"]) {
                    $liAttributes["class"] .= " active";
                }

                $html .= Html::beginTag("li", $liAttributes);
                $html .= Html::a("
                    <i class=\"fa fa-{$menu["fontawesomeIcon"]}\"></i>
                    <span>{$menu["title"]}</span>
                    <span class=\"pull-right-container\">
                        <i class=\"fa fa-angle-left pull-right\"></i>
                    </span>
                ", "javascript:;");

                $html .= Html::beginTag("ul", ["class" => "treeview-menu"]);
                $html = static::showSidebarMenu($menu["children"], $html);
                $html .= Html::endTag("ul");
            }

            $html .= Html::endTag("li");
        }

        return $html;
    }

    public static function showSuccessMsg($msg)
    {
        Yii::$app->session->setFlash(\dominus77\sweetalert2\Alert::TYPE_SUCCESS, [
            [
                'title' => 'Success!!',
                'text' => $msg,
                'confirmButtonText' => 'Done!',
            ]
        ]);
    }

    public static function showErrorMsg($msg)
    {
        Yii::$app->session->setFlash(\dominus77\sweetalert2\Alert::TYPE_ERROR, [
            [
                'title' => 'Error!!',
                'text' => $msg,
                'confirmButtonText' => 'Done!',
            ]
        ]);
    }

    public static function fetchBarcodeData($barcode)
    {
        $type = $barcode['type'];
        $id = $barcode['id'];

        switch ($type) {
            case 'CST':
                $data = Customer::findOne($id);
                if (empty($data)) {

                }
                break;
        }
    }

    public static function streamBarcodePdf()
    {

    }

    public static function sendEmail($data)
    {
        //echo "<pre>";print_r($data);echo "</pre>";die('Call');
        $emailer = Yii::$app->emailer->compose();
        $emailer->setFrom([$data["fromEmail"] => $data["fromName"]]);
        $emailer->setTo([$data["toEmail"] => $data["toName"]]);
        $emailer->setSubject($data["subject"]);


        if (!empty($data["htmlBody"])) {
            $emailer->setHtmlBody($data["htmlBody"]);
        }

        if (!empty($data["textBody"])) {
            $emailer->setTextBody($data["textBody"]);
        }
        //echo "<pre>";print_r($emailer);echo "</pre>";die('Call');


        return $emailer->send();
    }


    public static function formatTimeString($timeStamp)
    {
        $str_time = date("Y-m-d H:i:sP", $timeStamp);
        $time = strtotime($str_time);
        $d = new \DateTime($str_time);

        $weekDays = ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
        $months = ['Jan', 'Feb', 'Mar', 'Apr', ' May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

        if ($time > strtotime('-2 minutes')) {
            return 'Just now';
        } elseif ($time > strtotime('-59 minutes')) {
            $min_diff = floor((strtotime('now') - $time) / 60);
            return $min_diff . ' min' . (($min_diff != 1) ? "s" : "") . ' ago';
        } elseif ($time > strtotime('-23 hours')) {
            return 'Today';
            $hour_diff = floor((strtotime('now') - $time) / (60 * 60));
            return $hour_diff . ' hour' . (($hour_diff != 1) ? "s" : "") . ' ago';
        } elseif ($time > strtotime('today')) {
            return 'Today';
            return $d->format('G:i');
        } elseif ($time > strtotime('yesterday')) {
            return 'Yesterday';
            //return 'Yesterday at ' . $d->format('G:i');
        } elseif ($time > strtotime('this week')) {
            return $weekDays[$d->format('N') - 1];
            //return $weekDays[$d->format('N') - 1] . ' at ' . $d->format('G:i');
        } else {
            return $d->format('j') . ' ' . $months[$d->format('n') - 1] .($d->format(' Y'));
                //(($d->format('Y') != date("Y")) ? $d->format(' Y') : "");
                //' at ' . $d->format('G:i');
        }


    }

    public static function diffInDays($earlierDate, $laterDate){
        $earlier = new \DateTime($earlierDate);
        $later = new \DateTime($laterDate);
        $diff = $later->diff($earlier)->format("%a");
        return $diff;
    }

    public static function getCityDetails($id){
        $data = Cities::find()->where(['id' => $id])->one();
        if(!empty($data)){
            return $data;
        }
        return false;
    }
}