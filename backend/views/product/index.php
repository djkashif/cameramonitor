<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VehicleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
$this->params["headerButtons"][] = Html::a(Yii::t("app", "Advance Search"), "javascript:;", ["class" => "btn btn-primary", "data" => ["toggle" => "modal", "target" => "#searchModal"]]);
if (Yii::$app->user->can("createProduct")) {
    $this->params["headerButtons"][] = Html::a(Yii::t("app", "Create"), Url::to(["create"]), ["class" => "btn btn-primary"]);
}
if (Yii::$app->user->can("exportProducts")) {
    $this->params["headerButtons"][] = Html::a(Yii::t("app", "Export Products"), Url::to(["export-all-products"]), ["class" => "btn btn-warning"]);
}

if (Yii::$app->user->can("printProductLabels")) {
    $this->params["headerButtons"][] = Html::a(Yii::t("app", "Print Labels"), Url::to(["prepare-barcode"]), ["class" => "btn btn-default"]);
}

echo $this->render("_search", [
    "searchModel" => $searchModel
]);
if (Yii::$app->user->can("deleteProduct")) {
    $this->registerJs("    
        $(\".btn-confirm\").click(function () {
            var btn = $(this);

            $.showModal({
                title: \"Confirmation\",
                body: btn.attr(\"data-body\"),
                buttons: \"<a href=\\\"\" + btn.attr(\"data-href\") + \"\\\" class=\\\"btn btn-primary btn-confirm\\\" data-method=\\\"post\\\">CONFIRM</button>\"
            });
        });
    ");
}
?>
<div class="vehicle-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'product_title',
            'calories',
            'fat',
            'carbohydrates',
            [
                "attribute" => "protiens",
                "label" => Yii::t("app", "Protein"),
            ],
            [
                "label" => Yii::t("app", "UOM"),
                "format" => "raw",

                "value" => function ($model) {
                    return (isset(\backend\components\GeneralHelper::$unitOfMeasurement[$model->uom])) ? \backend\components\GeneralHelper::$unitOfMeasurement[$model->uom] : 'N/A';
                }
            ],
            [
                "label" => Yii::t("app", "CODE"),
                "format" => "raw",
                "value" => function ($model) {
                    return 'ITM'.$model->id;
                }
            ],
            "price",
            /*[
                "label" => Yii::t("app", "Barcode"),
                "format" => "raw",

                "value" => function ($model) {
                    return HTML::img('/barcodes/' . $model->barcode, ["width" => 200, "height" => 25]);
                }
            ],*/


            'createdOn',
            //'modifiedOn',

            //['class' => 'yii\grid\ActionColumn'],
            [
                "class" => "yii\\grid\\ActionColumn",
                "headerOptions" => ["class" => "text-center", "style" => "width: 8%"],
                "contentOptions" => ["class" => "text-center"],


                "template" => "{update}{delete}",
                "buttons" => [
                    "view" => function ($url, $model) {
                        if (Yii::$app->user->can("viewProducts")) {
                            return Html::a("<span class=\"fa fa-eye\"></span>", $url, ["title" => "View Product"]);
                        }
                    },
                    "update" => function ($url, $model) {
                        if (Yii::$app->user->can("updateProduct")) {
                            return Html::a("<span class=\"glyphicon glyphicon-pencil\"></span>", $url, ["title" => "Update Product"]);
                        }
                    },
                    "delete" => function ($url, $model) {

                        if (Yii::$app->user->can("deleteProduct")) {
                            return Html::a("<span class=\"glyphicon glyphicon-trash\"></span>", "javascript:;", ["title" => "Delete Product", "class" => "btn-confirm", "data" => ["href" => $url, "body" => Yii::t("app", "Are you sure you want to delete product #{$model->id}?") . "<br /><br /><strong>" . Yii::t("app", "THIS ACTION IS IRREVERSIBLE") . "</strong>"]]);
                        }
                    }
                ],
                "urlCreator" => function ($action, $model, $key, $index) {
                    if ($action === "view") {
                        return Url::to(["view", "id" => $model->id]);
                    }
                    if ($action === "update") {
                        return Url::to(["update", "id" => $model->id]);
                    }
                    if ($action === "delete") {
                        return Url::to(["delete", "id" => $model->id]);
                    }
                }
            ]
        ],
    ]); ?>


</div>
