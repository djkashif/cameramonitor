<?php

namespace backend\modules\api\controllers;

use backend\auth\UserIdentity;
use backend\components\GeneralHelper;
use backend\components\TicketsManager;
use common\models\Customer;
use common\models\Meals;
use common\models\NotificationLog;
use common\models\Package;
use common\models\PackageProducts;
use common\models\SupportTickets;
use common\models\User;
use common\models\Users;
use common\models\VehicleTrips;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;

/**
 * Default controller for the `api` module
 */
class CustomerController extends Controller
{
    public static $user = null;
    public static $customer = null;

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->request->isPost) {
            $this->asJson([
                'status' => 0,
                'msg' => 'Invalid request',
                'data' => (object)[],
            ]);
            return false;
        }

        Yii::info(Yii::$app->getRequest()->getRawBody(), 'api_hit');
        if (Yii::$app->request->getHeaders()->get('appToken') == Yii::$app->params['apiHeaderToken']) {
            if ($action->id == 'register' || $action->id == 'login') {
                return parent::beforeAction($action);
            }

            if (Yii::$app->request->getHeaders()->get('authorization')) {
                $token = str_replace('Bearer ', '', Yii::$app->request->getHeaders()->get('authorization'));
                $user = Users::findIdentityByAccessToken($token);
                if (!empty($user)) {
                    self::$user = $user;
                    self::$customer = Customer::findOne(['user_id' => self::$user->id]);
                    return parent::beforeAction($action);
                } else {
                    $this->asJson([
                        'status' => 0,
                        'msg' => 'invalid user token',
                        'data' => (object)[],
                    ]);
                }
            } else {
                $this->asJson([
                    'status' => 0,
                    'msg' => 'User authentication failed.',
                    'data' => (object)[],
                ]);
            }
            return false;


            /*if(){

            }*/


        } else {
            $this->asJson([
                'status' => 0,
                'msg' => 'invalid api token',
                'data' => (object)[],
            ]);
            return false;
        }
        //echo "<pre>";print_r(Yii::$app->params['apiHeaderToken']);echo "</pre>";die('Call');
        return parent::beforeAction($action);
    }

    public function actionUpdateWeight()
    {
        $dynamicModel = new DynamicModel([
            'weight'
        ]);
        $dynamicModel->addRule([
            'weight'
        ], 'required');
        $post['DynamicModel'] = json_decode(Yii::$app->getRequest()->getRawBody(), 1);
        $dynamicModel->load($post);

        if ($dynamicModel->validate()) {
            self::$customer->weight = $dynamicModel->weight;
            self::$customer->save();
            return [
                'status' => 1,
                'msg' => 'Weight updated successfully',
                'data' => (object)[],
            ];
        } else {
            return [
                'status' => 0,
                'msg' => 'validation failed',
                'data' => self::parseErrors($dynamicModel->getErrors()),
            ];
        }
    }

    public function actionPackage()
    {

        $pkgOfTheDay = Package::find()->where([
            'package_status' => 1,
            'customer_id' => self::$customer->id
        ])
            ->andWhere([
                '<=', 'package_start_date', date('Y-m-d')
            ])
            ->andWhere([
                '>', 'package_expiration', date('Y-m-d')
            ])->one();


        $pkgDetails = [
            'hasPkg' => 0,
            'package_id' => null,
            'title' => null,

        ];
        if (($pkgOfTheDay)) {
            $pkgDetails['hasPkg'] = 1;
            $pkgDetails['package_id'] = $pkgOfTheDay->id;
            $pkgDetails['title'] = $pkgOfTheDay->package_title;

            $meals = array_values(ArrayHelper::map($pkgOfTheDay->meals, "id", "meal_id"));


            $pkgDetails['meals_list']['meals'] = [];
            $pkgDetailsNew['meals_list']['meals'] = [];
            $pkgMeals = PackageProducts::find()->where([
                'package_id' => $pkgOfTheDay->id,
                //'meal_id' => $meal,
                //'serve_date' => date('Y-m-d')
            ])->andWhere([
                '>=', 'serve_date', date('Y-m-d')
            ])->andWhere(['in', 'meal_id', $meals])->with(['product', 'mealDetail'])->all();


            foreach ($pkgMeals as $pkgMeal) {
                $pkgDetails['meals_list']['meals'][] = [
                    'meal_type_id' => $pkgMeal->meal_id,
                    'meal_type' => $pkgMeal->mealDetail->meal_title,
                    'meal_title' => $pkgMeal->product->product_title,
                    'fat' => $pkgMeal->product->fat,
                    'carbohydrates' => $pkgMeal->product->carbohydrates,
                    'protiens' => $pkgMeal->product->protiens,
                    'calories' => $pkgMeal->product->calories,
                    'price' => $pkgMeal->product->price,
                    'serve_date' => $pkgMeal->serve_date,
                    'serve_day' => date('l', strtotime($pkgMeal->serve_date)),
                ];

                if (isset($pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date])) {
                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meal_items'][$pkgMeal->mealDetail->meal_title][] = [
                        'meal_title' => $pkgMeal->product->product_title,
                        'selection_type' => ($pkgMeal->is_selected) ? $pkgMeal->is_selected : 0,
                        'fat' => $pkgMeal->product->fat,
                        'carbohydrates' => $pkgMeal->product->carbohydrates,
                        'protiens' => $pkgMeal->product->protiens,
                        'calories' => $pkgMeal->product->calories,
                        'price' => $pkgMeal->product->price,
                    ];

                } else {
                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['serve_date'] = $pkgMeal->serve_date;
                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['serve_day'] = date('l', strtotime($pkgMeal->serve_date));

                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meal_items']['Breakfast'] = null;
                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meal_items']['Lunch'] = null;
                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meal_items']['Dinner'] = null;

                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meal_items'][$pkgMeal->mealDetail->meal_title] = [
                        [
                            'meal_title' => $pkgMeal->product->product_title,
                            'selection_type' => ($pkgMeal->is_selected) ? $pkgMeal->is_selected : 0,
                            'fat' => $pkgMeal->product->fat,
                            'carbohydrates' => $pkgMeal->product->carbohydrates,
                            'protiens' => $pkgMeal->product->protiens,
                            'calories' => $pkgMeal->product->calories,
                            'price' => $pkgMeal->product->price,
                        ]
                    ];
                }
            }
        }

        if (!isset($pkgDetails['meals_list']['meals']) || !count($pkgDetails['meals_list']['meals'])) {
            $pkgDetails['meals_list'] = (object)[];
        }

        if (!isset($pkgDetailsNew['meals_list']['meals']) || !count($pkgDetailsNew['meals_list']['meals'])) {
            $pkgDetailsNew['meals_list'] = (object)[];
        } else {
            $pkgDetailsNew['meals_list']['meals'] = array_values($pkgDetailsNew['meals_list']['meals']);
        }

        return [
            'status' => 1,
            'msg' => 'Success',
            //'data' => $pkgDetails,
            'data' => $pkgDetailsNew
        ];
    }

    public function actionDateWisePackage()
    {

        $dynamicModel = new DynamicModel([
            'date'
        ]);
        $dynamicModel->addRule([
            'date'
        ], 'required');
        $post['DynamicModel'] = json_decode(Yii::$app->getRequest()->getRawBody(), 1);
        $dynamicModel->load($post);

        if($dynamicModel->validate()) {

            $mainMealTypes = Meals::find()->select('meal_title')->all();
            $mainMealTypesLabels = [];
            foreach ($mainMealTypes as $mainMealType){
                $mainMealTypesLabels[] = $mainMealType->meal_title;
            }



            $pkgOfTheDay = Package::find()->where([
                'package_status' => 1,
                'customer_id' => self::$customer->id
            ])
                ->andWhere([
                    '<=', 'package_start_date', date('Y-m-d', strtotime($dynamicModel->date))
                ])
                ->andWhere([
                    '>', 'package_expiration', date('Y-m-d',  strtotime($dynamicModel->date))
                ])->one();


            $pkgDetails = [
                'hasPkg' => 0,
                'package_id' => null,
                'title' => null,

            ];
            if (($pkgOfTheDay)) {
                $pkgDetails['hasPkg'] = 1;
                $pkgDetails['package_id'] = $pkgOfTheDay->id;
                $pkgDetails['title'] = $pkgOfTheDay->package_title;

                $meals = array_values(ArrayHelper::map($pkgOfTheDay->meals, "id", "meal_id"));


                $pkgDetails['meals_list']['meals'] = [];
                $pkgDetailsNew['meals_list']['meals'] = [];
                $pkgMeals = PackageProducts::find()->where([
                    'package_id' => $pkgOfTheDay->id,
                    //'meal_id' => $meal,
                    //'serve_date' => date('Y-m-d')
                ])
                    ->andWhere([
                        '=', 'serve_date', date('Y-m-d', strtotime($dynamicModel->date))
                    ])

                    ->andWhere(['in', 'meal_id', $meals])->with(['product', 'mealDetail'])->all();


                foreach ($pkgMeals as $pkgMeal) {
                    $pkgDetails['meals_list']['meals'][] = [
                        'meal_type_id' => $pkgMeal->meal_id,
                        'meal_type' => $pkgMeal->mealDetail->meal_title,
                        'meal_title' => $pkgMeal->product->product_title,
                        'fat' => $pkgMeal->product->fat,
                        'carbohydrates' => $pkgMeal->product->carbohydrates,
                        'protiens' => $pkgMeal->product->protiens,
                        'calories' => $pkgMeal->product->calories,
                        'price' => $pkgMeal->product->price,
                        'serve_date' => $pkgMeal->serve_date,
                        'serve_day' => date('l', strtotime($pkgMeal->serve_date)),
                    ];

                    if (isset($pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date])) {}
                    else {
                        $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['serve_date'] = $pkgMeal->serve_date;
                        $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['serve_day'] = date('l', strtotime($pkgMeal->serve_date));
                        foreach ($mainMealTypesLabels as $mainMealTypesLabel){
                            $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meal_items'][$mainMealTypesLabel] = null;
                        }

                        /*$pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meal_items']['Breakfast'] = null;
                        $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meal_items']['Lunch'] = null;
                        $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meal_items']['Dinner'] = null;*/
                    }
                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meal_items'][$pkgMeal->mealDetail->meal_title]['meal_title'] = $pkgMeal->mealDetail->meal_title;
                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meal_items'][$pkgMeal->mealDetail->meal_title]['items'][] = [
                        'meal_title' => $pkgMeal->product->product_title,
                        'selection_type' => ($pkgMeal->is_selected) ? $pkgMeal->is_selected : 0,
                        'product_id' => $pkgMeal->product_id,
                        'meal_type' => $pkgMeal->mealDetail->id,
                        'package_id' => $pkgMeal->package_id,
                        'fat' => $pkgMeal->product->fat,
                        'carbohydrates' => $pkgMeal->product->carbohydrates,
                        'protiens' => $pkgMeal->product->protiens,
                        'calories' => $pkgMeal->product->calories,
                        'price' => $pkgMeal->product->price,
                    ];
                }
            }
            //echo "<pre>";print_r($pkgDetailsNew);echo "</pre>";die('Call');

            if (!isset($pkgDetails['meals_list']['meals']) || !count($pkgDetails['meals_list']['meals'])) {
                $pkgDetails['meals_list'] = (object)[];
            }

            if (!isset($pkgDetailsNew['meals_list']['meals']) || !count($pkgDetailsNew['meals_list']['meals'])) {
                $pkgDetailsNew['meals_list'] = (object)[];
            } else {
                $pkgDetailsNew['meals_list']['meals'] = array_values($pkgDetailsNew['meals_list']['meals'])[0];
                $pkgDetailsNew['meals_list'] = $pkgDetailsNew['meals_list']['meals'];
                $pkgDetailsNew['meals_list']['meal_items'] = array_values($pkgDetailsNew['meals_list']['meal_items']);
            }
            //echo "<pre>";print_r($pkgDetailsNew['meals_list']['meal_items']);echo "</pre>";die('Call');

            return [
                'status' => 1,
                'msg' => 'Success',
                //'data' => $pkgDetails,
                'data' => $pkgDetailsNew
            ];

        }else {
            return [
                'status' => 0,
                'msg' => 'validation failed',
                'data' => self::parseErrors($dynamicModel->getErrors()),
            ];
        }
    }

    public function actionPackage_()
    {

        $pkgOfTheDay = Package::find()->where([
            'package_status' => 1,
            'customer_id' => self::$customer->id
        ])
            ->andWhere([
                '<=', 'package_start_date', date('Y-m-d')
            ])
            ->andWhere([
                '>', 'package_expiration', date('Y-m-d')
            ])->one();


        $pkgDetailsNew = $pkgDetails = [
            'hasPkg' => 0,
            'package_id' => null,
            'title' => null,

        ];

        if (($pkgOfTheDay)) {
            $pkgDetailsNew['hasPkg'] = $pkgDetails['hasPkg'] = 1;
            $pkgDetailsNew['package_id'] = $pkgDetails['package_id'] = $pkgOfTheDay->id;
            $pkgDetailsNew['title'] = $pkgDetails['title'] = $pkgOfTheDay->package_title;

            $meals = array_values(ArrayHelper::map($pkgOfTheDay->meals, "id", "meal_id"));


            $pkgDetails['meals_list']['meals'] = [];
            $pkgDetailsNew['meals_list']['meals'] = [];
            $pkgMeals = PackageProducts::find()->where([
                'package_id' => $pkgOfTheDay->id,
                //'meal_id' => $meal,
                //'serve_date' => date('Y-m-d')
            ])->andWhere([
                '>=', 'serve_date', date('Y-m-d')
            ])->andWhere(['in', 'meal_id', $meals])->with(['product', 'mealDetail'])->all();


            foreach ($pkgMeals as $pkgMeal) {
                $pkgDetails['meals_list']['meals'][] = [
                    'meal_type_id' => $pkgMeal->meal_id,
                    'meal_type' => $pkgMeal->mealDetail->meal_title,
                    'meal_title' => $pkgMeal->product->product_title,
                    'fat' => $pkgMeal->product->fat,
                    'carbohydrates' => $pkgMeal->product->carbohydrates,
                    'protiens' => $pkgMeal->product->protiens,
                    'calories' => $pkgMeal->product->calories,
                    'price' => $pkgMeal->product->price,
                    'serve_date' => $pkgMeal->serve_date,

                ];

                if (isset($pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date])) {
                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meals'][] = [
                        'meal_type_id' => $pkgMeal->meal_id,
                        'meal_type' => $pkgMeal->mealDetail->meal_title,
                        'meal_title' => $pkgMeal->product->product_title,
                        'fat' => $pkgMeal->product->fat,
                        'carbohydrates' => $pkgMeal->product->carbohydrates,
                        'protiens' => $pkgMeal->product->protiens,
                        'calories' => $pkgMeal->product->calories,
                        'price' => $pkgMeal->product->price,
                        'serve_date' => $pkgMeal->serve_date,

                    ];

                } else {
                    //$pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['serve_date'] = $pkgMeal->serve_date;
                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['day_name'] = date('l', strtotime($pkgMeal->serve_date));
                    $pkgDetailsNew['meals_list']['meals'][$pkgMeal->serve_date]['meals'] = [
                        [
                            'meal_type_id' => $pkgMeal->meal_id,
                            'meal_type' => $pkgMeal->mealDetail->meal_title,
                            'meal_title' => $pkgMeal->product->product_title,
                            'fat' => $pkgMeal->product->fat,
                            'carbohydrates' => $pkgMeal->product->carbohydrates,
                            'protiens' => $pkgMeal->product->protiens,
                            'calories' => $pkgMeal->product->calories,
                            'price' => $pkgMeal->product->price,
                            'serve_date' => $pkgMeal->serve_date,
                        ]
                    ];
                }
            }
        }

        if (!isset($pkgDetails['meals_list']['meals']) || !count($pkgDetails['meals_list']['meals'])) {
            $pkgDetails['meals_list'] = (object)[];
        }

        if (!isset($pkgDetailsNew['meals_list']['meals']) || !count($pkgDetailsNew['meals_list']['meals'])) {
            $pkgDetailsNew['meals_list'] = (object)[];
        } else {
            $pkgDetailsNew['meals_list']['meals'] = array_values($pkgDetailsNew['meals_list']['meals']);
        }

        return [
            'status' => 1,
            'msg' => 'Success',
            //'data' => $pkgDetails,
            'data' => $pkgDetailsNew
        ];
    }

    public function actionUpdatePackage()
    {
        $dynamicModel = new DynamicModel([
            'product_id',
            'selection_type',
            'date',
            'meal_type',
            'package_id'
        ]);
        $dynamicModel->addRule([
            'product_id',
            'selection_type',
            'date',
            'meal_type',
            'package_id'
        ], 'required');
        $post['DynamicModel'] = json_decode(Yii::$app->getRequest()->getRawBody(), 1);

        $dynamicModel->load($post);

        if ($dynamicModel->validate()) {

            $pkgProduct = PackageProducts::find()->where(
                [
                    'product_id' => $dynamicModel->product_id,
                    'package_id' => $dynamicModel->package_id,
                    'meal_id' => $dynamicModel->meal_type,
                    'serve_date' => $dynamicModel->date
                ]
            )->one();
            if (empty($pkgProduct)) {
                return [
                    'status' => 0,
                    'msg' => 'no such package product found to update',
                    'data' => (object)[],
                ];
            }
            //echo "<pre>";print_r($dynamicModel);echo "</pre>";die('Call');

            //$pkgProduct->product_id = $dynamicModel->product_id;
            //$pkgProduct->is_selected = ($dynamicModel->selection_type == 1) ? 1 : $pkgProduct->is_selected;
            $pkgProduct->is_selected = $dynamicModel->selection_type;
            //echo "<pre>";print_r($pkgProduct);echo "</pre>";die('Call');
            if ($pkgProduct->save()) {
                return [
                    'status' => 1,
                    'msg' => 'Package updated successfully',
                    'data' => (object)[],
                ];
            } else {
                return [
                    'status' => 0,
                    'msg' => 'validation failed',
                    'data' => self::parseErrors($pkgProduct->getErrors()),
                ];
            }
        } else {
            return [
                'status' => 0,
                'msg' => 'validation failed',
                'data' => self::parseErrors($dynamicModel->getErrors()),
            ];
        }
    }

    public function actionConsumption()
    {
        $dynamicModel = new DynamicModel([
            'duration_type',
            'report_type',
            'date_from',
            'date_to',
        ]);
        $dynamicModel->addRule([
            'duration_type',
            'report_type',
        ], 'required');
        $dynamicModel->addRule([
            'date_from',
            'date_to',
        ], 'safe');
        $post['DynamicModel'] = json_decode(Yii::$app->getRequest()->getRawBody(), 1);
        $dynamicModel->load($post);

        if ($dynamicModel->validate()) {

            //$objectQuery = PackageProducts::find();
            switch ($dynamicModel->duration_type) {
                case '1':
                    $dateStart = date('Y-m-d') . " 00:00:00";
                    $dateEnd = date('Y-m-d') . " 23:59:59";
                    break;
                case '2':
                    $dateStart = date('Y-m-d', strtotime("-1 days")) . " 00:00:00";;
                    $dateEnd = date('Y-m-d') . " 23:59:59";

                    break;
                case '3':
                    $dateStart = date('Y-m-d', strtotime("-7 days")) . " 00:00:00";
                    $dateEnd = date('Y-m-d') . " 23:59:59";
                    break;
                case '4':
                    $dateStart = date('Y-m-d', strtotime($dynamicModel->date_from)) . " 00:00:00";
                    $dateEnd = date('Y-m-d', strtotime($dynamicModel->date_to)) . " 23:59:59";
                    break;
                default:
                    break;
            }
            $reportType = (in_array($dynamicModel->report_type, ['summary', 'detail'])) ? $dynamicModel->report_type : 'detail';

            switch ($reportType) {
                case 'detail':
                    $query__ = "SELECT 
                        p.id, 
                        p.product_title, 
                        sum(p.calories) as calories, 
                        sum(p.fat) as fat, 
                        sum(p.carbohydrates) as carbohydrates, 
                        sum(p.protiens) as protiens 
                        
                        
                        FROM `orders` o 
                        inner join order_meals om on om.order_id = o.id
                        INNER join order_meal_items omi on omi.order_meal_id = om.id
                        inner JOIN product p on p.id = omi.product_id
                        
                        WHERE o.customer_id = " . self::$customer->id . "
                        -- WHERE o.customer_id = 4
                        AND o.createdOn >= '{$dateStart}' AND o.createdOn <= '{$dateEnd}' 
                        GROUP by omi.product_id";

                    $query = "SELECT 
                        
                        DATE_FORMAT(omi.createdOn, '%Y-%m-%d') as serve_date,
                        p.product_title,
                        m.meal_title,
                        sum(p.calories) as calories, 
                        sum(p.fat) as fat, 
                        sum(p.carbohydrates) as carbohydrates, 
                        sum(p.protiens) as protiens 
                        
                        
                        FROM `orders` o 
                        inner join order_meals om on om.order_id = o.id
                        INNER join order_meal_items omi on omi.order_meal_id = om.id
                        inner JOIN product p on p.id = omi.product_id
                        inner join meals m on om.meal_id = m.id
                        
                        WHERE o.customer_id = " . self::$customer->id . "
                        -- WHERE o.customer_id = 4
                        AND o.createdOn >= '{$dateStart}' AND o.createdOn <= '{$dateEnd}'
                        
                        GROUP by DATE_FORMAT(omi.createdOn, '%Y-%m-%d'), m.id,omi.product_id
                        order by serve_date, m.meal_title";

                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand($query);
                    $result = $command->queryAll();
                    if (empty($result)) {
                        return [
                            'status' => 0,
                            'msg' => 'no data found',
                            'data' => (object)[],
                        ];
                    }

                    $finalResult = [];
                    foreach ($result as $res){
                        if(!isset($finalResult[$res['serve_date']][$res['meal_title']])){
                            $finalResult[$res['serve_date']][$res['meal_title']] = [
                                'serve_date' => $res['serve_date'],
                                'meal_title' => $res['meal_title'],
                                'meals' => []
                            ];
                        }
                        $finalResult[$res['serve_date']][$res['meal_title']]['meals'][] = [
                            'product_title' => $res['product_title'],
                            'calories' => $res['calories'],
                            'fat' => $res['fat'],
                            'carbohydrates' => $res['carbohydrates'],
                            'protiens' => $res['protiens']
                        ];
                    }
                    $finalResult2 = [];
                    array_map(function($fr) use (&$finalResult2){
                        $finalResult2[] = array_values($fr);
                    }, $finalResult);

                    $finalResult2 = (count($finalResult2)) ? $finalResult2[0] : $finalResult2;

                    //$finalResult = array_values($finalResult);
                    return [
                        'status' => 1,
                        'msg' => 'Success',
                        'data' => [
                            'consumption_list' => $finalResult2
                        ],
                    ];


                    break;
                case 'summary':
                    $query = "SELECT 
                        m.meal_title,
                        sum(p.calories) as calories, 
                        sum(p.fat) as fat, 
                        sum(p.carbohydrates) as carbohydrates, 
                        sum(p.protiens) as protiens 
                        
                        
                        FROM `orders` o 
                        inner join order_meals om on om.order_id = o.id
                        INNER join order_meal_items omi on omi.order_meal_id = om.id
                        inner JOIN product p on p.id = omi.product_id
                        inner join meals m on om.meal_id = m.id
                        WHERE o.customer_id = " . self::$customer->id . "
                        AND o.createdOn >= '{$dateStart}' AND o.createdOn <= '{$dateEnd}'
                        GROUP by  m.id";

                    $connection = Yii::$app->getDb();
                    $command = $connection->createCommand($query);
                    $result = $command->queryAll();
                    if (empty($result)) {
                        return [
                            'status' => 0,
                            'msg' => 'no data found',
                            'data' => (object)[],
                        ];
                    }

                    return [
                        'status' => 1,
                        'msg' => 'Success',
                        'data' => [
                            'consumption_list' => $result
                        ],
                    ];

                    break;

            }


        } else {
            return [
                'status' => 0,
                'msg' => 'validation failed',
                'data' => self::parseErrors($dynamicModel->getErrors()),
            ];
        }
    }

    public function actionCreateSupportTicket()
    {
        $dynamicModel = new DynamicModel([
            'title',
            'department',
            'description'
        ]);
        $dynamicModel->addRule([
            'title',
            'department',
            'description',
        ], 'required');
        $post['DynamicModel'] = json_decode(Yii::$app->getRequest()->getRawBody(), 1);
        $dynamicModel->load($post);

        if ($dynamicModel->validate()) {

            $ticket = TicketsManager::createNewTicket([
                'ticket_title' => $dynamicModel->title,
                'ticket_dept' => $dynamicModel->department,
                'ticket_description' => $dynamicModel->description,
                'created_by' => self::$customer->id,
            ]);
            if ($ticket) {
                return [
                    'status' => 1,
                    'msg' => 'Your ticket # ' . $ticket . ' generated successfully. Customer Support team will contact you shortly. Thank You.',
                    'data' => (object)[],
                ];
            } else {
                return [
                    'status' => 0,
                    'msg' => 'Failed to generate ticket. Please try agian',
                    'data' => [],
                ];
            }
        } else {
            return [
                'status' => 0,
                'msg' => 'validation failed',
                'data' => self::parseErrors($dynamicModel->getErrors()),
            ];
        }
    }
    public function actionSupportTicketList()
    {
        $SupportTickets = SupportTickets::find()->where(['created_by' => self::$customer->id])->orderBy('createdOn asc, modifiedOn asc')->asArray()->all();
        $SupportTicketList = [];
        foreach ($SupportTickets as $SupportTicket){
            $SupportTicketList[] = [
                'msg' => $SupportTicket['ticket_description'],
                'time' => $SupportTicket['createdOn'],
                'msg_type' => 'user',
            ];
            if(!empty($SupportTicket['ticket_response'])){
                $SupportTicketList[] = [
                    'msg' => $SupportTicket['ticket_response'],
                    'time' => $SupportTicket['modifiedOn'],
                    'msg_type' => 'admin',
                ];
            }
        }
        return [
            'status' => 1,
            'msg' => '',
            'data' => [
                'msg_list' => $SupportTicketList
            ],
        ];
    }

    public function actionMetaData()
    {
        $metaDataFields = [
            'gender',
            'weight',
            'activity_level',
            'goal_id',
            'goal_data',
            'height',
            'dob',
            'firstName',
            'lastName',
            'contactNumber',
            'address',
            'city_id',
        ];
        $dynamicModel = new DynamicModel($metaDataFields);
        $dynamicModel->addRule($metaDataFields, 'safe');
        $post['DynamicModel'] = json_decode(Yii::$app->getRequest()->getRawBody(), 1);

        $dynamicModel->load($post);

        $performed = false;
        $performedUser = false;
        if ($dynamicModel->validate()) {
            if (!empty($dynamicModel->gender)) {
                $performed = true;
                self::$customer->gender = $dynamicModel->gender;
            }

            if (!empty($dynamicModel->weight)) {
                $performed = true;
                self::$customer->weight = $dynamicModel->weight;
            }

            if (!empty($dynamicModel->activity_level)) {
                $performed = true;
                self::$customer->activity_level = $dynamicModel->activity_level;
            }

            if (!empty($dynamicModel->goal_id)) {
                $performed = true;
                self::$customer->goal_id = $dynamicModel->goal_id;
            }

            if (!empty($dynamicModel->goal_data)) {
                $performed = true;
                self::$customer->goal_data = $dynamicModel->goal_data;
            }

            if (!empty($dynamicModel->height)) {
                $performed = true;
                self::$customer->height = $dynamicModel->height;
            }
            if (!empty($dynamicModel->dob)) {
                $performed = true;
                self::$customer->dob = date('Y-m-d', strtotime($dynamicModel->dob));
            }

            if (!empty($dynamicModel->firstName)) {
                $performedUser = true;
                $performed = true;
                self::$user->firstName = $dynamicModel->firstName;
            }

            if (!empty($dynamicModel->lastName)) {
                $performedUser = true;
                $performed = true;
                self::$user->lastName = $dynamicModel->lastName;
            }
            if (!empty($dynamicModel->contactNumber)) {
                $performedUser = true;
                $performed = true;
                self::$customer->contactNumber = $dynamicModel->contactNumber;
            }

            if (!empty($dynamicModel->address)) {
                $performedUser = true;
                $performed = true;
                self::$customer->address = $dynamicModel->address;
            }

            if (!empty($dynamicModel->city_id)) {
                $performedUser = true;
                $performed = true;
                self::$customer->city_id = $dynamicModel->city_id;
            }


            if ($performed) {
                if (!self::$customer->save() || !self::$user->save()) {
                    return [
                        'status' => 0,
                        'msg' => 'Failed to save data. Please try again',
                        'data' => (object)[],
                    ];
                }
                return [
                    'status' => 1,
                    'msg' => 'Data saved successfully',
                    'data' => (object)[],
                ];
            } else {
                return [
                    'status' => 0,
                    'msg' => 'Invalid meta data',
                    'data' => (object)[],
                ];
            }

        } else {
            return [
                'status' => 0,
                'msg' => 'validation failed',
                'data' => self::parseErrors($dynamicModel->getErrors()),
            ];
        }
    }

    public function actionWeightHistory()
    {
        $history = self::$customer->weightHistory;
        if (count($history)) {
            $current = $history[0];
            $data = [
                'current' => [
                    'weight' => $current->weight,
                    'date' => $current->date,
                    'since' => GeneralHelper::formatTimeString(strtotime($current->date))
                ],
                'history' => []
            ];
            //array_shift($history);
            if (count($history) > 1) {
                foreach ($history as $index => $hist) {

                    $descriptionText = '';
                    $growth = '';
                    $weightDiff = '';
                    $perDayText = '';
                    if ($index < count($history) - 1) {
                        $weightDiff = $hist->weight - $history[$index + 1]->weight;
                        $growth = ($weightDiff > 0) ? 'up' : (($weightDiff < 0) ? 'down' : '');

                        switch ($growth) {
                            case 'up':
                                $descriptionText = "Gained {$weightDiff} Kilogram(s) since " . date('d M Y', strtotime($history[$index + 1]->date));
                                $perDay = (($weightDiff) / GeneralHelper::diffInDays($hist->date, $history[$index + 1]->date));
                                $perDayText = "Gained " . $perDay . " kilogram(s) per day";

                                break;
                            case 'down':
                                $weightDiff = $weightDiff * -1;
                                $descriptionText = "Lost {$weightDiff} Kilogram(s) since " . date('d M Y', strtotime($history[$index + 1]->date));
                                $perDay = number_format(($weightDiff) / GeneralHelper::diffInDays($hist->date, $history[$index + 1]->date), 2);
                                $perDayText = "Lost " . $perDay . " kilogram(s) per day";
                                break;
                        }

                    }
                    $data['history'][] = [
                        'weight' => $hist->weight,
                        'date' => $hist->date,
                        'descriptionText' => $descriptionText,
                        'perDayText' => $perDayText,
                        'growth' => $growth,
                        //'since' => GeneralHelper::formatTimeString(strtotime($hist->date))
                    ];
                }
            }

        } else {
            $data = (object)[];
        }

        return [
            'status' => count($data) ? 1 : 0,
            'msg' => (count($data)) ? 'Success' : 'Weight history not found',
            'data' => $data,
        ];
    }

    public function actionNotifications()
    {
        $notifications = NotificationLog::find()->where(['customer_id' => self::$customer->id, 'read_status' => 0])->orderBy([
            'createdOn' => SORT_DESC
        ])->all();
        $notificationList = [];
        array_map(function ($notification) use (&$notificationList) {
            $notificationList['notifications'][] = [
                'id' => $notification->id,
                'notification' => $notification->notification,
                'date_time' => $notification->createdOn,
            ];
        }, $notifications);

        $notificationList = (count($notificationList)) ? $notificationList : (object)[];
        return [
            'status' => 1,
            'msg' => 'Success',
            'data' => $notificationList
        ];

    }

    public function actionReadNotification()
    {
        $dynamicModel = new DynamicModel([
            'notification_id'
        ]);
        $dynamicModel->addRule([
            'notification_id'
        ], 'required');
        $post['DynamicModel'] = json_decode(Yii::$app->getRequest()->getRawBody(), 1);
        $dynamicModel->load($post);

        if ($dynamicModel->validate()) {
            $notification = NotificationLog::find()->where(['id' => $dynamicModel->notification_id, 'customer_id' => self::$customer->id])->one();
            if (isset($notification->id)) {
                $notification->read_status = 1;
                $notification->save();
                return [
                    'status' => 1,
                    'msg' => 'Notification marked as read successfully',
                    'data' => (object)[],
                ];
            } else {
                return [
                    'status' => 0,
                    'msg' => 'Invalid notification id',
                    'data' => (object)[],
                ];
            }
        } else {
            return [
                'status' => 0,
                'msg' => 'validation failed',
                'data' => self::parseErrors($dynamicModel->getErrors()),
            ];
        }
    }

    public function actionUpdateFcmToken()
    {
        $dynamicModel = new DynamicModel([
            'fcm_token'
        ]);
        $dynamicModel->addRule([
            'fcm_token'
        ], 'required');
        $post['DynamicModel'] = json_decode(Yii::$app->getRequest()->getRawBody(), 1);
        $dynamicModel->load($post);

        if ($dynamicModel->validate()) {
            self::$customer->firebaseToken = $dynamicModel->fcm_token;
            self::$customer->save();
            return [
                'status' => 1,
                'msg' => 'FCM token updated successfully',
                'data' => (object)[],
            ];
        } else {
            return [
                'status' => 0,
                'msg' => 'validation failed',
                'data' => self::parseErrors($dynamicModel->getErrors()),
            ];
        }
    }


    private function parseErrors($errors)
    {
        $errorWrapper = [];
        foreach ($errors as $error_key => $error) {
            $errorWrapper[$error_key] = implode(', ', $error);
        }
        return $errorWrapper;
    }
}
