<?php

use yii\helpers\Html;


$this->title = 'Create Customer';
$this->params['breadcrumbs'][] = ['label' => 'Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-create">
    <?= $this->render('_form', [
        'customerModel' => $customerModel,
        'userModel' => $userModel,
        'countryList' => $countryList,
        'dynamicModel' => $dynamicModel,
        'stateList' => $stateList,
    ]) ?>

</div>
