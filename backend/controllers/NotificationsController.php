<?php

namespace backend\controllers;


use backend\components\FcmManager;
use backend\components\GeneralHelper;
use backend\components\NotificationsManager;
use common\models\Cities;
use common\models\Customer;
use common\models\NotificationLogSearch;
use common\models\NotificationTemplates;
use common\models\NotificationTemplatesSearch;

use Yii;

use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class NotificationsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Admin"],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['viewAllTemplates', 'viewTemplates']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['createNotificationTemplate']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteNotificationTemplate']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => ['updateNotificationTemplate']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['notification-log'],
                        'roles' => ['notificationsLog']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['realtime-notification'],
                        'roles' => ['sendNotification']
                    ],
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication");
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    public function actionIndex()
    {
        $searchModel = new NotificationTemplatesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCreate()
    {
        $model = new NotificationTemplates();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->save();
            GeneralHelper::showSuccessMsg('Template saved successfully');
            return $this->redirect('/notifications');
        }
        $model->status = 1;
        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->save();
            GeneralHelper::showSuccessMsg('Template saved successfully');
            //Yii::$app->session->setFlash("success", Yii::t("app", "Data saved successfully"));
            return $this->redirect('/notifications');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        GeneralHelper::showSuccessMsg('Notification template deleted successfully');

        return $this->redirect(['index']);
    }


    public function actionRealtimeNotification(){
        $dynamicModel = new DynamicModel([
            'customer_id',
            'pkg_expiry_days',
            'city_id'

        ]);
        $dynamicModel->addRule([
            'customer_id',
            'pkg_expiry_days',
            'city_id',
        ], 'safe');


        $filteredCustomers = null;
        $filteredCustomersEncoded = '';
        $notificationTemplates = [];
        if (Yii::$app->request->get() && $dynamicModel->load(Yii::$app->request->get())) {
            $query = "SELECT c.id, u.firstName, u.lastName from package p
                        inner join customer c on c.id = p.customer_id
                        inner join users u on u.id = c.user_id
                        where u.active = 1";

            if(!empty($dynamicModel->pkg_expiry_days)){
                $query .= " AND p.package_expiration between now() and adddate(now(), INTERVAL {$dynamicModel->pkg_expiry_days} DAY) ";
            }

            if(!empty($dynamicModel->customer_id)){
                $query .= " AND c.id = '{$dynamicModel->customer_id}' ";
            }

            if(!empty($dynamicModel->city_id)){
                $query .= " AND c.city_id = '{$dynamicModel->city_id}' ";
            }

            $connection = Yii::$app->getDb();
            $command = $connection->createCommand($query);
            $filteredCustomers = $command->queryAll();
            $filteredCustomersEncoded = base64_encode(json_encode($filteredCustomersEncoded));

            $notificationTemplates = ArrayHelper::map(NotificationTemplates::find()->where(['status' => 1])->all(), "id", "template_title");
        }
        //die('Call');


        $customers = Customer::find()->innerJoinWith(['user' => function($query){
            return $query->where(['active' => 1]);

        }])->all();
        $customersList = ArrayHelper::map($customers, 'id', function ($model) {
            return trim($model->user->firstName . " " . $model->user->lastName);
        });

        $cityList = ArrayHelper::map(Cities::find()->all(), "id", "name");

        return $this->render('realtime-notifications', [
            'dynamicModel' => $dynamicModel,
            'customersList' => $customersList,
            "cityList" => $cityList,
            "filteredCustomers" => $filteredCustomers,
            "filteredCustomersEncoded" => $filteredCustomersEncoded,
            "notificationTemplates" => $notificationTemplates,
        ]);


    }

    /**
     * Finds the notifications model based on its     primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return notifications the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NotificationTemplates::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSendNotification(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();

        $notificationTemplate = NotificationTemplates::find()->where(['id' => $data['template']])->one();
        foreach ($data['customer'] as $customerId){
            $customer = Customer::find()->where(['id' => $customerId])->one();
            if(isset($customer->id)){

                $parsedTemplate = NotificationsManager::parseTemplate($notificationTemplate->template_content, $customer);
                if(!empty($customer->firebaseToken)){
                    $fcmManager = new FcmManager();
                    /*$fcmManager->set_rid([
                        $customer->firebaseToken
                    ]); //Where $rid is an array of registration ids, atleast one is required*/
                    $fcmManager->set_to($customer->firebaseToken);
                    $fcmManager->set_msg('Notification', $parsedTemplate, $subtitle = null, $ticketText = null, $vibrate = true, $sound = 1);
                    $response = $fcmManager->send();
                    NotificationsManager::_logNotification($customer->id, $parsedTemplate, 1, 'Success : '.json_encode($response));
                }else{
                    NotificationsManager::_logNotification($customer->id, $parsedTemplate, 2, 'Firebase Token is not set for this user');
                }
            }
        }
        return [
            'status' => 1,
            'data' => [],
            'msg' => 'Sent successfully',
        ];
    }

    public function actionNotificationLog(){
        $searchModel = new NotificationLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $customers = Customer::find()->innerJoinWith(['user' => function($query){
            return $query->where(['active' => 1]);

        }])->all();
        $customersList = ArrayHelper::map($customers, 'id', function ($model) {
            return trim($model->user->firstName . " " . $model->user->lastName);
        });


        return $this->render('notification-log', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'customersList' => $customersList,
        ]);
    }

}
