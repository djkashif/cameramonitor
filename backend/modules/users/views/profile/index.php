<?php
use backend\components\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t("app", "Profile");

?>
<div class="box">
    <?php $form = ActiveForm::begin(); ?>
        <div class="box-body with-border">
            <div class="row">
                <div class="col-md-4">
                    <?php echo $form->field($model, "firstName")->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->field($model, "lastName")->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->field($model, "email")->textInput(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?php echo $form->field($changePasswordModel, "oldPassword")->passwordInput(); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->field($changePasswordModel, "newPassword")->passwordInput(); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->field($changePasswordModel, "confirmPassword")->passwordInput(); ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <?php echo Html::a(Yii::t("app", "Back"), Url::to(["index"]), ["class" => "btn btn-default"]); ?>
            <?php echo Html::submitButton(Yii::t("app", "Submit"), ["class" => "btn btn-primary pull-right"]); ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>