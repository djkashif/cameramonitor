<?php
use \backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language; ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset; ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo Html::csrfMetaTags(); ?>
    <title><?php echo Html::encode($this->title); ?></title>
    <?php $this->head(); ?>
</head>
<body class="hold-transition login-page">
<?php $this->beginBody(); ?>
<div class="login-box">
    <div class="login-logo">
        <a href="<?php echo Url::to(["/"]); ?>">
            <?= Yii::$app->params['appFullNameFormatted'] ?>
        </a>
    </div>
    <div class="login-box-body">

        <?php echo $content; ?>
    </div>
</div>
<?= \dominus77\sweetalert2\Alert::widget(['useSessionFlash' => true]) ?>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>