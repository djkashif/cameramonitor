<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <!-- form start -->
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">


                <?= $form->field($model, 'template_title')->textInput(['maxlength' => true, 'placeholder' => 'Title']) ?>

                <?= $form->field($model, 'template_content')->textarea(['maxlength' => true, 'placeholder' => 'Template Content']) ?>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'status', [])->widget(\kartik\select2\Select2::classname(), [
                            'data' => [
                                    '1' => 'Active',
                                    '0' => 'Inactive',
                            ],
                            'options' => ['placeholder' => 'Status'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Status');
                        ?>
                    </div>

                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
        <!-- /.box -->
    </div>
    <!--/.col (left) -->

</div>


