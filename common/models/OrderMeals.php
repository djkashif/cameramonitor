<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_meals".
 *
 * @property int $id
 * @property int $order_id
 * @property int $meal_id
 * @property double $grandTotal
 * @property string $createdOn
 * @property string $modifiedOn
 */
class OrderMeals extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_meals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'meal_id', 'grandTotal'], 'required'],
            [['order_id', 'meal_id'], 'integer'],
            [['grandTotal'], 'number'],
            [['createdOn', 'modifiedOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'meal_id' => 'Meal ID',
            'grandTotal' => 'Grand Total',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }

    public function getMeal(){
        return $this->hasOne(Meals::className(), ["id" => "meal_id"]);
    }

    public function getOrderMealItems(){
        return $this->hasMany(OrderMealItems::className(), ["order_meal_id" => "id"]);
    }

    public function getOrder(){
        return $this->hasOne(Orders::className(), ["id" => "order_id"]);
    }
}
