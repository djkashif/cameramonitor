<?php
namespace common\models;


use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord as YiiActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

class ActiveRecord extends YiiActiveRecord
{
    public static function find()
    {
        return Yii::createObject(ActiveQuery::className(), [get_called_class()]);
    }



    public function beforeSave($insert)
    {
        if (ArrayHelper::keyExists("user_id", $this->attributes) && (isset(Yii::$app->user) && empty($this->user_id))) {
            $this->user_id = isset(Yii::$app->user->identity->id) ? Yii::$app->user->identity->id : null;
        }

        if (ArrayHelper::keyExists("createdOn", $this->attributes) && !isset($this->attributes["createdOn"]) && $this->isNewRecord) {
            $this->createdOn = new Expression("NOW()");
        }

        if (ArrayHelper::keyExists("modifiedOn", $this->attributes) && !$this->isNewRecord) {
            $this->modifiedOn = new Expression("NOW()");
        }

        if (ArrayHelper::keyExists("active", $this->attributes) && !isset($this->attributes["active"])) {
            $this->active = 1;
        }

        if (ArrayHelper::keyExists("deleted", $this->attributes) && !isset($this->deleted)) {
            $this->deleted = 0;
        }
        
        if (ArrayHelper::keyExists("language", $this->attributes) && empty($this->language)) {
            $this->language = Yii::$app->language;
        }

        /**
         * This loop is to replace all empty values with null as it takes up
         * less space hen filtering as compared to empty values..
         */
        foreach ($this->attributes as $key => $value) {
            if ($value === "") {
                $this->$key = null;
            }
        }

        return parent::beforeSave($insert);
    }

    public function beforeValidate()
    {
        if (ArrayHelper::keyExists("language", $this->attributes) && empty($this->language)) {
            $this->language = Yii::$app->language;
        }
        
        if (ArrayHelper::keyExists("deleted", $this->attributes) && empty($this->deleted)) {
            $this->deleted = 0;
        }

        return parent::beforeValidate();
    }
}