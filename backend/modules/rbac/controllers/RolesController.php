<?php
namespace backend\modules\rbac\controllers;

use backend\modules\rbac\models\AuthItem;
use backend\modules\rbac\search\RolesSearch;
use Yii;
use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;

class RolesController extends Controller
{
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Admin"],
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication/logout");
                }
            ]
        ];
    }
    
    public function actionIndex()
    {
        $searchModel = new RolesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 20;

        return $this->render("index", [
            "searchModel" => $searchModel,
            "dataProvider" => $dataProvider
        ]);
    }
    
    public function actionCreate()
    {
        $model = new DynamicModel(["title"]);
        $model->addRule("title", "required");
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $auth = Yii::$app->authManager;
            $auth->add($auth->createRole($model->title));
            
            Yii::$app->session->setFlash("success", Yii::t("app", "Data saved successfully"));
            return $this->redirect(Url::to(["index"]));
        }
        
        return $this->render("form", [
            "model" => $model
        ]);
    }
    
    public function actionAssignpermissions($id)
    {
        $model = $this->__findModel($id);
        
        if ($model === null) {
            Yii::$app->session->setFlash("error", Yii::t("app", "The requested url could not be found"));
            return $this->redirect(Url::to(["index"]));
        }
        
        $auth = Yii::$app->authManager;
        $permissionsList = $auth->getPermissions();
        $rolePermissionsList = $auth->getPermissionsByRole($model->name);
        $postPermissions = Yii::$app->request->post("permissions");
        $rolePermissionsListKeys = array_keys($rolePermissionsList);


        
        if ($postPermissions) {

            $role = $auth->getRole($model->name);
            foreach ($rolePermissionsListKeys as $rolePermissionsListKey){
                $permissionRBAC = $auth->getPermission($rolePermissionsListKey);
                $auth->removeChild($role, $permissionRBAC);
            }

            foreach ($postPermissions as $postPermissionKey => $postPermissionValue) {
                $permissionRBAC = $auth->getPermission($postPermissionKey);

                $auth->addChild($role, $permissionRBAC);
            }

            //$newPermissions = array_diff(array_keys($postPermissions), array_keys($rolePermissionsList));



            
            /*if (!empty($newPermissions)) {
                $role = $auth->getRole($model->name);
                foreach ($rolePermissionsList as $rolePermissionsL){
                    $auth->removeChild($role, $rolePermissionsL);
                }

                foreach ($postPermissions as $postPermissionKey => $postPermissionValue) {
                    $permissionRBAC = $auth->getPermission($postPermissionKey);

                    $auth->addChild($role, $permissionRBAC);
                }
            }*/
            
            Yii::$app->session->setFlash("success", Yii::t("app", "Permissions assigned successfully"));
            return $this->redirect(Url::to(["index"]));
        }
        
        return $this->render("assignpermissionsform", [
            "permissionsList" => $permissionsList,
            "rolePermissionsList" => $rolePermissionsList
        ]);
    }
    
    private function __findModel($id)
    {
        return AuthItem::findOne(["name" => $id, "type" => 1]);
    }
}