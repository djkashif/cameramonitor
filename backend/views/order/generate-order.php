<?php
$this->title = 'Generate New Orders';

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <?php $form = ActiveForm::begin(['method' => 'get']); ?>
                <div class="row">
                    <div class="col-md-1">
                        <a href="/order/generate-order" class="btn btn-default"><i class="fa fa-refresh"></i>Reset</a>
                    </div>
                    <div class="col-md-3">
                        <?=
                        $form->field($dynamicModel, 'date')->widget(\kartik\date\DatePicker::classname(), [
                                'options' => ['placeholder' => 'Enter start date ...'],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd'
                                ]
                        ])->label(false);

                        ?>
                    </div>
                    <div class="col-md-3">
                        <?=
                        $form->field($dynamicModel, 'customer_id')->widget(\kartik\select2\Select2::classname(), [
                            'data' => $customersList,
                            'options' => ['placeholder' => 'All Active Customers', 'multiple' => false],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false);
                        ?>

                    </div>
                    <div class="col-md-3">
                        <div class="btn-group">
                            <?php
                            foreach ($meals as $mealId => $meal) {
                                ?>
                                <button type="button" class="btn btn-default meal-btns" data-meal-type="<?= $mealId ?>"
                                        onclick="PackageManager.selectMeal(this)"><?= $meal ?></button>
                                <?php
                            }
                            ?>
                            <?php echo $form->field($dynamicModel, 'selectedMeals')->input('hidden')->label(false); ?>


                        </div>
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-info"
                                onclick="PackageManager.OrderGeneration.fetchOrders(this)"><i
                                    class="fa fa-check-square">&nbsp;</i>Fetch Orders
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <?=
                        $form->field($dynamicModel, 'city_id')->widget(\kartik\select2\Select2::classname(), [
                            'data' => $cityList,
                            'options' => ['placeholder' => 'Select City', 'multiple' => false],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('City');
                        ?>

                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php
if ($pkgDetailList !== null):
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary direct-chat direct-chat-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New Orders</h3>

                    <button type="button" class="btn btn-success pull-right"
                            onclick="PackageManager.OrderGeneration.generateOrder()"><i class="fa fa-plus-square">&nbsp;</i>Generate Transaction</button>
                    <input type="hidden" name="order-content" value="<?= $orderDataEncoded ?>">
                    <span class="pull-right">Fetched <span class="badge badge-secondary"><?= count($customersListFetched) ?></span> Customers</span>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                        <tr>
                            <th>Customer</th>
                            <th>Package</th>
                            <th>Serve Date</th>
                            <th>Meal Type</th>
                            <th>Meal Title</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php


                        if ($pkgDetailList != null && count($pkgDetailList)) {
                            foreach ($pkgDetailList as $pkgDetail) {
                                foreach ($pkgDetail['meals_list']['meals'] as $pkgMealId => $pkgMeals) {
                                    foreach ($pkgMeals['items'] as $item) {
                                        ?>
                                        <tr>
                                            <td><?= $pkgDetail['customerTitle'] ?></td>
                                            <td><?= $pkgDetail['packageTitle'] ?></td>
                                            <td><?= $item['serve_date'] ?></td>
                                            <td><?= $pkgMeals['title'] ?></td>
                                            <td><?= $item['title'] ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                        }
                        ?>


                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>
                <!-- /.box-footer-->
            </div>
        </div>

    </div>

<?php
endif;
?>



<?php
$this->registerCssFile("@web/css/dataTables.bootstrap.min.css", [
    'depends' => [
        \backend\assets\AppAsset::className()
    ]
]);
$this->registerJsFile("@web/js/jquery.dataTables.min.js", [
    'depends' => [
        \backend\assets\AppAsset::className()
    ]
]);
$this->registerJsFile("@web/js/dataTables.bootstrap.min.js", [
    'depends' => [
        \backend\assets\AppAsset::className()
    ]
]);

$this->registerJs("
        $('#example').DataTable();
        PackageManager.OrderGeneration.onloadPage();
");

?>


