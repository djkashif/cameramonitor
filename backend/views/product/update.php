<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Vehicle */

$this->title = 'Update Product: ' . $model->product_title;

?>
<div class="customer-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
