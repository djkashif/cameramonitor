<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->title = Yii::t("app", Yii::$app->params['appFullName']);

?>
<?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($model, "email")->textInput(["autofocus" => true]); ?>
    <?php echo $form->field($model, "password")->passwordInput(); ?>
    <div class="row">
        <div class="col-md-12">
            <?php echo Html::submitButton(Yii::t("app", "Submit"), ["class" => "btn btn-primary pull-right"]); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>