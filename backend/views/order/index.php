<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VehicleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
$this->params["headerButtons"][] = Html::a(Yii::t("app", "Advance Search"), "javascript:;", ["class" => "btn btn-primary", "data" => ["toggle" => "modal", "target" => "#searchModal"]]);
$this->params["headerButtons"][] = Html::a(Yii::t("app", "Create"), Url::to(["process-new-orders"]), ["class" => "btn btn-primary"]);
echo $this->render("_search", [
    "searchModel" => $searchModel
]);
?>
<div class="vehicle-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                "label" => Yii::t("app", "Transaction #"),
                "format" => "raw",

                "value" => function ($model) {
                    return trim($model->transaction_id);
                }
            ],
            [
               'attribute' => 'id',
                "label" => Yii::t("app", "Order #"),
            ],
            [
                "label" => Yii::t("app", "Account #"),
                "format" => "raw",

                "value" => function ($model) {
                    return trim($model->customer->accountNumber);
                }
            ],
            [
                "label" => Yii::t("app", "Customer"),
                "format" => "raw",

                "value" => function ($model) {
                    return trim($model->customer->firstName . " " . $model->customer->lastName);
                }
            ],
            [
                "label" => Yii::t("app", "Package"),
                "format" => "raw",

                "value" => function ($model) {
                    return trim($model->package->package_title);
                }
            ],
            [
                "label" => Yii::t("app", "Item Details"),
                "format" => "raw",

                "value" => function ($model) {
                $html = "";
                    if($model->orderMeals){
                        foreach ($model->orderMeals as $orderMeal){
                            foreach ($orderMeal->orderMealItems as $orderMealItem){
                                $html .= '<li><span class="label label-info">'.$orderMeal->meal->meal_title.'</span>'.$orderMealItem->product->product_title.'</li>';
                            }
                        }
                    }
                    return "<ul>{$html}</ul>";
                }

            ],

            'createdOn',
            'modifiedOn',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
