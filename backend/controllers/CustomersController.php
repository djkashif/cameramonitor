<?php

namespace backend\controllers;

use backend\components\BarcodeGenerator;
use backend\components\GeneralHelper;
use backend\modules\rbac\models\AuthItem;
use common\models\Package;
use common\models\Users;
use common\models\Cities;
use common\models\Countries;
use common\models\Customer;
use common\models\CustomerSearch;
use common\models\States;
use Yii;

use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Admin"],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['viewCustomers']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['viewCustomers']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['createCustomer']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update', 'changestatus'],
                        'roles' => ['updateCustomer']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteCustomer']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['dashboard'],
                        'roles' => ['viewCustomerDashboard']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['export-all-customers'],
                        'roles' => ['exportCustomers']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['country-states', 'state-cities'],
                        'roles' => ['@']
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication");
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $cityList = ArrayHelper::map(Cities::find()->Where(['active'=>'1'])->all(), "id", "name");

        return $this->render('index', [
            'searchModel' => $searchModel,
            'cityList' => $cityList,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single customer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $customerModel = new Customer();
        $userModel = new Users([
            "password" => Yii::$app->security->generatePasswordHash("abc1234"),
            "token" => Yii::$app->security->generateRandomString()
        ]);
        $rolesModel = new DynamicModel(["role"]);
        $rolesModel->addRule("role", "required");
        $rolesModel->role = 'Customer';

        $dynamicModel = new DynamicModel([
            'country_id',
            'state_id',
            'password',
        ]);
        $dynamicModel->addRule(['country_id', 'state_id', 'password'], "required");


        $rolesList = ArrayHelper::map(AuthItem::findAll(["type" => 1]), "name", "name");

        if ($userModel->load(Yii::$app->request->post()) && $customerModel->load(Yii::$app->request->post())) {

            if ($userModel->validate() && $rolesModel->validate()) {
                $userModel->password = Yii::$app->security->generatePasswordHash($dynamicModel->password);
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if (!$userModel->save()) {
                        throw new \Exception(Yii::t("app", "An error occurred while saving user data"));
                    }
                    $authManager = Yii::$app->authManager;
                    $role = $authManager->getRole($rolesModel->role);
                    $authManager->assign($role, $userModel->id);


                    if ($customerModel->load(Yii::$app->request->post())) {
                        /*$accountNumber = (new \yii\db\Query())
                                ->from('customer')
                                ->max('accountNumber') + 1;*/
                        $customerModel->user_id = $userModel->id;
                        //$customerModel->accountNumber = (string)$accountNumber;
                        if ($customerModel->save()) {

                            $barCodeTitle = 'cst_' . $customerModel->accountNumber . '_' . time() . '.png';
                            /*$barcodeData = json_encode([
                                'type' => 'CST',
                                'id' => $customerModel->id,
                            ]);*/
                            $barcodeData = "CST" . $customerModel->accountNumber;
                            BarcodeGenerator::createPngBarcode($barcodeData, 'barcodes/' . $barCodeTitle);
                            $customerModel->barcode = $barCodeTitle;
                            $customerModel->save();
                            GeneralHelper::showSuccessMsg('Customer saved successfully');

                        } else {
                            throw new \Exception(Yii::t("app", "An error occurred while saving user data"));
                        }
                    }
                    $transaction->commit();
                    Yii::$app->session->setFlash("success", Yii::t("app", "Data saved successfully"));
                    return $this->redirect('/customers');
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    GeneralHelper::showErrorMsg('Error occured while saving customer');
                }
            }
        }else{
            $accountNumber = (new \yii\db\Query())
                    ->from('customer')
                    ->max('accountNumber') + 1;
            $customerModel->accountNumber = $accountNumber;
        }

        $countryList = ArrayHelper::map(Countries::find()->all(), "id", "name");
        $stateList = ArrayHelper::map(Countries::find()->where(['name' => 'Pakistan'])->one()->states, "id", "name");
        //echo "<pre>";print_r($stateList);echo "</pre>";die('Call');


        return $this->render('create', [
            'customerModel' => $customerModel,
            'userModel' => $userModel,
            'dynamicModel' => $dynamicModel,
            'countryList' => $countryList,
            'stateList' => $stateList,
        ]);
    }


    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $customerModel = $this->findModel($id);
        $userModel = $customerModel->user;


        $dynamicModel = new DynamicModel([
            'country_id',
            'state_id',
            'password'
        ]);
        $dynamicModel->addRule(['country_id', 'state_id'], "required");
        $dynamicModel->addRule(['password'], "safe");
        $dynamicModel->country_id = (!empty($customerModel->city)) ? $customerModel->city->state->country_id : '';
        $dynamicModel->state_id = (!empty($customerModel->city)) ? $customerModel->city->state_id : '';

        if ($userModel->load(Yii::$app->request->post()) && $customerModel->load(Yii::$app->request->post()) && $dynamicModel->load(Yii::$app->request->post())) {
            if ($userModel->validate()) {

                $transaction = Yii::$app->db->beginTransaction();
                if(!empty($dynamicModel->password)){
                    $userModel->password = Yii::$app->security->generatePasswordHash($dynamicModel->password);
                }



                try {
                    if (!$userModel->save()) {
                        throw new \Exception(Yii::t("app", "An error occurred while saving user data"));
                    }
                    if ($customerModel->load(Yii::$app->request->post()) && $customerModel->save()) {
                        $barCodeTitle = 'cst_' . $customerModel->accountNumber . '_' . time() . '.png';
                        /*$barcodeData = json_encode([
                            'type' => 'CST',
                            'id' => $customerModel->id,
                        ]);*/
                        $barcodeData = "CST" . $customerModel->accountNumber;
                        BarcodeGenerator::createPngBarcode($barcodeData, 'barcodes/' . $barCodeTitle);
                        $customerModel->barcode = $barCodeTitle;
                        $customerModel->save();
                        GeneralHelper::showSuccessMsg('Customer saved successfully');
                    } else {
                        throw new \Exception(Yii::t("app", "An error occurred while saving user data"));
                    }
                    $transaction->commit();
                    return $this->redirect('/customers');
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    GeneralHelper::showErrorMsg('Error occured while saving customer'.$e->getMessage());
                }
            }
        }

        $countryList = ArrayHelper::map(Countries::find()->all(), "id", "name");
        $stateList = [];
        return $this->render('update', [
            'customerModel' => $customerModel,
            'userModel' => $userModel,
            'dynamicModel' => $dynamicModel,
            'countryList' => $countryList,
            'stateList' => $stateList,
        ]);
    }

    public function actionCountryStates()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $stateList = [];
        $selected = '';
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $country_id = $parents[0];
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];

                }
                $selected = (isset($_POST['depdrop_all_params']['state_id_pre'])) ? $_POST['depdrop_all_params']['state_id_pre'] : '';
                $stateListData = States::find()->where(['country_id' => $country_id])->all();
                foreach ($stateListData as $stateListD) {
                    $stateList[] = [
                        'id' => $stateListD->id,
                        'name' => $stateListD->name,
                    ];
                }
                $stateList = (count($stateList)) ? $stateList : '';
                return ['output' => $stateList, 'selected' => $selected];
            }
        }
        return ['output' => '', 'selected' => $selected];
    }


    public function actionStateCities()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $selected = '';
        $cityList = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $state_id = $parents[0];
                if (!empty($_POST['depdrop_params'])) {
                    $params = $_POST['depdrop_params'];
                }
                $selected = (isset($_POST['depdrop_all_params']['city_id_pre'])) ? $_POST['depdrop_all_params']['city_id_pre'] : '';
                $cityListData = Cities::find()->where(['state_id' => $state_id])->all();
                foreach ($cityListData as $cityListD) {
                    $cityList[] = [
                        'id' => $cityListD->id,
                        'name' => $cityListD->name,
                    ];
                }
                $cityList = (count($cityList)) ? $cityList : '';
                return ['output' => $cityList, 'selected' => $selected];
            }
        }
        return ['output' => '', 'selected' => $selected];
    }


    public function actionDashboard($id){
        $filterModel = new DynamicModel(["view_type", "date_range", "date_from", "date_to"]);
        $filterModel->addRule(["view_type", "date_range", "date_from", "date_to"], "safe");
        $post = Yii::$app->request->get();
        //echo "<pre>";print_r($post);echo "</pre>";die('Call');




        $customer  = Customer::findOne(['id' => $id]);
        $pkgOfTheDay = Package::find()->where([
            'package_status' => 1,
            'customer_id' => $customer->id
        ])
            ->andWhere([
                '<=', 'package_start_date', date('Y-m-d')
            ])
            ->andWhere([
                '>', 'package_expiration', date('Y-m-d')
            ])->one();

        if(!empty($pkgOfTheDay)){
            $dateStart = $pkgOfTheDay->package_start_date;
            $dateCondition = "AND o.createdOn >= '{$dateStart}'";

        }else{
            $dateCondition = "";
        }
        $filterModel->view_type = "summary";
        if ($filterModel->load($post) && !empty($filterModel->date_range)) {
            $dateStart = $filterModel->date_from." 00:00:00";;
            $dateEnd = $filterModel->date_to." 23:59:59";;
            $dateCondition = "AND o.createdOn >= '{$dateStart}' AND o.createdOn <= '{$dateEnd}'";
        }


        $connection = Yii::$app->getDb();

        switch ($filterModel->view_type){
            case 'detailed':
                $queryReportGrid = "SELECT 
                        
                        DATE_FORMAT(omi.createdOn, '%Y-%m-%d') as serve_date,
                        p.product_title,
                        m.meal_title,
                        sum(p.calories) as calories, 
                        sum(p.fat) as fat, 
                        sum(p.carbohydrates) as carbohydrates, 
                        sum(p.protiens) as protiens 
                        
                        
                        FROM `orders` o 
                        inner join order_meals om on om.order_id = o.id
                        INNER join order_meal_items omi on omi.order_meal_id = om.id
                        inner JOIN product p on p.id = omi.product_id
                        inner join meals m on om.meal_id = m.id
                        
                        WHERE o.customer_id = ".$customer ->id."
                        -- WHERE o.customer_id = 4
                        {$dateCondition}
                        
                        GROUP by DATE_FORMAT(omi.createdOn, '%Y-%m-%d'), m.id,omi.product_id
                        order by serve_date, m.meal_title";
                break;
            default:
                $queryReportGrid = "SELECT 
                        m.meal_title,
                        sum(p.calories) as calories, 
                        sum(p.fat) as fat, 
                        sum(p.carbohydrates) as carbohydrates, 
                        sum(p.protiens) as protiens 
                        
                        
                        FROM `orders` o 
                        inner join order_meals om on om.order_id = o.id
                        INNER join order_meal_items omi on omi.order_meal_id = om.id
                        inner JOIN product p on p.id = omi.product_id
                        inner join meals m on om.meal_id = m.id
                        WHERE o.customer_id = ".$customer ->id."
                        {$dateCondition}
                        GROUP by  m.id";
                break;
        }
        //echo "<pre>";print_r($queryReportGrid);echo "</pre>";die('Call');
        $command = $connection->createCommand($queryReportGrid);
        $queryReportGridData = $command->queryAll();
        //echo "<pre>";print_r($queryReportGridData);echo "</pre>";die('Call');






        $query = "SELECT 
                        sum(p.calories) as calories, 
                        sum(p.fat) as fat, 
                        sum(p.carbohydrates) as carbohydrates, 
                        sum(p.protiens) as protiens 
                        
                        
                        FROM `orders` o 
                        inner join order_meals om on om.order_id = o.id
                        INNER join order_meal_items omi on omi.order_meal_id = om.id
                        inner JOIN product p on p.id = omi.product_id
                        
                        WHERE o.customer_id = ".$customer ->id."
                        -- WHERE o.customer_id = 4
                         {$dateCondition}
                        -- GROUP by omi.product_id";




        $command = $connection->createCommand($query);
        $totalEnergyConsumption = $command->queryOne();


        $query = "SELECT 
                        DATE_FORMAT(omi.createdOn, '%Y-%m-%d') as date_served,
                        sum(p.calories) as calories, 
                        sum(p.fat) as fat, 
                        sum(p.carbohydrates) as carbohydrates, 
                        sum(p.protiens) as protiens 
                        
                        
                        FROM `orders` o 
                        inner join order_meals om on om.order_id = o.id
                        INNER join order_meal_items omi on omi.order_meal_id = om.id
                        inner JOIN product p on p.id = omi.product_id
                        
                        WHERE o.customer_id = ".$customer ->id."
                        -- WHERE o.customer_id = 4
                        {$dateCondition} 
                        GROUP by DATE_FORMAT(omi.createdOn, '%Y-%m-%d')";



        $command = $connection->createCommand($query);
        $dateWiseConsumption = $command->queryAll();

        $dates = array_map(function ($ar) {return $ar['date_served'];}, $dateWiseConsumption);
        $calories = array_map(function ($ar) {return $ar['calories'];}, $dateWiseConsumption);
        $fat = array_map(function ($ar) {return $ar['fat'];}, $dateWiseConsumption);
        $carbohydrates = array_map(function ($ar) {return $ar['carbohydrates'];}, $dateWiseConsumption);
        $protiens = array_map(function ($ar) {return $ar['protiens'];}, $dateWiseConsumption);

        $areaChartData = [
            'labels' => $dates,
            'datasets' => [
                [
                    'label' => 'Calories',
                    'fillColor' => '#44c1ef',
                    'strokeColor' => '#44c1ef',
                    'pointColor' => '#44c1ef',
                    'pointStrokeColor' => '#44c1ef',
                    'pointHighlightFill' => '#fff',
                    'pointHighlightStroke' => '#44c1ef',
                    'data' => $calories,
                ],
                [
                    'label' => 'FAT',
                    'fillColor' => '#dd4a39',
                    'strokeColor' => '#dd4a39',
                    'pointColor' => '#dd4a39',
                    'pointStrokeColor' => '#dd4a39',
                    'pointHighlightFill' => '#fff',
                    'pointHighlightStroke' => '#dd4a39',
                    'data' => $fat,
                ],
                [
                    'label' => 'Carbohydrates',
                    'fillColor' => '#00a65a63',
                    'strokeColor' => '#00a65a63',
                    'pointColor' => '#00a65a63',
                    'pointStrokeColor' => '#00a65a63',
                    'pointHighlightFill' => '#fff',
                    'pointHighlightStroke' => '#00a65a63',
                    'data' => $carbohydrates,
                ],
                [
                    'label' => 'Proteins',
                    'data' => $protiens,
                ]
            ]
        ];


        /*echo "<pre>";print_r($totalEnergyConsumption);echo "</pre>";
        echo "<pre>";print_r($dateWiseConsumption);echo "</pre>";die('Call');*/


        //echo "<pre>";print_r($queryReportGridData);echo "</pre>";die('Call');
        return $this->render('dashboard', [
            'customer' => $customer,
            'filterModel' => $filterModel,
            'totalEnergyConsumption' => $totalEnergyConsumption,
            'areaChartData' => json_encode($areaChartData),
            'queryReportGridData' => $queryReportGridData
        ]);
    }


    public function actionChangestatus($id)
    {
        $model = $this->findModel($id);

        if ($model === null) {
            Yii::$app->session->setFlash("error", Yii::t("app", "The requested url could not be found"));
            return $this->redirect(Url::to(["index"]));
        }

        if ($model->user->active === 1) {
            $model->user->active = 0;
            $message = "Customer deactivated successfully.";
        } else {
            $model->user->active = 1;
            $message = "Customer activated successfully.";
        }

        if ($model->user->save()) {
            Yii::$app->session->setFlash("success", Yii::t("app", $message));
        } else {
            Yii::$app->session->setFlash("error", Yii::t("app", "An error occurred while changing user's status"));
        }

        return $this->redirect(Url::to(["index"]));
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionExportAllCustomers()
    {
        $customers = Customer::find()->all();
        $data = [];
        foreach ($customers as $customer) {
            $data[] = [
                $customer->accountNumber,
                $customer->user->firstName,
                $customer->user->lastName,
                $customer->fatherName,
                $customer->cnic,
                $customer->contactNumber,
                (!empty($customer->gender)) ? (($customer->gender == 1) ? 'Male' : 'Female') : '',
                $customer->weight,
                $customer->height,
                $customer->dob,
                $customer->address,
                (isset($customer->city->name)) ? $customer->city->name : '',
                ($customer->user->active == 1) ? 'Active' : 'Inactive'

            ];
        }

        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                'Package Details' => [   // Name of the excel sheet
                    'data' => $data,

                    // Set to `false` to suppress the title row
                    'titles' => [
                        'Account Number',
                        'First Name',
                        'Last Name',
                        'Father Name',
                        'CNIC',
                        'Contact Number',
                        'Gender',
                        'Weight',
                        'Height',
                        'DOB',
                        'Address',
                        'City',
                        'Status'

                    ],
                ],
            ]
        ]);
        //echo "<pre>";print_r($file);echo "</pre>";die('Call');
        //$file->send('Test.xlsx');
        $fileTitle = 'CustomerExport_' . date('d-m-Y');

        $file->send(str_replace(' ', '_', $fileTitle) . '_' . rand(1000, 20000) . '.xlsx');
        die;

    }

    public function actionUpdateBarcode($id){
        $ids = explode(',', $id);
        foreach ($ids as $idss){
            $customerModel = Customer::find()->where(['id' => $idss])->one();
            if(!empty($customerModel)){
                $barCodeTitle = 'cst_' . $customerModel->accountNumber . '_' . time() . '.png';
                $barcodeData = "CST" . $customerModel->accountNumber;
                BarcodeGenerator::createPngBarcode($barcodeData, 'barcodes/' . $barCodeTitle);
                $customerModel->barcode = $barCodeTitle;
                $customerModel->save();
            }
        }



    }

    public function actionTestBarcode(){
        BarcodeGenerator::createPngBarcode('CST12345', 'barcodes/123.png');
    }

}
