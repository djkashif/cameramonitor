<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_meal_items".
 *
 * @property int $id
 * @property int $order_meal_id
 * @property int $product_id
 * @property double $price
 * @property double $qty
 * @property int $status
 * @property string $createdOn
 * @property string $modifiedOn
 */
class OrderMealItems extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_meal_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_meal_id', 'product_id', 'price'], 'required'],
            [['order_meal_id', 'product_id', 'status'], 'integer'],
            [['price', 'qty'], 'number'],
            [['createdOn', 'modifiedOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_meal_id' => 'Order Meal ID',
            'product_id' => 'Product ID',
            'price' => 'Price',
            'qty' => 'Qty',
            'status' => 'Status',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }

    public function getProduct(){
        return $this->hasOne(Product::className(), ["id" => "product_id"]);
    }

    public function getOrderMeal(){
        return $this->hasOne(OrderMeals::className(), ["id" => "order_meal_id"]);
    }
}
