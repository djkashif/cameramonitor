<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <!-- form start -->
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">


                <?= $form->field($model, 'product_title')->textInput(['maxlength' => true, 'placeholder' => 'Title']) ?>

                <?= $form->field($model, 'product_description')->textarea(['maxlength' => true, 'placeholder' => 'Description']) ?>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'unit_measurement_qty')->textInput(['maxlength' => true, 'placeholder' => 'Unit Measurement Qty']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'uom', [])->widget(\kartik\select2\Select2::classname(), [
                            'data' => \backend\components\GeneralHelper::$unitOfMeasurement,
                            'options' => ['placeholder' => 'Select UOM ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Unit Of Measurement');
                        ?>
                    </div>

                </div>


                <?= $form->field($model, 'calories')->textInput(['maxlength' => true, 'placeholder' => 'Calories']) ?>

                <?= $form->field($model, 'fat')->textInput(['maxlength' => true, 'placeholder' => 'Fats']) ?>

                <?= $form->field($model, 'protiens')->textInput(['maxlength' => true, 'placeholder' => 'Protein'])->label('Protein') ?>

                <?= $form->field($model, 'carbohydrates')->textInput(['maxlength' => true, 'placeholder' => 'Carbohydrates'])->label('Carbohydrates') ?>

                <?= $form->field($model, 'price')->textInput(['maxlength' => true, 'placeholder' => 'Price'])->label('Price') ?>




                <div class="box-footer">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
        <!-- /.box -->
    </div>
    <!--/.col (left) -->

</div>


