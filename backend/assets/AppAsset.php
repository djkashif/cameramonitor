<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/all.min.css',
        'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
        'css/tempusdominus-bootstrap-4.min.css',
        'css/adminlte.min.css',
        'css/skins/OverlayScrollbars.min.css',
        'css/daterangepicker.css',
        'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700',
        'css/custom.css',
    ];
    public $js = [
        /*'js/jquery.min.js',*/
        'js/jquery.min.js',
        'js/bootstrap.bundle.min.js',
        'js/adminlte.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
