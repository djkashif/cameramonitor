<?php
namespace backend\modules\rbac\models;

use common\models\ActiveRecord;

class AuthItem extends ActiveRecord
{
    public static function tableName()
    {
        return "{{%auth_item}}";
    }

    public function rules()
    {
        return [
	    [["name", "type"], "required"],
            [["type"], "integer"],
            [["description", "data"], "string"],
            [["name", "rule_name"], "string", "max" => 64],
            [["createdOn", "modifiedOn"], "safe"],
        ];
    }
}