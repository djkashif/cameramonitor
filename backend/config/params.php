<?php
return [
    'adminEmail' => 'admin@example.com',
    'appSmallName' => 'F <i class="fa fa-heartbeat" aria-hidden="true"></i> T',
    'appFullName' => 'Fitness Food',
    'appFullNameFormatted' => 'FITNESS <i class="fa fa-heartbeat" aria-hidden="true"></i> FOOD',
    'apiHeaderToken' => base64_encode('FT1234!!@#$'),
    //'fcmApiKey' => 'AAAAMOi5jT0:APA91bGsLYzaTWwpHdHWh34aQSkXxd4_RHGm38sOMf5MkypTCy9qm_8ISyDNNljY1nkC379grZjEAL5GzhLwrKj1Snu6D1dg-FmGrd4HZ4Lyb7QJYT6e42ckk2DX6ILwC5JDE63b3R3j',
    //'fcmApiKey' => 'AAAAykkCbtk:APA91bElqkHQqKKa-LvZdC-mwkN2eGqMQXFDNZScnzUh7ixGfB9Tczul858RbKWVIlDWx2yuU_qiM7-EkJyoGpWkEAbh-WCNDtOqtBeFDuQxjUHeK1uFrOX290r9Szeu6kKebm-MyHtO',
    'fcmApiKey' => 'AAAAI2Z9rzs:APA91bGjdgYnhtcwCIou69jE3hiBG6XTyV1_n2RyACjEdXEyX46AaYvXVk2LyhGJG_Us32MsXHdLP5MQEwGglwsqmMu_Q4RC3ETkKTdrBXF-bF74O7Ztxo3gSqi0l3s9depUf0P6P5V3',

];
