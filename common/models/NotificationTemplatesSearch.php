<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;



/**
 * VehicleSearch represents the model behind the search form of `common\models\Vehicle`.
 */
class NotificationTemplatesSearch extends NotificationTemplates
{
    public function rules()
    {
        return [
            [['template_title'], "string", "max" => 255]
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NotificationTemplates::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'createdOn' => $this->createdOn,
            'modifiedOn' => $this->modifiedOn,
        ]);

        $query
            ->andFilterWhere(['like', 'template_title', $this->template_title]);


        return $dataProvider;
    }
}
