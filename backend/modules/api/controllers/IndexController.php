<?php

namespace backend\modules\api\controllers;


use backend\components\CustomerManager;
use common\models\Cities;
use common\models\Customer;
use common\models\Users;

use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use backend\components\BarcodeGenerator;

/**
 * Default controller for the `api` module
 */
class IndexController extends Controller
{
    public static $user = null;
    public static $customer = null;
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->request->isPost) {
            $this->asJson([
                'status' => 0,
                'msg' => 'Invalid request',
                'data' => (object)[],
            ]);
            return false;
        }

        Yii::info(Yii::$app->getRequest()->getRawBody(), 'api_hit');
        if(Yii::$app->request->getHeaders()->get('appToken') == Yii::$app->params['apiHeaderToken']){
            if($action->id == 'register' || $action->id == 'sign-up' || $action->id == 'login' || $action->id == 'cities' || $action->id == 'activity-list' || $action->id == 'goal-list' ){
                return parent::beforeAction($action);
            }

            if(Yii::$app->request->getHeaders()->get('authorization')){
                $token = str_replace('Bearer ', '', Yii::$app->request->getHeaders()->get('authorization'));
                $user = Users::findIdentityByAccessToken($token);
                if(!empty($user)){
                    self::$user = $user;
                    self::$customer = Customer::findOne(['user_id' => self::$user->id]);
                    return parent::beforeAction($action);
                }else{
                    $this->asJson([
                        'status' => 0,
                        'msg' => 'invalid user token',
                        'data' => (object)[],
                    ]);
                }
            }else{
                $this->asJson([
                    'status' => 0,
                    'msg' => 'User authentication failed.',
                    'data' => (object)[],
                ]);
            }
            return false;



            /*if(){

            }*/


        }else{
            $this->asJson([
                'status' => 0,
                'msg' => 'invalid api token',
                'data' => (object)[],
            ]);
            return false;
        }
        //echo "<pre>";print_r(Yii::$app->params['apiHeaderToken']);echo "</pre>";die('Call');
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSignUp(){
        $dynamicModel = new DynamicModel([
            'firstName',
            'lastName',
            'fatherName',
            'email',
            'password',
            'cnic',
            'contactNumber',
            'address',
            'city_id',
        ]);
        $dynamicModel->addRule([
            'firstName',
            'email',
            'password',

            'contactNumber',
            'address',
            'city_id',
        ], 'required');

        $dynamicModel->addRule([
            'lastName',
            'fatherName',
            'cnic',
        ], 'safe');
        $post['DynamicModel'] = json_decode(Yii::$app->getRequest()->getRawBody(), 1);
        $dynamicModel->load($post);
        if($dynamicModel->validate()){
            $accountNumber = (new \yii\db\Query())
                    ->from('customer')
                    ->max('accountNumber')+1;


            $transaction = Yii::$app->db->beginTransaction();


            $userModel = new Users([
                'firstName' => $dynamicModel->firstName,
                'lastName' => $dynamicModel->lastName,
                'email' => $dynamicModel->email,
                "password" => Yii::$app->security->generatePasswordHash($dynamicModel->password),
                "token" => Yii::$app->security->generateRandomString()
            ]);
            $rolesModel = new DynamicModel(["role"]);
            $rolesModel->addRule("role", "required");
            $rolesModel->role = 'Customer';

            try{
                if (!$userModel->save()) {
                    throw new \Exception(Yii::t("app", "An error occurred while saving user data"), 1);
                }
                $authManager = Yii::$app->authManager;
                $role = $authManager->getRole($rolesModel->role);
                $authManager->assign($role, $userModel->id);

                $customerModel = new Customer([
                    'fatherName' => $dynamicModel->fatherName,
                    'cnic' => $dynamicModel->cnic,
                    'contactNumber' => $dynamicModel->contactNumber,
                    'address' => $dynamicModel->address,
                    'city_id' => $dynamicModel->city_id,
                ]);
                $customerModel->user_id = $userModel->id;
                $customerModel->accountNumber = (string)$accountNumber;
                if($customerModel->save()){

                    $barCodeTitle = 'cst_'.$customerModel->id.'_'.time().'.png';
                    /*$barcodeData = json_encode([
                        'type' => 'CST',
                        'id' => $customerModel->id,
                    ]);*/
                    $barcodeData = "CST".$customerModel->id;
                    BarcodeGenerator::createPngBarcode($barcodeData, 'barcodes/'.$barCodeTitle);
                    $customerModel->barcode = $barCodeTitle;
                    $customerModel->save();
                }else{

                    throw new \Exception(Yii::t("app", "An error occurred while saving user data"), 2);
                }

                $transaction->commit();
            }catch (\Exception $e){
                $transaction->rollBack();
                return [
                    'status' => 0,
                    'msg' => $e->getMessage(),
                    'data' => ($e->getCode() == 1) ? self::parseErrors($userModel->getErrors()) : (($e->getCode() == 2) ? self::parseErrors($customerModel->getErrors()) : (object)[]) ,
                ];
            }
            return [
                'status' => 1,
                'msg' => 'Customer created successfully',
                'data' => [
                    /*'firstName' => $userModel->firstName,
                    'lastName' => $userModel->lastName,
                    'accountNumber' => $customerModel->accountNumber,
                    'fatherName' => $customerModel->fatherName,
                    'weight' => $customerModel->weight,
                    'email' => $userModel->email,
                    'contactNumber' => $customerModel->contactNumber,
                    'address' => $customerModel->address,
                    'token' => $userModel->token,*/

                    'firstName' => $userModel->firstName,
                    'lastName' => $userModel->lastName,
                    'accountNumber' => $customerModel->accountNumber,
                    'fatherName' => $customerModel->fatherName,
                    'weight' => $customerModel->weight,
                    'email' => $userModel->email,
                    'contactNumber' => $customerModel->contactNumber,
                    'address' => $customerModel->address,
                    'cityId' => $customerModel->city_id,
                    'city' => (isset($customerModel->city->name)) ? $customerModel->city->name : '',
                    'genderId' => $customerModel->gender,
                    'genderTitle' => (!empty($customerModel->gender) && isset(CustomerManager::$genderTitles[$customerModel->gender])) ? CustomerManager::$genderTitles[$customerModel->gender] : '',
                    'activityLevelId' => $customerModel->activity_level,
                    'activityLevelDetails' => (!empty($customerModel->activity_level) && isset(CustomerManager::$activityLevelsLabel[$customerModel->activity_level])) ? CustomerManager::$activityLevelsLabel[$customerModel->activity_level] : (object)[],
                    'goalId' => $customerModel->goal_id,
                    'goalTitle' => (!empty($customerModel->goal_id) && isset(CustomerManager::$goals[$customerModel->goal_id])) ? CustomerManager::$goals[$customerModel->goal_id] : '',
                    'goalData' => $customerModel->goal_data,
                    'height' => $customerModel->height,
                    'dob' => $customerModel->dob,
                    'token' => $userModel->token,
                ]
            ];
        }else{
            return [
                'status' => 0,
                'msg' => 'validation failed',
                'data' => self::parseErrors($dynamicModel->getErrors()),
            ];
        }
    }



    public function actionLogin(){
        $dynamicModel = new DynamicModel([
            'email',
            'password',
            'fcm_token'
        ]);
        $dynamicModel->addRule([
            'email',
            'password',
            'fcm_token',
        ], 'required');
        $post['DynamicModel'] = json_decode(Yii::$app->getRequest()->getRawBody(), 1);
        $dynamicModel->load($post);

        if($dynamicModel->validate()){
            $user = Users::findOne([
                'email' => $dynamicModel->email,
                'active' => 1
            ]);



            if(!empty($user)&& Yii::$app->security->validatePassword($dynamicModel->password, $user->password)){
                $customer = Customer::findOne([
                    'user_id' => $user->id
                ]);
                if(!empty($customer) ){
                    $customer->firebaseToken = $dynamicModel->fcm_token;
                    $customer->save();
                    return [
                        'status' => 1,
                        'msg' => 'Login success',
                        'data' => [
                            'firstName' => $user->firstName,
                            'lastName' => $user->lastName,
                            'accountNumber' => $customer->accountNumber,
                            'fatherName' => $customer->fatherName,
                            'weight' => $customer->weight,
                            'email' => $user->email,
                            'contactNumber' => $customer->contactNumber,
                            'address' => $customer->address,
                            'cityId' => $customer->city_id,
                            'city' => (isset($customer->city->name)) ? $customer->city->name : '',
                            'genderId' => $customer->gender,
                            'genderTitle' => (!empty($customer->gender) && isset(CustomerManager::$genderTitles[$customer->gender])) ? CustomerManager::$genderTitles[$customer->gender] : '',
                            'activityLevelId' => $customer->activity_level,
                            'activityLevelDetails' => (!empty($customer->activity_level) && isset(CustomerManager::$activityLevelsLabel[$customer->activity_level])) ? CustomerManager::$activityLevelsLabel[$customer->activity_level] : (object)[],
                            'goalId' => $customer->goal_id,
                            'goalTitle' => (!empty($customer->goal_id) && isset(CustomerManager::$goals[$customer->goal_id])) ? CustomerManager::$goals[$customer->goal_id] : '',
                            'goalData' => $customer->goal_data,
                            'height' => $customer->height,
                            'dob' => $customer->dob,
                            'token' => $user->token,
                            'firebaseToken' => $customer->firebaseToken,
                        ],
                    ];
                }else{
                    return [
                        'status' => 0,
                        'msg' => 'Invalid login credentials',
                        'data' => (object)[],
                    ];
                }

            }else{
                return [
                    'status' => 0,
                    'msg' => 'Invalid login credentials',
                    'data' => (object)[],
                ];
            }
        }else{
            return [
                'status' => 0,
                'msg' => 'validation failed',
                'data' => self::parseErrors($dynamicModel->getErrors()),
            ];
        }
    }

    public function actionUpdatePassword(){

        $dynamicModel = new DynamicModel([
            'oldPassword',
            'newPassword',
            'confirmPassword',
        ]);
        $dynamicModel->addRule([
            'oldPassword',
            'newPassword',
            'confirmPassword',
        ], 'required');
        $dynamicModel->addRule([
            'confirmPassword',
        ], 'compare',['compareAttribute'=>'newPassword', 'message'=>"Passwords don't match"]);
        $post['DynamicModel'] = json_decode(Yii::$app->getRequest()->getRawBody(), 1);
        $dynamicModel->load($post);
        if($dynamicModel->validate()){
            if(Yii::$app->security->validatePassword($dynamicModel->oldPassword, self::$user->password)){
                self::$user->password = Yii::$app->security->generatePasswordHash($dynamicModel->newPassword);
                //echo "<pre>";print_r(self::$user);echo "</pre>";die('Call');
                self::$user->save();
                return [
                    'status' => 1,
                    'msg' => 'Password changed successfully',
                    'data' => (object)[],
                ];
            }else{
                return [
                    'status' => 0,
                    'msg' => 'Invalid old password ',
                    'data' => (object)[],
                ];
            }
        }else{
            return [
                'status' => 0,
                'msg' => 'validation failed',
                'data' => self::parseErrors($dynamicModel->getErrors()),
            ];
        }
    }

    public function actionGetProfileData(){
        return [
            'status' => 1,
            'msg' => 'Success',
            //'data' => self::$user->toArray(),
            'data' => [
                'firstName' => self::$user->firstName,
                'lastName' => self::$user->lastName,
                'accountNumber' => self::$customer->accountNumber,
                'fatherName' => self::$customer->fatherName,
                'weight' => self::$customer->weight,
                'email' => self::$user->email,
                'contactNumber' => self::$customer->contactNumber,
                'address' => self::$customer->address,
                'cityId' => self::$customer->city_id,
                'city' => (isset(self::$customer->city->name)) ? self::$customer->city->name : '',
                'genderId' => self::$customer->gender,
                'genderTitle' => (!empty(self::$customer->gender) && isset(CustomerManager::$genderTitles[self::$customer->gender])) ? CustomerManager::$genderTitles[self::$customer->gender] : '',
                'activityLevelId' => self::$customer->activity_level,
                'activityLevelDetails' => (!empty(self::$customer->activity_level) && isset(CustomerManager::$activityLevelsLabel[self::$customer->activity_level])) ? CustomerManager::$activityLevelsLabel[self::$customer->activity_level] : (object)[],
                'goalId' => self::$customer->goal_id,
                'goalTitle' => (!empty(self::$customer->goal_id) && isset(CustomerManager::$goals[self::$customer->goal_id])) ? CustomerManager::$goals[self::$customer->goal_id] : '',
                'goalData' => self::$customer->goal_data,
                'height' => self::$customer->height,
                'dob' => self::$customer->dob,
            ],
        ];
    }

    public function actionCities(){
        //$cityList = ArrayHelper::map(Cities::find()->all(), "id", "name");
        $cityList = Cities::find()->asArray()->all();
        $cityListData = [];
        array_map(function ($city) use (&$cityListData){
            $cityListData[] = [
                'id' => $city['id'],
                'name' => $city['name'],
            ];
        }, $cityList);


        return [
            'status' => 1,
            'msg' => 'Success',
            //'data' => self::$user->toArray(),
            'data' => [
                'city' => $cityListData
            ],
        ];

    }

    public function actionActivityList(){

        $list = [];
        array_map(function($d) use (&$list){
            $list[] = $d;

        }, CustomerManager::$activityLevelsLabel);

        return [
            'status' => 1,
            'msg' => 'Success',
            'data' => [
                'activityList' => $list
            ]
        ];
    }

    public function actionGoalList(){

        $goals = [];
        foreach (CustomerManager::$goals as $id => $goal){
            $goals[] = [
                'id' => $id,
                'name' => $goal
            ];
        }

        return [
            'status' => 1,
            'msg' => 'Success',
            'data' => [
                'goals' => $goals
            ]
        ];
    }




    private function parseErrors($errors){
        $errorWrapper = [];
        foreach ($errors as $error_key => $error){
            $errorWrapper[$error_key] = implode(', ', $error);
        }
        return $errorWrapper;
    }
}
