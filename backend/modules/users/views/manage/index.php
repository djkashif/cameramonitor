<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t("app", "Users");

$this->params["headerButtons"][] = Html::a(Yii::t("app", "Advance Search"), "javascript:;", ["class" => "btn btn-primary", "data" => ["toggle" => "modal", "target" => "#searchModal"]]);
if (Yii::$app->user->can("createUsers")) {
    $this->params["headerButtons"][] = Html::a(Yii::t("app", "Create"), Url::to(["create"]), ["class" => "btn btn-primary"]);
}

echo $this->render("_search", [
    "searchModel" => $searchModel
]);

if (Yii::$app->user->can("changeUsersStatus") || Yii::$app->user->can("deleteUsers")) {
    $this->registerJs("
        $(\".btn-confirm\").click(function () {
            var btn = $(this);

            $.showModal({
                title: \"Confirmation\",
                body: btn.attr(\"data-body\"),
                buttons: \"<a href=\\\"\" + btn.attr(\"data-href\") + \"\\\" class=\\\"btn btn-primary btn-confirm\\\" data-method=\\\"post\\\">CONFIRM</button>\"
            });
        });
    ");
}

echo GridView::widget([
    "layout" => "
        <div class=\"pull-right\">{pager}</div>
        {summary}{items}
    ",
    "dataProvider" => $dataProvider,
    "columns" => [
        [
            "class" => "yii\grid\CheckboxColumn",
            "headerOptions" => ["class" => "text-center", "style" => "width: 3%"],
            "contentOptions" => ["class" => "text-center"]
        ],
        [
            "attribute" => "id",
            "headerOptions" => ["style" => "width: 5%"],
            "contentOptions" => ["class" => "text-center"]
        ],
        "firstName",
        "lastName",
        "email",
        [
            "label" => Yii::t("app", "Roles"),
            "format" => "raw",
            "value" => function ($model) {
                $rolesByUser = Yii::$app->authManager->getRolesByUser($model->id);
                $userRoles = [];
                
                if (!empty($rolesByUser)) {
                    foreach ($rolesByUser as $row) {
                        $userRoles[] = $row->name;
                    }
                    
                    return implode(" || ", $userRoles);
                }
            }
        ],
        [
            "attribute" => "active",
            "label" => Yii::t("app", "Status"),
            "format" => "raw",
            "headerOptions" => ["style" => "width: 8%"],
            "contentOptions" => ["class" => "text-center"],
            "value" => function ($data) {
                $status = "success";
                $label = Yii::t("app", "Active");

                if ($data->active !== 1) {
                    $status = "danger";
                    $label = Yii::t("app", "Not Active");
                }

                $html = "<span class=\"label label-sm label-{$status}\">{$label}</span>";

                return $html;
            }
        ],
        [
            "class" => "yii\\grid\\ActionColumn",
            "headerOptions" => ["class" => "text-center", "style" => "width: 8%"],
            "contentOptions" => ["class" => "text-center"],
            "template" => "{update}{changestatus}{delete}",
            "buttons" => [
                "update" => function ($url, $model) {
                    if (Yii::$app->user->can("updateUsers")) {
                        return Html::a("<span class=\"glyphicon glyphicon-pencil\"></span>", $url, ["title" => "Update User"]);
                    }
                },
                "changestatus" => function ($url, $model) {
                    if (Yii::$app->user->can("changeUsersStatus")) {
                        $icon = ($model->active === 1) ? "glyphicon-repeat" : "glyphicon-refresh";
                        $text = ($model->active === 1) ? "deactivate" : "activate";
                        
                        return Html::a("<span class=\"glyphicon {$icon}\"></span>", "javascript:;", ["title" => ucwords($text) . " User", "class" => "btn-confirm", "data" => ["href" => $url, "body" => Yii::t("app", "Are you sure you want to {$text} user #{$model->id}?")]]);
                    }
                },
                "delete" => function ($url, $model) {
                    if (Yii::$app->user->can("deleteUsers")) {
                        return Html::a("<span class=\"glyphicon glyphicon-trash\"></span>", "javascript:;", ["title" => "Delete User", "class" => "btn-confirm", "data" => ["href" => $url, "body" => Yii::t("app", "Are you sure you want to delete user #{$model->id}?") . "<br /><br /><strong>" . Yii::t("app", "THIS ACTION IS IRREVERSIBLE") . "</strong>"]]);
                    }
                }
            ],
            "urlCreator" => function ($action, $model, $key, $index) {
                if ($action === "update") {
                    return Url::to(["update", "id" => $model->id]);
                }
                
                if ($action === "changestatus") {
                    return Url::to(["changestatus", "id" => $model->id]);
                }

                if ($action === "delete") {
                    return Url::to(["delete", "id" => $model->id]);
                }
            }
        ]
    ]
]);