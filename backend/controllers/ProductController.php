<?php

namespace backend\controllers;


use backend\components\BarcodeGenerator;
use backend\components\GeneralHelper;
use common\models\Product;
use common\models\ProductSearch;
use kartik\mpdf\Pdf;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Picqer\Barcode\BarcodeGeneratorPNG;
use Yii;

use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Admin"],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['viewProducts']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => ['viewProducts']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['createProduct']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => ['updateProduct']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteProduct']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['prepare-barcode'],
                        'roles' => ['printProductLabels']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['export-all-products'],
                        'roles' => ['exportProducts']
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication");
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $barCodeTitle = 'item_' . $model->id . '_' . time() . '.png';
            /*$barcodeData = json_encode([
                'type' => 'ITEM',
                'id' => $model->id,
            ]);*/
            $barcodeData = "ITM" . $model->id;
            BarcodeGenerator::createPngBarcode($barcodeData, 'barcodes/' . $barCodeTitle);
            $model->barcode = $barCodeTitle;
            $model->save();
            //Yii::$app->session->setFlash("success", Yii::t("app", "Data saved successfully"));
            GeneralHelper::showSuccessMsg('Item saved successfully');
            return $this->redirect('/product');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (empty($model->barcode)) {
                $barCodeTitle = 'item_' . $model->id . '_' . time() . '.png';
                /*$barcodeData = json_encode([
                    'type' => 'ITEM',
                    'id' => $model->id,
                ]);*/
                $barcodeData = "ITM" . $model->id;
                BarcodeGenerator::createPngBarcode($barcodeData, 'barcodes/' . $barCodeTitle);
                $model->barcode = $barCodeTitle;
                $model->save();
            }

            GeneralHelper::showSuccessMsg('Item saved successfully');
            //Yii::$app->session->setFlash("success", Yii::t("app", "Data saved successfully"));
            return $this->redirect('/product');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        GeneralHelper::showSuccessMsg('Item deleted successfully');

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its     primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionPrepareBarcode()
    {

        $products = Product::findAll(['status' => 1]);

        $content = $this->renderPartial('_prepare-barcode', [
            'products' => $products
        ]);
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([

            'mode' => Pdf::MODE_CORE,

            'format' => [101.6, 50.8],
            'marginLeft' => 1,
            'marginRight' => 1,
            'marginTop' => 1,
            'marginBottom' => 1,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@backend/web/css/bootstrap.min.css',
            'cssInline' => '.bar-container{
                                border: 2px solid;
                                border-radius: 12px;
                                height: 184px;
                                width: 396px;
                                overflow: hidden;
                            }
                            .heading-wrapper{
                                border-bottom: 4px solid;
                                padding: 0 0 0 8px;
                            }
                            .heading-wrapper2{
                                border-bottom: 1px solid;
                                padding: 0 0 0 8px;
                            }
                            .heading{
                                font-size: 18px;
                            }
                            .col-div{
                                padding: 4px;
                            }
                            .nut-heading{
                                font-size: 24px;
                                font-weight: bold;
                            }
                            .nut-value{
                                font-size: 24px;
                            }
                            .col-div p{
                                margin: 0;
                                padding: 0px 0;
                            }
                            .barcode-image img{
                                width: 100%;
                                height: 70px;
                            }
                            .content-div{
                                width: 30%;
                                float: left;
                            }
                            .blank-div{
                                width: 10%;
                                float: left;
                            }
                            .barcode-div{
                                width: 60%;
                                float: left;
                            }
                            .content-body-wrapper{
                                border-bottom: 2px solid;
                            }
                            .content-footer-wrapper{
                                text-align: center;
                            }
                            .content-footer-wrapper p{
                                margin: 0;
                                font-size: 13px;
                                font-weight: bold;
                            }',
            'options' => ['title' => 'Product Barcodes'],
            /*'methods' => [
                'SetHeader'=>['Krajee Report Header'],
                'SetFooter'=>['{PAGENO}'],
            ]*/
        ]);
        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionExportAllProducts()
    {
        $products = Product::find()->all();
        $data = [];
        foreach ($products as $product) {
            $data[] = [
                $product->product_title,
                $product->calories,
                $product->fat,
                $product->carbohydrates,
                $product->protiens,
                $product->uom,
                $product->unit_measurement_qty,
                $product->price
            ];
        }

        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                'Package Details' => [   // Name of the excel sheet
                    'data' => $data,

                    // Set to `false` to suppress the title row
                    'titles' => [
                        'Product Title',
                        'Calories',
                        'FAT',
                        'Carbohydrates',
                        'Proteins',
                        'UOM',
                        'UOM Qty',
                        'Price'
                    ],
                ],
            ]
        ]);
        //echo "<pre>";print_r($file);echo "</pre>";die('Call');
        //$file->send('Test.xlsx');
        $fileTitle = 'ProductExport_' . date('d-m-Y');

        $file->send(str_replace(' ', '_', $fileTitle) . '_' . rand(1000, 20000) . '.xlsx');
        die;

    }

    public function actionTest()
    {
        BarcodeGenerator::createPngBarcode('TEST_' . time(), 'barcodes/c_' . time() . '.png');
    }
}
