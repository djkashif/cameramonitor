<?php

namespace backend\modules\rbac\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Default controller for the `rbac` module
 */
class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Admin"],
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication/logout");
                }
            ]
        ];
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
