<?php
namespace backend\modules\users\models;

use common\models\ActiveRecord;

class Users extends ActiveRecord
{
    public static function tableName()
    {
        return "{{%users}}";
    }

    public function rules()
    {
        return [
            [["firstName", "lastName", "email", "password", "city_id"], "required"],
            [["createdOn", "modifiedOn"], "safe"],
            [["firstName", "lastName"], "string", "max" => 100],
            [["email", "password", "token"], "string", "max" => 255],
            [["active", "deleted", "city_id"], "integer"],
            [["email"], "unique"],
            ["email", "email"]
        ];
    }
}