<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transactions".
 *
 * @property int $id
 * @property int $user_id
 * @property double $grandTotal
 * @property int $status
 * @property string $createdOn
 * @property string $modifiedOn
 */
class Transactions extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'grandTotal'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['grandTotal'], 'number'],
            [['createdOn', 'modifiedOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'grandTotal' => 'Grand Total',
            'status' => 'Status',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }

    public function getOrders(){
        return $this->hasMany(Orders::className(), ['transaction_id' => 'id']);
    }
}
