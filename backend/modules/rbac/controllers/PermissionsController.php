<?php
namespace backend\modules\rbac\controllers;

use backend\modules\rbac\search\PermissionsSearch;
use Yii;
use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;

class PermissionsController extends Controller
{
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Admin"],
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication/logout");
                }
            ]
        ];
    }
    
    public function actionIndex()
    {
        $searchModel = new PermissionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 20;

        return $this->render("index", [
            "searchModel" => $searchModel,
            "dataProvider" => $dataProvider
        ]);
    }
    
    public function actionCreate()
    {
        $model = new DynamicModel(["title"]);
        $model->addRule("title", "required");
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $auth = Yii::$app->authManager;
            $auth->add($auth->createPermission($model->title));
            Yii::$app->session->setFlash("success", Yii::t("app", "Data saved successfully"));
            return $this->redirect(Url::to(["index"]));
        }
        
        return $this->render("form", [
            "model" => $model
        ]);
    }
}