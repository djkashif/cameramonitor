<?php

namespace backend\modules\customer\controllers;

use backend\components\CustomerManager;
use backend\components\GeneralHelper;
use common\models\Customer;
use common\models\Package;
use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

/**
 * Default controller for the `rbac` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Customer"],
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication");
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {


        $filterModel = new DynamicModel(["view_type", "date_range", "date_from", "date_to"]);
        $filterModel->addRule(["view_type", "date_range", "date_from", "date_to"], "safe");
        $post = Yii::$app->request->get();
        //echo "<pre>";print_r($post);echo "</pre>";die('Call');




        $customer  = Customer::findOne(['user_id' => Yii::$app->user->id]);
        $pkgOfTheDay = Package::find()->where([
            'package_status' => 1,
            'customer_id' => $customer->id
        ])
            ->andWhere([
                '<=', 'package_start_date', date('Y-m-d')
            ])
            ->andWhere([
                '>', 'package_expiration', date('Y-m-d')
            ])->one();

        if(!empty($pkgOfTheDay)){
            $dateStart = $pkgOfTheDay->package_start_date;
            $dateCondition = "AND o.createdOn >= '{$dateStart}'";

        }else{
            $dateCondition = "";
        }
        $filterModel->view_type = "summary";
        if ($filterModel->load($post) && !empty($filterModel->date_range)) {
            $dateStart = $filterModel->date_from." 00:00:00";;
            $dateEnd = $filterModel->date_to." 23:59:59";;
            $dateCondition = "AND o.createdOn >= '{$dateStart}' AND o.createdOn <= '{$dateEnd}'";
        }


        $connection = Yii::$app->getDb();

        switch ($filterModel->view_type){
            case 'detailed':
                $queryReportGrid = "SELECT 
                        
                        DATE_FORMAT(omi.createdOn, '%Y-%m-%d') as serve_date,
                        p.product_title,
                        m.meal_title,
                        sum(p.calories) as calories, 
                        sum(p.fat) as fat, 
                        sum(p.carbohydrates) as carbohydrates, 
                        sum(p.protiens) as protiens 
                        
                        
                        FROM `orders` o 
                        inner join order_meals om on om.order_id = o.id
                        INNER join order_meal_items omi on omi.order_meal_id = om.id
                        inner JOIN product p on p.id = omi.product_id
                        inner join meals m on om.meal_id = m.id
                        
                        WHERE o.customer_id = ".$customer ->id."
                        -- WHERE o.customer_id = 4
                        {$dateCondition}
                        
                        GROUP by DATE_FORMAT(omi.createdOn, '%Y-%m-%d'), m.id,omi.product_id
                        order by serve_date, m.meal_title";
                break;
            default:
                $queryReportGrid = "SELECT 
                        m.meal_title,
                        sum(p.calories) as calories, 
                        sum(p.fat) as fat, 
                        sum(p.carbohydrates) as carbohydrates, 
                        sum(p.protiens) as protiens 
                        
                        
                        FROM `orders` o 
                        inner join order_meals om on om.order_id = o.id
                        INNER join order_meal_items omi on omi.order_meal_id = om.id
                        inner JOIN product p on p.id = omi.product_id
                        inner join meals m on om.meal_id = m.id
                        WHERE o.customer_id = ".$customer ->id."
                        {$dateCondition}
                        GROUP by  m.id";
                break;
        }
        //echo "<pre>";print_r($queryReportGrid);echo "</pre>";die('Call');
        $command = $connection->createCommand($queryReportGrid);
        $queryReportGridData = $command->queryAll();
        //echo "<pre>";print_r($queryReportGridData);echo "</pre>";die('Call');






        $query = "SELECT 
                        sum(p.calories) as calories, 
                        sum(p.fat) as fat, 
                        sum(p.carbohydrates) as carbohydrates, 
                        sum(p.protiens) as protiens 
                        
                        
                        FROM `orders` o 
                        inner join order_meals om on om.order_id = o.id
                        INNER join order_meal_items omi on omi.order_meal_id = om.id
                        inner JOIN product p on p.id = omi.product_id
                        
                        WHERE o.customer_id = ".$customer ->id."
                        -- WHERE o.customer_id = 4
                         {$dateCondition}
                        -- GROUP by omi.product_id";




        $command = $connection->createCommand($query);
        $totalEnergyConsumption = $command->queryOne();


        $query = "SELECT 
                        DATE_FORMAT(omi.createdOn, '%Y-%m-%d') as date_served,
                        sum(p.calories) as calories, 
                        sum(p.fat) as fat, 
                        sum(p.carbohydrates) as carbohydrates, 
                        sum(p.protiens) as protiens 
                        
                        
                        FROM `orders` o 
                        inner join order_meals om on om.order_id = o.id
                        INNER join order_meal_items omi on omi.order_meal_id = om.id
                        inner JOIN product p on p.id = omi.product_id
                        
                        WHERE o.customer_id = ".$customer ->id."
                        -- WHERE o.customer_id = 4
                        {$dateCondition} 
                        GROUP by DATE_FORMAT(omi.createdOn, '%Y-%m-%d')";



        $command = $connection->createCommand($query);
        $dateWiseConsumption = $command->queryAll();

        $dates = array_map(function ($ar) {return $ar['date_served'];}, $dateWiseConsumption);
        $calories = array_map(function ($ar) {return $ar['calories'];}, $dateWiseConsumption);
        $fat = array_map(function ($ar) {return $ar['fat'];}, $dateWiseConsumption);
        $carbohydrates = array_map(function ($ar) {return $ar['carbohydrates'];}, $dateWiseConsumption);
        $protiens = array_map(function ($ar) {return $ar['protiens'];}, $dateWiseConsumption);

        $areaChartData = [
            'labels' => $dates,
            'datasets' => [
                [
                    'label' => 'Calories',
                    'fillColor' => '#44c1ef',
                    'strokeColor' => '#44c1ef',
                    'pointColor' => '#44c1ef',
                    'pointStrokeColor' => '#44c1ef',
                    'pointHighlightFill' => '#fff',
                    'pointHighlightStroke' => '#44c1ef',
                    'data' => $calories,
                ],
                [
                    'label' => 'FAT',
                    'fillColor' => '#dd4a39',
                    'strokeColor' => '#dd4a39',
                    'pointColor' => '#dd4a39',
                    'pointStrokeColor' => '#dd4a39',
                    'pointHighlightFill' => '#fff',
                    'pointHighlightStroke' => '#dd4a39',
                    'data' => $fat,
                ],
                [
                    'label' => 'Carbohydrates',
                    'fillColor' => '#00a65a63',
                    'strokeColor' => '#00a65a63',
                    'pointColor' => '#00a65a63',
                    'pointStrokeColor' => '#00a65a63',
                    'pointHighlightFill' => '#fff',
                    'pointHighlightStroke' => '#00a65a63',
                    'data' => $carbohydrates,
                ],
                [
                    'label' => 'Proteins',
                    'data' => $protiens,
                ]
            ]
        ];


        /*echo "<pre>";print_r($totalEnergyConsumption);echo "</pre>";
        echo "<pre>";print_r($dateWiseConsumption);echo "</pre>";die('Call');*/


        //echo "<pre>";print_r($queryReportGridData);echo "</pre>";die('Call');
        return $this->render('index', [
            'filterModel' => $filterModel,
            'totalEnergyConsumption' => $totalEnergyConsumption,
            'areaChartData' => json_encode($areaChartData),
            'queryReportGridData' => $queryReportGridData
        ]);
    }

    public function actionUpdateWeight(){
        $customer  = Customer::findOne(['user_id' => Yii::$app->user->id]);
        if(Yii::$app->request->post()){
            if ($customer->load(Yii::$app->request->post()) && $customer->save()) {
                GeneralHelper::showSuccessMsg('Weight saved successfully');
                return $this->refresh();
            }
        }
        //echo "<pre>";print_r($customer->weightHistory);echo "</pre>";die('Call');
        return $this->render('update-weight', [
            'customer' => $customer
        ]);
    }
}
