<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notification_templates".
 *
 * @property int $id
 * @property string $template_title
 * @property string $template_content
 * @property int $status
 * @property string $createdOn
 * @property string $modifiedOn
 */
class NotificationTemplates extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification_templates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
                [['template_title', 'template_content'], 'required'],
            [['template_content'], 'string'],
            [['status'], 'integer'],
            [['createdOn', 'modifiedOn'], 'safe'],
            [['template_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template_title' => 'Template Title',
            'template_content' => 'Template Content',
            'status' => 'Status',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }
}
