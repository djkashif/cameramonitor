<?php

namespace backend\components;

use common\models\Customer;
use common\models\Transactions;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class OrderManager
{
    public static $statusLables = [
        '1' => 'Pending',
        '2' => 'Verified',
        '3' => 'Completed',
        '4' => 'on Hold',
        '5' => 'Cancelled',
        '6' => 'Partially Completed',
        '7' => 'Partially Verified',
    ];

    public static function setTransactionStatus($transactionId){
        $transaction = Transactions::find()->where(['id' => $transactionId])->one();
        $orderStatusArray = [];
        if($transaction ){
            $orders = $transaction->orders;
            if($orders){
                foreach ($orders as $order){
                    $order = self::setOrderStatus($order);

                    $orderStatusArray[] = $order->status;
                }
            }
            $uniqueStatuses = array_unique($orderStatusArray);
            $newTransactionStatus = self::calculateStatus($uniqueStatuses);

            $transaction->status = $newTransactionStatus;
            $transaction->save();
        }
    }

    public static function createStatusLabel($status){
        $statusTitle = self::$statusLables[$status];
        switch ($status){
            case 1:
                $label = "<label class='label label-default'>{$statusTitle}</label>";
                break;
                case 3:
            case 6:
                $label = "<label class='label label-success'>{$statusTitle}</label>";
                break;
            case 4:
                $label = "<label class='label label-warning'>{$statusTitle}</label>";
                break;
            case 5:
                $label = "<label class='label label-danger'>{$statusTitle}</label>";
                break;
            case 7:
            case 2:
                $label = "<label class='label label-info'>{$statusTitle}</label>";
                break;
        }
        return $label;
    }


    public static function setOrderStatus($order){
        $orderMeals = $order->orderMeals;
        $itemsStatusArray = [];
        if($orderMeals){
            foreach ($orderMeals as $orderMeal){
                $orderMealItems = $orderMeal->orderMealItems;
                if($orderMealItems){
                    foreach ($orderMealItems as $orderMealItem){
                        $itemsStatusArray[] = $orderMealItem->status;
                    }
                }
            }
        }

        $uniqueStatuses = array_unique($itemsStatusArray);
        $newStatusCode = self::calculateStatus($uniqueStatuses);
        $order->status = $newStatusCode;
        $order->save();
        return $order;
    }

    public static function calculateStatus($uniqueStatuses){
        if(count($uniqueStatuses) == 1){
            $statusCode = $uniqueStatuses[0];
        }else {
            if(in_array(3, $uniqueStatuses)){
                $statusCode = 6; //// Partially Completed
            }else if(in_array(2, $uniqueStatuses)){
                $statusCode = 7; // Partially Verified
            }else if(in_array(7, $uniqueStatuses)){
                $statusCode = 7; // Partially Verified
            }else{
                $statusCode = 1;
            }
        }
        return $statusCode;
    }
}

