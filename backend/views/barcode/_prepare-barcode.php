
<?php foreach ($data as $pkgData): ?>
    <div class="container bar-container">
        <!--<div class="row heading-wrapper"  style="text-align: center">
            <span class="heading"><?/*= $pkgData['package']['package_title'] */?></span>
        </div>
        <div class="row heading-wrapper2">
            <span class="heading"><?/*= $pkgData['customer']['title'] */?></span>
        </div>-->
        <div class="row">
            <div class="">
                <table style="width: 100%;">
                    <tbody>
                    <tr style="">
                        <td colspan="2" style="width: 100%;height: 120px">
                            <table style="width: 100%">
                                <?php
                                $rowTotal = ceil(count($pkgData['products'])/3);
                                /*$rowRem = count($pkgData['products'])%3;
                                $rowTotal = ($rowRem > 0) ? $rowTotal+1 : $rowTotal;*/

                                $mealTitleStyle = "font-size: 1.00em;padding: 0px 0 0 5px;font-weight: bold;vertical-align: top;";
                                $productTitleStyle = "font-size: 0.85em;padding: 0px 0 0 5px;vertical-align: top;";


                                    foreach ($pkgData['products'] as $mealTitle => $products){
                                        ?>
                                        <tr>
                                            <td style="<?= $mealTitleStyle ?>" colspan="3">
                                                <?= $mealTitle ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="<?= $productTitleStyle ?>">
                                                <?php
                                                foreach ($products as $key => $product){
                                                    echo ' ('.($key+1).') '.$product['product_title'];
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>

                                    <?php


                                ?>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center;border-top: 2px solid;">
                            <p style="margin: 0;font-size: 0.65em;">
                                Store in a clean, dry, cool and odorless place.
                                Use within three days (if stored properly)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left;border-top: 2px solid;">
                            <p style="margin: 0;font-size: 14px;font-weight: bold;">
                                <img src="/barcodes/<?= $pkgData['customer']['barcode'] ?>" alt="" style="vertical-align: middle;width: 60%;padding: 5px 10px 0px 10px;height: 40px;">
                                <span style="width: 35%;font-size: 1.4em">Box # <?= $pkgData['customer']['accountNumber'] ?></span>
                            </p>
                        </td>
                    </tr>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
    <div style="page-break-after: auto;"></div>
    <?php endforeach; ?>




