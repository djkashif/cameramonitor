<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "meals".
 *
 * @property int $id
 * @property string $meal_title
 * @property int $status
 * @property string $createdOn
 * @property string $modifiedOn
 */
class Meals extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meal_title'], 'required'],
            [['status'], 'integer'],
            [['createdOn', 'modifiedOn'], 'safe'],
            [['meal_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meal_title' => 'Meal Title',
            'status' => 'Status',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }
}
