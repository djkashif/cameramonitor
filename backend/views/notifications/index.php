<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VehicleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notification Templates';
$this->params['breadcrumbs'][] = $this->title;
$this->params["headerButtons"][] = Html::a(Yii::t("app", "Advance Search"), "javascript:;", ["class" => "btn btn-primary", "data" => ["toggle" => "modal", "target" => "#searchModal"]]);
if (Yii::$app->user->can("createNotificationTemplate")) {
    $this->params["headerButtons"][] = Html::a(Yii::t("app", "Create"), Url::to(["create"]), ["class" => "btn btn-primary"]);
}

echo $this->render("_search", [
    "searchModel" => $searchModel
]);
if (Yii::$app->user->can("deleteNotificationTemplate")) {
    $this->registerJs("    
        $(\".btn-confirm\").click(function () {
            var btn = $(this);

            $.showModal({
                title: \"Confirmation\",
                body: btn.attr(\"data-body\"),
                buttons: \"<a href=\\\"\" + btn.attr(\"data-href\") + \"\\\" class=\\\"btn btn-primary btn-confirm\\\" data-method=\\\"post\\\">CONFIRM</button>\"
            });
        });
    ");
}
?>
<div class="vehicle-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'template_title',
            'createdOn',
            [
                "class" => "yii\\grid\\ActionColumn",
                "headerOptions" => ["class" => "text-center", "style" => "width: 8%"],
                "contentOptions" => ["class" => "text-center"],


                "template" => "{update}{delete}",
                "buttons" => [

                    "update" => function ($url, $model) {
                        if (Yii::$app->user->can("updateNotificationTemplate")) {
                            return Html::a("<span class=\"glyphicon glyphicon-pencil\"></span>", $url, ["title" => "Update Product"]);
                        }
                    },
                    "delete" => function ($url, $model) {

                        if (Yii::$app->user->can("deleteNotificationTemplate")) {
                            return Html::a("<span class=\"glyphicon glyphicon-trash\"></span>", "javascript:;", ["title" => "Delete Product", "class" => "btn-confirm", "data" => ["href" => $url, "body" => Yii::t("app", "Are you sure you want to delete user #{$model->id}?") . "<br /><br /><strong>" . Yii::t("app", "THIS ACTION IS IRREVERSIBLE") . "</strong>"]]);
                        }
                    }
                ],
                "urlCreator" => function ($action, $model, $key, $index) {

                    if ($action === "update") {
                        return Url::to(["update", "id" => $model->id]);
                    }
                    if ($action === "delete") {
                        return Url::to(["delete", "id" => $model->id]);
                    }
                }
            ]
        ],
    ]); ?>


</div>
