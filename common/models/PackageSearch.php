<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;



/**
 * VehicleSearch represents the model behind the search form of `common\models\Vehicle`.
 */
class PackageSearch extends Package
{
    public $customer;
    public $customer_city_id;

    public function rules()
    {
        return [
            [['package_title', 'customer','customer_city_id'], "string", "max" => 255]
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Package::find()
            ->innerJoinWith(["customer"])
            ->join('INNER JOIN', 'users', 'customer.user_id = users.id');;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'createdOn' => $this->createdOn,
            'modifiedOn' => $this->modifiedOn,
        ]);

        $query
            ->andFilterWhere(['like', 'users.firstName', $this->customer])
            ->orFilterWhere(['like', 'users.lastName', $this->customer])
            ->andFilterWhere(['like', 'customer.city_id', $this->customer_city_id])
            ->andFilterWhere(['like', 'package_title', $this->package_title]);



        return $dataProvider;
    }
}
