<?php
namespace backend\modules\users\controllers;

use backend\modules\users\models\Users;
use Yii;
use yii\base\DynamicModel;
use yii\web\Controller;

class ProfileController extends Controller
{
    public function actionIndex()
    {
        $model = Users::findOne(Yii::$app->user->identity->id);
        
        if ($model === null) {
            Yii::$app->session->setFlash("error", Yii::t("app", "The requested URL could not be found"));
            return $this->goBack();
        }
        
        $changePasswordModel = new DynamicModel([
            "oldPassword", "newPassword", "confirmPassword"
        ]);
        
        $changePasswordModel->addRule(["newPassword", "confirmPassword"], "required", ["when" => function ($model) {
            return !empty($model->oldPassword);
        }, "whenClient" => "function (attribute, value) {
            return $(\"#dynamicmodel-oldpassword\").val() !== \"\";
        }"]);
        
        if ($model->load(Yii::$app->request->post()) && $changePasswordModel->load(Yii::$app->request->post())) {
            if (!empty($changePasswordModel->oldPassword)) {
                
            }
        }
        
        return $this->render("index", [
            "model" => $model,
            "changePasswordModel" => $changePasswordModel
        ]);
    }
}