<?php

namespace backend\modules\api\controllers;

use backend\auth\UserIdentity;
use common\models\Customer;
use common\models\Package;
use common\models\PackageProducts;
use common\models\Product;
use common\models\User;
use common\models\Users;
use common\models\VehicleTrips;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;

/**
 * Default controller for the `api` module
 */
class ProductsController extends Controller
{
    public static $user = null;
    public static $customer = null;

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->request->isPost) {
            $this->asJson([
                'status' => 0,
                'msg' => 'Invalid request',
                'data' => (object)[],
            ]);
            return false;
        }

        Yii::info(Yii::$app->getRequest()->getRawBody(), 'api_hit');
        if(Yii::$app->request->getHeaders()->get('appToken') == Yii::$app->params['apiHeaderToken']){
            if($action->id == 'register' || $action->id == 'login'){
                return parent::beforeAction($action);
            }

            if(Yii::$app->request->getHeaders()->get('authorization')){
                $token = str_replace('Bearer ', '', Yii::$app->request->getHeaders()->get('authorization'));
                $user = User::findIdentityByAccessToken($token);
                if(!empty($user)){
                    self::$user = $user;
                    self::$customer = Customer::findOne(['user_id' => self::$user->id]);
                    return parent::beforeAction($action);
                }else{
                    $this->asJson([
                        'status' => 0,
                        'msg' => 'invalid user token',
                        'data' => (object)[],
                    ]);
                }
            }else{
                $this->asJson([
                    'status' => 0,
                    'msg' => 'User authentication failed.',
                    'data' => (object)[],
                ]);
            }
            return false;



            /*if(){

            }*/


        }else{
            $this->asJson([
                'status' => 0,
                'msg' => 'invalid api token',
                'data' => (object)[],
            ]);
            return false;
        }
        //echo "<pre>";print_r(Yii::$app->params['apiHeaderToken']);echo "</pre>";die('Call');
        return parent::beforeAction($action);
    }

    public function actionList(){
        $products = Product::find()
            ->where(['status' => 1])
            ->orderBy('product_title')
            ->all();
        $productsData = [];
        if(!empty($products)){
            foreach ($products as $product){
                $productsData[] = [
                    'id' => $product->id,
                    'product_title' => $product->product_title,
                    'fat' => (double)$product->fat,
                    'carbohydrates' => (double)$product->carbohydrates,
                    'protiens' => (double)$product->protiens,
                    'calories' => (double)$product->calories,
                    'price' => (double)$product->price,
                ];
            }
        }
        return [
            'status' => 1,
            'msg' => 'Success',
            'data' => (count($productsData)) ? ['product_list' => $productsData] : (object)[]
        ];
    }








    private function parseErrors($errors){
        $errorWrapper = [];
        foreach ($errors as $error_key => $error){
            $errorWrapper[$error_key] = implode(', ', $error);
        }
        return $errorWrapper;
    }
}
