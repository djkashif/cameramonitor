<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Customer Support Ticket # ' . $model->id;

?>
<style>
    .direct-chat-messages {
        height: 400px;
    }
</style>


<div class="row">
    <div class="col-md-offset-2 col-md-8">
        <!-- DIRECT CHAT -->
        <div class="box box-warning direct-chat direct-chat-warning">
            <div class="box-header with-border">
                <div class="user-block pull-left">
                    <img class="img-circle" src="/img/avatar5.png" alt="User Image">
                    <span class="username"><a
                                href="#"><?= trim($model->createdBy->user->firstName . " " . $model->createdBy->user->lastName) ?></a></span>
                    <span class="description">Created at - <?= date('d M, Y H:i a', strtotime($model->createdOn)) ?></span>
                </div>

                <?php
                if ($model->ticket_status == 1) {
                    ?>
                    <?php $form = ActiveForm::begin(['action' => '/customer-support/close-ticket?id='.$model->id]); ?>
                    <div class="pull-right">
                        <?= Html::submitButton('Close Ticket', ['class' => 'btn btn-success']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <?php
                }
                ?>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Conversations are loaded here -->
                <div class="direct-chat-messages" id="direct-chat-messages">
                    <!-- Message. Default to the left -->
                    <?php
                    foreach ($supportTicketList as $supportMsg) {
                        if ($supportMsg['msg_type'] == 'user') {
                            ?>
                            <div class="direct-chat-msg">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-left"><?= trim($model->createdBy->user->firstName . " " . $model->createdBy->user->lastName) ?></span>
                                    <span class="direct-chat-timestamp pull-right"><?= $supportMsg['time'] ?></span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="/img/user.png" alt="message user image">
                                <!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    <?= $supportMsg['msg'] ?>
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="direct-chat-msg right">
                                <div class="direct-chat-info clearfix">
                                    <span class="direct-chat-name pull-right">Admin</span>
                                    <span class="direct-chat-timestamp pull-left"><?= $supportMsg['time'] ?></span>
                                </div>
                                <!-- /.direct-chat-info -->
                                <img class="direct-chat-img" src="/img/avatar5.png" alt="message user image">
                                <!-- /.direct-chat-img -->
                                <div class="direct-chat-text">
                                    <?= $supportMsg['msg'] ?>
                                </div>
                                <!-- /.direct-chat-text -->
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                    }
                    ?>
                </div>
                <!--/.direct-chat-messages-->


            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <?php if ($model->ticket_status == 1) {
                    ?>
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="input-group">

                        <?= $form->field($dynamicModel, 'ticket_response', [
                            'template' => '{input}', // Leave only input (remove label, error and hint)
                            'options' => [
                                'tag' => false, // Don't wrap with "form-group" div

                            ],
                        ]) ?>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning btn-flat">Send</button>

                          </span>


                    </div>
                    <?php ActiveForm::end(); ?>
                    <?php
                }
                ?>

            </div>
            <!-- /.box-footer-->
        </div>
        <!--/.direct-chat -->
    </div>

</div>


<?php
$this->registerJs("
$(\"#direct-chat-messages\").animate({ scrollTop: $('#direct-chat-messages').prop(\"scrollHeight\")}, 1000);
");
?>