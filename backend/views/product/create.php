<?php

use yii\helpers\Html;


$this->title = 'Create Product';

?>
<div class="customer-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
