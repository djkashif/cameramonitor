<?php
namespace backend\modules\rbac\search;

use backend\modules\rbac\models\AuthItem;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

class RolesSearch extends AuthItem
{
    public function rules()
    {
        return [
            [["name"], "string", "max" => 64]
        ];
    }
    
    public function search($params)
    {
        $this->load($params);
        
        if (!$this->validate()) {
            return new ArrayDataProvider();
        }
        
        $query = self::find()
                ->andWhere(["type" => 1])
                ->andFilterWhere(["like", "name", $this->name]);
        
        $dataProvider = new ActiveDataProvider([
            "query" => $query,
            "sort" => ["defaultOrder" => ["name" => SORT_DESC]]
        ]);
        
        return $dataProvider;
    }
}