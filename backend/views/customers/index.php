<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VehicleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
$this->params["headerButtons"][] = Html::a(Yii::t("app", "Advance Search"), "javascript:;", ["class" => "btn btn-primary", "data" => ["toggle" => "modal", "target" => "#searchModal"]]);
if (Yii::$app->user->can("createCustomer")) {
    $this->params["headerButtons"][] = Html::a(Yii::t("app", "Create"), Url::to(["create"]), ["class" => "btn btn-primary"]);
}
if (Yii::$app->user->can("exportCustomers")) {
    $this->params["headerButtons"][] = Html::a(Yii::t("app", "Export Customers"), Url::to(["export-all-customers"]), ["class" => "btn btn-warning"]);
}

echo $this->render("_search", [
    "searchModel" => $searchModel,
    'cityList' => $cityList
]);
if (Yii::$app->user->can("updateCustomer") || Yii::$app->user->can("deleteCustomer")) {
    $this->registerJs("
        $(\".btn-confirm\").click(function () {
            var btn = $(this);

            $.showModal({
                title: \"Confirmation\",
                body: btn.attr(\"data-body\"),
                buttons: \"<a href=\\\"\" + btn.attr(\"data-href\") + \"\\\" class=\\\"btn btn-primary btn-confirm\\\" data-method=\\\"post\\\">CONFIRM</button>\"
            });
        });
    ");
}
?>
<div class="vehicle-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'accountNumber',
            //'firstName',
            [
                "label" => Yii::t("app", "Name"),
                "format" => "raw",
                "value" => function ($data) {
                    return trim($data->user->firstName . " " . $data->user->lastName);
                }
            ],
            [
                "label" => Yii::t("app", "City"),
                "format" => "raw",
                "value" => function ($data) {
                    return trim($data->city->name);
                }
            ],
            //'lastName',
            'contactNumber',
            'cnic',
            [
                "label" => Yii::t("app", "CODE"),
                "format" => "raw",
                "value" => function ($data) {

                    return 'CST' . $data->id;
                }
            ],
            /*[
                "label" => Yii::t("app", "Barcode"),
                "format" => "raw",

                "value" => function ($model) {
                    return HTML::img('/barcodes/' . $model->barcode, ["width" => 200, "height" => 25]);
                }
            ],*/
            [

                "label" => Yii::t("app", "Status"),
                "format" => "raw",
                "headerOptions" => ["style" => "width: 8%"],
                "contentOptions" => ["class" => "text-center"],
                "value" => function ($data) {
                    $status = "success";
                    $label = Yii::t("app", "Active");

                    if ($data->user->active !== 1) {
                        $status = "danger";
                        $label = Yii::t("app", "Not Active");
                    }

                    $html = "<span class=\"label label-sm label-{$status}\">{$label}</span>";

                    return $html;
                }
            ],
            'createdOn',


            //['class' => 'yii\grid\ActionColumn'],
            [
                "class" => "yii\\grid\\ActionColumn",
                "headerOptions" => ["class" => "text-center", "style" => "width: 8%"],
                "contentOptions" => ["class" => "text-center"],


                "template" => "{dashboard}{update}{changestatus}{delete}",
                "buttons" => [
                    "update" => function ($url, $model) {
                        if (Yii::$app->user->can("updateCustomer")) {
                            return Html::a("<span class=\"glyphicon glyphicon-pencil\"></span>", $url, ["title" => "Update User"]);
                        }
                    },

                    "dashboard" => function ($url, $model) {
                        if (Yii::$app->user->can("viewCustomerDashboard")) {
                            return Html::a("<span class=\"fa fa-tachometer \"></span>", $url, ["title" => "View Customer Dashboard"]);
                        }
                    },
                    "changestatus" => function ($url, $model) {
                        if (Yii::$app->user->can("updateCustomer")) {
                            $icon = ($model->user->active === 1) ? "glyphicon-repeat" : "glyphicon-refresh";
                            $text = ($model->user->active === 1) ? "deactivate" : "activate";

                            return Html::a("<span class=\"glyphicon {$icon}\"></span>", "javascript:;", ["title" => ucwords($text) . " Customer", "class" => "btn-confirm", "data" => ["href" => $url, "body" => Yii::t("app", "Are you sure you want to {$text} this customer?")]]);
                        }
                    },
                    "delete" => function ($url, $model) {

                        if (Yii::$app->user->can("deleteCustomer")) {
                            return Html::a("<span class=\"glyphicon glyphicon-trash\"></span>", "javascript:;", ["title" => "Delete User", "class" => "btn-confirm", "data" => ["href" => $url, "body" => Yii::t("app", "Are you sure you want to delete user #{$model->id}?") . "<br /><br /><strong>" . Yii::t("app", "THIS ACTION IS IRREVERSIBLE") . "</strong>"]]);
                        }
                    }
                ],
                "urlCreator" => function ($action, $model, $key, $index) {
                    if ($action === "update") {
                        return Url::to(["update", "id" => $model->id]);
                    }

                    if ($action === "dashboard") {
                        return Url::to(["dashboard", "id" => $model->id]);
                    }
                    if ($action === "changestatus") {
                        return Url::to(["changestatus", "id" => $model->id]);
                    }

                    if ($action === "delete") {
                        return Url::to(["delete", "id" => $model->id]);
                    }
                }
            ]
        ],
    ]); ?>


</div>
