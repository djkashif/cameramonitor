<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = Yii::t("app", "Bulk Update - Packages");

?>

<div class="box">
    <?php $form = ActiveForm::begin(["options" => ["enctype" => "multipart/form-data"]]) ?>
    <div class="box-body with-border">
        <div class="row">
            <div class="col-md-4">
                <?php echo $form->field($fileModel, "packages")->fileInput(["accept" => ".xlsx, .xls, .csv"])->label("Packages File"); ?>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?php echo Html::a(Yii::t("app", "Back"), Url::to(["index"]), ["class" => "btn btn-default"]); ?>
        <?php echo Html::submitButton(Yii::t("app", "Submit"), ["class" => "btn btn-primary pull-right"]); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>