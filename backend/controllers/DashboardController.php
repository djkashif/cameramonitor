<?php

namespace backend\controllers;

use backend\components\GeneralHelper;
use yii\filters\AccessControl;

class DashboardController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => false,
                        "roles" => [
                            "Customer"
                        ],
                    ],
                    [
                        "allow" => true,
                        "roles" => [
                            "@"
                        ],
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication");
                }
            ]
        ];
    }
    public function actionIndex()
    {


        return $this->render('index');
    }

}
