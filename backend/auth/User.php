<?php
namespace backend\auth;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\User as YiiUser;

class User extends YiiUser
{
    public function can($permissionName, $params = array(), $allowCaching = true)
    {
        $roles = Yii::$app->session->get("roles");
        if ($permissionName != "Customer" && !empty($roles) && ArrayHelper::keyExists("Admin", $roles)) {
            return true;
        }
        
        return parent::can($permissionName, $params, $allowCaching);
    }
}