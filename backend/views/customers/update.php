<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Vehicle */

$this->title = 'Update Customer: ' . $userModel->firstName;

?>
<div class="customer-update">
    <?= $this->render('_form', [
        'customerModel' => $customerModel,
        'userModel' => $userModel,
        'countryList' => $countryList,
        'dynamicModel' => $dynamicModel,
        'stateList' => $stateList,
    ]) ?>

</div>
