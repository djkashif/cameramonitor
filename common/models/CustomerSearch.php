<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * VehicleSearch represents the model behind the search form of `common\models\Vehicle`.
 */
class CustomerSearch extends Customer
{
    public $firstName;
    public $lastName;
    public function rules()
    {
        return [
            [["accountNumber", "firstName", "cnic", "city_id"], "string", "max" => 255]
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Customer::find();
        //$query->join('inner', 'users');
        $query->innerJoinWith('user');


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'createdOn' => $this->createdOn,
            'modifiedOn' => $this->modifiedOn,
        ]);

        $query
            ->andFilterWhere(['like', 'users.firstName', $this->firstName])
            ->andFilterWhere(['like', 'users.lastName', $this->lastName])
            ->andFilterWhere(['like', 'cnic', $this->cnic])
            ->andFilterWhere(['like', 'customer.city_id', $this->city_id])
            ->andFilterWhere(['like', 'accountNumber', $this->accountNumber]);

        return $dataProvider;
    }
}
