<?php

namespace backend\modules\customer\controllers;

use backend\components\GeneralHelper;
use common\models\Customer;
use common\models\Meals;
use common\models\Product;
use Yii;
use common\models\Package;
use common\models\PackageProducts;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Default controller for the `rbac` module
 */
class PackageController extends Controller
{
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Customer"],
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication");
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $customer  = Customer::findOne(['user_id' => Yii::$app->user->id]);
        $pkgOfTheDay = Package::find()->where([
            'package_status' => 1,
            'customer_id' => $customer->id
        ])
            ->andWhere([
                '<=', 'package_start_date', date('Y-m-d')
            ])
            ->andWhere([
                '>', 'package_expiration', date('Y-m-d')
            ])->one();

        if(empty($pkgOfTheDay)){
            GeneralHelper::showErrorMsg('No package found');
            return $this->redirect('/customer');
        }



        $packageMealDetails = [];
        $packageProducts = PackageProducts::find()->where(['package_id' => $pkgOfTheDay->id])->all();
        if (!empty($packageProducts)) {
            foreach ($packageProducts as $packageProduct) {
                $packageMealDetails['products'][date('d-M-Y', strtotime($packageProduct->serve_date))][$packageProduct->meal_id][] = $packageProduct->product_id;
                $packageMealDetails['status'][date('d-M-Y', strtotime($packageProduct->serve_date))] = $packageProduct->status;
            }
        }

        $packageDateList = [];
        $period = new \DatePeriod(
            new \DateTime(date('Y-m-d', strtotime($pkgOfTheDay->package_start_date))),
            new \DateInterval('P1D'),
            new \DateTime(date('Y-m-d', strtotime($pkgOfTheDay->package_expiration)))
        );
        foreach ($period as $key => $value) {
            $packageDateList[] = $value->format('d-M-Y');
        }


        $packageMeals = [];
        if ($pkgOfTheDay->meals) {
            foreach ($pkgOfTheDay->meals as $product) {
                $packageMeals[] = $product->meal_id;
            }
        }
        $products = ArrayHelper::map(Product::find()->all(), "id", "product_title");
        $meals = ArrayHelper::map(Meals::find()->all(), "id", "meal_title");
        $pkgStatusList = [
            1 => 'Activated',
            2 => 'On Hold',
            3 => 'Deactivated',
        ];
        //echo "<pre>";print_r($packageMealDetails);echo "</pre>";die('Call');

        return $this->render('index', [
            'pkgOfTheDay' => $pkgOfTheDay,
            'meals' => $meals,
            'packageMeals' => $packageMeals,
            'pkgStatusList' => $pkgStatusList,
            'packageDateList' => $packageDateList,
            'products' => $products,
            'packageMealDetails' => $packageMealDetails,

        ]);
    }

    public function actionMeals()
    {


        if (!Yii::$app->request->post()) {
            GeneralHelper::showErrorMsg('Invalid request');
            return $this->redirect('/customer/package/index');
        }
        $package_id = Yii::$app->request->post('package');
        $package = Package::findOne($package_id);
        if ($package == null) {
            GeneralHelper::showErrorMsg('Invalid package request');
            return $this->redirect('/customer/package/index');
        }

        PackageProducts::deleteAll(['package_id' => $package_id]);

        $package_date_meals = Yii::$app->request->post('package_date_meals');


        $products = $package_date_meals['products'];
        $statuses = $package_date_meals['status'];

        foreach ($products as $date => $date_products) {
            foreach ($date_products as $meal_id => $date_products) {
                if (!empty($date_products)) {
                    foreach($date_products as $date_product){
                        $packageProduct = new PackageProducts();
                        $packageProduct->package_id = $package_id;
                        $packageProduct->meal_id = $meal_id;
                        $packageProduct->product_id = $date_product;
                        $packageProduct->serve_date = date('Y-m-d', strtotime($date));
                        $packageProduct->status = $statuses[$date];


                        $packageProduct->save();
                    }

                }
            }
        }


        GeneralHelper::showSuccessMsg('Meals saved successfully');
        return $this->redirect('/customer/package/index');
    }
}
