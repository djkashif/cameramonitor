<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class HandsOnTableAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

        'css/handsontable.full.min.css',
    ];
    public $js = [
        /*'js/jquery.min.js',*/
        'js/languages.min.js',
        'js/handsontable.full.min.js',
    ];
    public $depends = [
        '\backend\assets\AppAsset',
        /*'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',*/
    ];
}
