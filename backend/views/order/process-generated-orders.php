<?php
$this->title = 'Process Generated Orders';
\backend\assets\HandsOnTableAsset::register($this);
use yii\widgets\ActiveForm;

?>
    <style>
        .hot-container {
            width: 500px;
            height: 500px;
            overflow: hidden;
        }

        li.valid-item {
            background-color: chartreuse;
        }

        ul.grid_list_item li {
            font-size: 15px;
        }

        .orders-table th.orders-table td {
            border-top-width: 0;
            border-left-width: 0;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            height: 22px;
            empty-cells: show;
            line-height: 21px;
            padding: 0 4px;
            background-color: #fff;
            vertical-align: top;
            overflow: hidden;
            outline-width: 0;
            white-space: pre-line;
            background-clip: padding-box;
        }

        .orders-table th {
            border: 1px solid #ccc !important;
            /*border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            border-left: 1px solid #ccc;*/
            background-color: #f0f0f0;
            color: #222;
            text-align: center;
            font-weight: 400;
            white-space: nowrap;
            padding: 0 !important;
        }

        .orders-table td {
            border: 1px solid #ccc !important;
            color: #222;
            text-align: center;
            font-weight: 400;
            white-space: nowrap;
            padding: 2px 0 2px 0 !important;
        }
        .orders-table  tr.selected-customer-row td {
            border: 2px solid #3c8dbc !important;

        }

        td.action {
            //height: 20px;
        }
        .td_qty_input{
            padding: 0 0 0 0 !important;
        }
        .qty_input{
            width: 34px;
            padding: 0;
            text-align: center;
            height: 17px;
        }
        .field-dynamicmodel-import_orders{
            display: none;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="col-md-12">

                                    <input type="text" class="form-control" name="barcode"
                                           placeholder="Scan barcode here..."
                                           onchange="PackageManager.scanBarcode()" value='' style="">
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => 'date-select-form']]); ?>
                            <?=
                                $form->field($dateModel, 'date')->widget(\kartik\date\DatePicker::classname(), [
                                        'options' => ['placeholder' => 'Select Date'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'yyyy-mm-dd',
                                            //'startDate' => date("Y-m-d")
                                        ],
                                    'pluginEvents' =>[
                                        "changeDate" => "function(e) {  $('form.date-select-form').submit()}",
                                    ]
                                ])->label(false);
                            ?>
                            <?php ActiveForm::end(); ?>

                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-success  pull-right"
                                    onclick="PackageManager.completeGeneratedOrder()"><i class="fa fa-save">&nbsp;</i>Complete
                                Order
                            </button>


                            <?php $form = ActiveForm::begin(["action" => "/order/import-orders-qty", "options" => ["enctype" => "multipart/form-data"]]) ?>
                            &nbsp;&nbsp;

                            <button type="button" class="btn btn-warning  pull-left"
                                    onclick="PackageManager.exportGeneratedOrder()"><i class="fa fa-download">&nbsp;</i>
                                Export Orders
                            </button>
                            <button type="button" class="btn"
                                    onclick="PackageManager.importGeneratedOrder()"><i class="fa fa-upload">&nbsp;</i>
                                Import Order Qty
                            </button>
                                <button type="button" class="btn upload-imported-btn" style="display: none;"
                                        onclick="PackageManager.uploadImportedOrders(this)"><i class="fa fa-upload">&nbsp;</i>
                                    <span>Upload</span>
                                </button>
                            <?php echo $form->field($fileModel, "import_orders")->fileInput(["accept" => ".xlsx, .xls, .csv", "style" => "display : none;"])->label(false); ?>
                            <?php ActiveForm::end(); ?>

                            <span class="pull-right">&nbsp;&nbsp;&nbsp;</span>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- Default box -->
            <div class="box">

                <div class="box-body">
                    <!--<div id="example1" class="hot handsontable htColumnHeaders"></div>-->
                    <div class="orders-table-wrapper">
                        <table class="table table-responsive orders-table">
                            <thead>
                            <tr>
                                <th>Customer Account</th>
                                <th>Customer Title</th>
                                <th>Meal Type</th>
                                <th>Meal Items</th>
                                <th>UOM</th>
                                <th>Qty</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php

                            foreach ($orderData as $customerAccount => $orderDetail) {
                                ?>
                                <tr data-customer="<?= $customerAccount ?>"  data-customer-id="<?= $orderDetail['customer_id'] ?>" onclick="PackageManager.OrderGeneration.customFocusOnSelectedRow(this)">
                                    <td rowspan="<?= count($orderDetail['items']) ?>"><?= $customerAccount ?></td>
                                    <td rowspan="<?= count($orderDetail['items']) ?>"><?= $orderDetail['customerName'] ?></td>
                                    <?php
                                    foreach ($orderDetail['items'] as $itemIndex => $item){
                                        $startHtml = ($itemIndex == 0) ? "" : "</tr><tr data-customer=\"$customerAccount\"   data-customer-id=\"".$orderDetail['customer_id']."\" onclick=\"PackageManager.OrderGeneration.customFocusOnSelectedRow(this)\">";
                                        $endHtml = ($itemIndex == 0) ? "" : "</tr>";
                                        echo $startHtml.
                                             "<td data-item-id='".$item['product_id']."' data-item-cell='meal_type'>{$item['meal_title']}</td>";
                                        echo "<td data-item-id='".$item['product_id']."' data-item-cell='title'>{$item['product_title']}</td>";
                                        echo "<td data-item-id='".$item['product_id']."' data-item-cell='uom_qty'>{$item['product_unit_measurement_qty']} {$item['product_uom']}</td>";
                                        echo "<td data-item-id='".$item['product_id']."' data-order-item-id='".$item['order_meal_item_id']."' data-item-cell='qty' class='td_qty_input'><input type='text' value='{$item['order_meal_item_qty']}' data-old-value='{$item['order_meal_item_qty']}' name='qty[{$item['product_id']}]' onchange='PackageManager.updateOrderItemQty(this)' class='qty_input'></td>";
                                        echo "<td data-item-id='".$item['product_id']."' data-item-cell='status' data-transaction-id='".$item['transaction_id']."'  data-item-value='".$item['order_meal_item_status']."' data-item-verified='".(($item['order_meal_item_status'] == 2) ? 1 : 0)."' data-order-meal-item-id='".$item['order_meal_item_id']."'>".\backend\components\OrderManager::createStatusLabel($item['order_meal_item_status'])."</td>";
                                        echo "<td class='action'>
                                                    <span>
                                                    <input type=\"checkbox\" ".(($item['order_meal_item_status'] == 4) ? 'checked' : '')." data-toggle=\"toggle\" data-size=\"mini\" data-onstyle=\"danger\" data-on=\"On Hold\" data-off=\"Hold\" onchange=\"PackageManager.OrderGeneration.toggleHoldOrderItem(this, ".$item['order_meal_item_id'].")\">
                                                    </span>
                                                    <span class='hold-loader-span'></span>
                                                    
                                                    </td>".$endHtml;
                                    }

                                    ?>
                                </tr>

                                <?php
                            }
                            ?>
                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>
                <!-- /.box-footer-->
            </div>

        </div>

    </div>

    <script>


    </script>
<?php
$this->registerCssFile("@web/css/bootstrap-toggle.min.css", [
    'depends' => [
        \backend\assets\AppAsset::className()
    ]
]);
$this->registerJsFile("@web/js/bootstrap-toggle.min.js", [
    'depends' => [
        \backend\assets\AppAsset::className()
    ]
]);

$this->registerJs("
    $('a.sidebar-toggle').click();
    $('input[type=\"file\"][name=\"DynamicModel[import_orders]\"]').on('change', function(){
            PackageManager.enableImportOrdersSubmit();
        })
    
   ");

?>