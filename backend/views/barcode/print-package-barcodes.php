<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Label Printing';
?>
<style>
    #iframe-wrapper{
        display: none;
    }
</style>
<!-- Default box -->
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Select Criteria</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $form = ActiveForm::begin(['method' => 'get']); ?>
                        <div class="row">
                            <div class="col-md-3">
                                <?php
                                // DateRangePicker in a dropdown format (uneditable/hidden input) and uses the preset dropdown.

                                echo '<div class="drp-container">';
                                echo \kartik\date\DatePicker::widget([
                                    'model' => $dynamicModel,
                                    'attribute' => 'packageDate',
                                    'options' => ['placeholder' => 'Enter date ...'],
                                    'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'dd-M-yyyy',
                                        'todayHighlight' => true
                                    ]
                                ]);
                                echo '</div>';
                                ?>
                            </div>
                            <div class="col-md-3">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default meal-btns" data-meal-type="1" onclick="PackageManager.selectMeal(this)">Breakfast</button>
                                    <button type="button" class="btn btn-default meal-btns" data-meal-type="2" onclick="PackageManager.selectMeal(this)">Lunch</button>
                                    <button type="button" class="btn btn-default meal-btns" data-meal-type="3" onclick="PackageManager.selectMeal(this)">Dinner</button>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <?=
                                $form->field($dynamicModel, 'customer_id')->widget(\kartik\select2\Select2::classname(), [
                                    'data' => $customersList,
                                    'options' => ['placeholder' => 'Select Customers ...', 'multiple' => true],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],

                                ])->label(false);

                                ?>

                            </div>
                            <div class="col-md-3">
                                <?= Html::button('Generate Labels', ['class' => 'btn btn-warning', 'onclick' => 'PackageManager.generateLabels(this)']) ?>

                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-3">
                                <?=
                                $form->field($dynamicModel, 'city_id')->widget(\kartik\select2\Select2::classname(), [
                                    'data' => $cityList,
                                    'options' => ['placeholder' => 'Select City ...', 'multiple' => false],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],

                                ])->label('City');


                                ?>
                            </div>


                        </div>



                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        
    </div>
    <!-- /.box-footer-->
</div>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Label Viewer</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12" id="iframe-wrapper">
                <iframe src="" width="100%" height="400px" id="">

                </iframe>
            </div>
        </div>
    </div>
    <div class="box-footer">

    </div>
    <!-- /.box-footer-->
</div>
<!-- /.box -->