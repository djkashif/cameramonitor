<?php

namespace backend\components;

use common\models\Customer;
use common\models\SupportTickets;
use common\models\Transactions;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class TicketsManager
{
    public static $statusLables = [
        '1' => 'Open',
        '2' => 'Hold',
        '3' => 'Closed'
    ];


    public static function createStatusLabel($status)
    {
        $statusTitle = self::$statusLables[$status];
        switch ($status) {
            case 1:
                $label = "<label class='label label-warning'>{$statusTitle}</label>";
                break;
            case 2:
                $label = "<label class='label label-danger'>{$statusTitle}</label>";
                break;
            case 3:
                $label = "<label class='label label-success'>{$statusTitle}</label>";
                break;
        }
        return $label;
    }

    public static function createNewTicket($data){
        $ticket = new SupportTickets();
        $ticket->ticket_title = $data['ticket_title'];
        $ticket->ticket_dept = $data['ticket_dept'];
        $ticket->ticket_description = $data['ticket_description'];
        $ticket->created_by = $data['created_by'];
        $ticket->ticket_status = 1;
        if(!$ticket->save()){
            return false;
        }
        return $ticket->id;
    }



    public static function findTicket($id){
        if (($model = SupportTickets::findOne($id)) !== null) {
            return $model;
        }
        return false;


    }
}

