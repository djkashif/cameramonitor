<?php
$this->title = 'New Orders';
\backend\assets\HandsOnTableAsset::register($this);

?>
    <style>
        .hot-container {
            width: 500px;
            height: 500px;
            overflow: hidden;
        }
        li.valid-item{
            background-color: chartreuse;
        }
        ul.grid_list_item li{
            font-size: 15px;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-default" onclick="PackageManager.addNewOrderRow()"><i class="fa fa-plus"></i>Add New Order</button>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <!--<input type="text" class="form-control" name="customer_barcode" placeholder="Scan Customer barcode here..." onkeyup="PackageManager.scanCustomerBarcodeOnKeyEnter(event)" onchange="PackageManager.scanBarcodeCustomer()" value='' style="display: none">-->
                                    <input type="text" class="form-control" name="customer_barcode" placeholder="Scan Customer barcode here..." onchange="PackageManager.scanBarcodeCustomer()" value='' style="display: none">
                                    <!--<input type="text" class="form-control" name="package_barcode" placeholder="Scan Package barcode here..." onkeyup="PackageManager.scanPackageBarcodeOnKeyEnter(event)" onchange="PackageManager.scanBarcodePackage()" value='' style="display: none">-->
                                    <input type="text" class="form-control" name="item_barcode" placeholder="Scan Item barcode here..." onchange="PackageManager.scanItemBarcode()" value='' style="display: none">
                                    <br>
                                    <!--{"type":"CST","id":"3"}<br>{"type":"PKG","id":"3"}-->
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="btn-group">
                                <?php
                                foreach ($meals as $mealId => $meal){
                                    ?>
                                    <button type="button" class="btn btn-default meal-btns" data-meal-type="<?= $mealId ?>" onclick="PackageManager.selectMeal(this)"><?= $meal ?></button>
                                    <?php
                                }
                                ?>


                            </div>
                        </div>
                        <div class="col-md-3">
                            <a href="/order/clear-draft" class="btn btn-warning"><i class="fa fa-trash">&nbsp;</i>Clear Draft</a>
                            <button type="button" class="btn btn-success" onclick="PackageManager.completeOrder()"><i class="fa fa-save">&nbsp;</i>Complete Order</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- left column -->
        <div class="col-md-8">
            <!-- Default box -->
            <div class="box">
                <!--<div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-3">
                            <button type="button" class="btn btn-success" onclick="PackageManager.addNewOrderRow()"><i class="fa fa-plus"></i>Add New Order</button>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <input type="text" class="form-control" name="barcode" placeholder="Scan barcode here..." onkeyup="PackageManager.scanBarcodeOnKeyEnter(event)" onchange="PackageManager.scanBarcode()" value='<?/*= json_encode([
                                    'type' => 'CST',
                                    'id' => '3',
                                ]) */?>'>
                            </div>
                        </div>
                    </div>
                </div>-->
                <div class="box-body">
                    <div id="example1" class="hot handsontable htColumnHeaders"></div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="row">

                        <!-- /.col -->

                        <!-- /.col -->

                        <!-- fix for small devices only -->


                        <div class="col-md-offset-2 col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Customers</span>
                                    <span class="info-box-number totalCustomers">0</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->

                        </div>
                        <!-- /.col -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Amount</span>
                                    <span class="info-box-number totalPrice">0</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
                <!-- /.box-footer-->
            </div>

        </div>
        <div class="col-md-4">


            <div class="box">
                <div class="box-header with-border">
                    <h4>Scan Results</h4>
                </div>
                <div class="box-body">
                    <div class="row" id="barcode-loader" style="display: none">
                        <div class="col-md-12 text-center">
                            <i class="fa fa-refresh fa-spin" style="font-size: 80px;"></i>
                        </div>
                    </div>
                    <div class="row customer-information" style="display: none">
                        <div class="col-md-12">
                            <div class="box box-widget widget-user-2">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header bg-yellow">
                                    <div class="widget-user-image">
                                        <img class="img-circle" src="/img/user.png" alt="User Avatar">
                                    </div>
                                    <!-- /.widget-user-image -->
                                    <h3 class="widget-user-username customer_name">Nadia Carmichael</h3>
                                    <h5 class="widget-user-desc customer_account">Lead Developer</h5>
                                </div>
                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                        <li><a href="#"><strong>Contact #</strong> <span class="pull-right badge bg-blue customer_contact">31</span></a></li>
                                        <li><a href="#"><strong>Address</strong> <span class="pull-right customer_address"></span></a></li>

                                    </ul>
                                </div>
                                <div class="box-footer no-padding">
                                    <ul class="nav nav-stacked">
                                        <li><a href="#"><strong>Package Of The Day :</strong> <span class="pull-right badge bg-blue pkg_of_day">31</span></a></li>
                                    </ul>
                                </div>
                                <div class="meal_item_list">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
    </div>

<script>


</script>
<?php

$this->registerJs("
PackageManager.prepareHandsOnTableForNewOrders();
");
$this->registerJs("$('a.sidebar-toggle').click();PackageManager.resetScanForm();PackageManager.updateHandsOnTableWidth();");
if($userDraftTransactions['hasDraft']){
    $this->registerJs("
        PackageManager.draftTransactionSaved = {$userDraftTransactions['data']};
        PackageManager.loadTransactionsFromDraft();
        
");
}
$this->registerJs("
        PackageManager.initDraftTransactions();
");


?>