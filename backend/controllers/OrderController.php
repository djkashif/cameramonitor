<?php

namespace backend\controllers;

use backend\components\GeneralHelper;
use backend\components\OrderManager;
use common\models\Cities;
use common\models\Customer;
use common\models\DraftTransactions;
use common\models\Meals;
use common\models\OrderMealItems;
use common\models\OrderMeals;
use common\models\Orders;
use common\models\OrderSearch;
use common\models\Package;
use common\models\PackageProducts;
use common\models\Product;
use common\models\Transactions;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use yii\base\DynamicModel;
use yii\filters\AccessControl;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class OrderController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Admin"],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['viewOrders']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['viewOrders']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['process-new-orders'],
                        'roles' => ['orderProcessing']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['generate-order', 'save-generate-order'],
                        'roles' => ['orderGenerate']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['process-generated-orders'],
                        'roles' => ['processingGeneratedOrders']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['get-barcode-data', 'draft-transaction', 'save-transaction', 'clear-draft', 'hold-order-meal-item', 'verify-order-meal-item', 'complete-order-meal-items'],
                        'roles' => ['@']
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication");
                }
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 20;
        return $this->render("index2", [
                "searchModel" => $searchModel,
                "dataProvider" => $dataProvider
            ]
        );
    }

    public function actionProcessNewOrders()
    {
        $meals = ArrayHelper::map(Meals::find()->all(), "id", "meal_title");

        $userDraftTransaction = DraftTransactions::find()->where(['user_id' => Yii::$app->user->id])->one();
        $userDraftTransactions = [
            'hasDraft' => false,
            'data' => json_encode([]),
        ];

        if (!empty($userDraftTransaction)) {
            $userDraftTransactions['hasDraft'] = true;
            $userDraftTransactions['data'] = (json_decode($userDraftTransaction->transaction_data)) ? json_decode($userDraftTransaction->transaction_data, 1) : [];
            $userDraftTransactions['data'] = json_encode($userDraftTransactions['data']);
        }


        return $this->render('process-new-orders', [
            'meals' => $meals,
            'userDraftTransactions' => $userDraftTransactions,
        ]);
    }

    public function actionExportOrderMealItems($items){
        $items = (base64_decode($items)) ? explode(',', base64_decode($items)) : [];
        $result = [];
        if(count($items)){
            $itemsImploded = implode(',', $items);
            $query = " select
                    o.id as 'order_id',
                    omi.id as 'order_meal_item_id',
                    omi.status as  'order_meal_item_status',
                    omi.qty as  'order_meal_item_qty',
                    c.accountNumber,
                    c.id as 'customer_id',
                    u.firstName,
                    u.lastName,
                    prd.product_title,
                    prd.uom as product_uom,
                    prd.unit_measurement_qty as product_unit_measurement_qty,
                    prd.id as 'product_id',
                    m.meal_title,
                    m.id as 'meal_type_id',
                    o.order_date
                    from orders o
                    inner JOIN order_meals om on om.order_id = o.id
                    inner JOIN order_meal_items omi on omi.order_meal_id = om.id
                    INNER JOIN product prd on prd.id = omi.product_id
                    INNER JOIN customer c on c.id = o.customer_id
                    inner join users u on u.id = c.user_id
                    inner join package p on p.id = o.package_id
                    inner join meals m on m.id = om.meal_id
                    where 
                    omi.id in ({$itemsImploded}) 
                    and omi.status in (1,2,4)";
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand($query);
            $result = $command->queryAll();
        }

        $data = [];
        if(!empty($result)){
            foreach ($result as $item){
                $data[] = [
                    $item['order_meal_item_id'],
                    $item['accountNumber'],
                    trim($item['firstName']." ".$item['firstName']),
                    $item['meal_title'],
                    $item['product_title'],
                    trim($item['product_unit_measurement_qty']. " " .(isset(\backend\components\GeneralHelper::$unitOfMeasurement[$item['product_uom']])) ? \backend\components\GeneralHelper::$unitOfMeasurement[$item['product_uom']] : 'N/A'),
                    $item['order_meal_item_qty']
                ];
            }
        }

        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                'Orders' => [   // Name of the excel sheet
                    'data' => $data,

                    // Set to `false` to suppress the title row
                    'titles' => [
                        'ID',
                        'Account Number',
                        'Customer',
                        'Meal Type',
                        'Product Title',
                        'Base UOM',
                        'Qty',
                    ],

                    /*'formats' => [
                        // Either column name or 0-based column index can be used
                        'C' => '#,##0.00',
                        3 => 'dd/mm/yyyy hh:mm:ss',
                    ],*/

                    /*'formatters' => [
                        // Dates and datetimes must be converted to Excel format
                        3 => function ($value, $row, $data) {
                            return \PHPExcel_Shared_Date::PHPToExcel(strtotime($value));
                        },
                    ],*/
                ],


            ]
        ]);
        //echo "<pre>";print_r($file);echo "</pre>";die('Call');
        //$file->send('Test.xlsx');
        $file->send(str_replace(' ', '_', 'Order Export').'_'.rand(1000,20000).'.xlsx');
        die;



    }

    public function actionProcessGeneratedOrders($id = null)
    {
        $meals = ArrayHelper::map(Meals::find()->all(), "id", "meal_title");
        $fileModel = new DynamicModel(["import_orders"]);
        $fileModel->addRule(["import_orders"], "file", ["maxSize" => 1024 * 1024, "skipOnEmpty" => false, "extensions" => "xlsx"]);

        $dateModel = new DynamicModel([
            'date',
        ]);
        $dateModel->addRule([
            'date',
        ], 'safe');
        if (Yii::$app->request->get() && $dateModel->load(Yii::$app->request->get())) {
            $tempDate = explode('-', $dateModel->date);
            if(!checkdate($tempDate[1], $tempDate[2], $tempDate[0])){
                $dateModel->date = date('Y-m-d');
            }
        }else{
            $dateModel->date = date('Y-m-d');
        }




        $orderIdCondition = ($id !== null) ?  ' and o.id ='.$id.' ' : '';
        if(!Yii::$app->user->can('Admin') && empty(Yii::$app->user->getIdentity()->city_id)){
            GeneralHelper::showErrorMsg('Your city is not set for operations. Please contact admin');
            return $this->goBack();
        }

        $operationalCityOrdersCondition = (Yii::$app->user->can('Admin')) ? '' : ' AND c.city_id = '.Yii::$app->user->getIdentity()->city_id.' ';

        $query = " select
                    t.id as 'transaction_id',
                    o.id as 'order_id',
                    omi.id as 'order_meal_item_id',
                    omi.status as  'order_meal_item_status',
                    omi.qty as  'order_meal_item_qty',
                    c.accountNumber,
                    c.id as 'customer_id',
                    u.firstName,
                    u.lastName,
                    prd.product_title,
                    prd.uom as product_uom,
                    prd.unit_measurement_qty as product_unit_measurement_qty,
                    prd.id as 'product_id',
                    m.meal_title,
                    m.id as 'meal_type_id',
                    o.order_date
                    from orders o
                    inner join transactions t on o.transaction_id = t.id
                    inner JOIN order_meals om on om.order_id = o.id
                    inner JOIN order_meal_items omi on omi.order_meal_id = om.id
                    INNER JOIN product prd on prd.id = omi.product_id
                    INNER JOIN customer c on c.id = o.customer_id
                    inner join users u on u.id = c.user_id
                    inner join package p on p.id = o.package_id
                    inner join meals m on m.id = om.meal_id
                    where o.order_date = '".($dateModel->date)."' 
                    and omi.status in (1,2,4)
                    $orderIdCondition
                    $operationalCityOrdersCondition
                    ";
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($query);
        $result = $command->queryAll();
        //echo "<pre>";print_r($query);echo "</pre>";die('Call');

        $groupedData = [];
        foreach ($result as $data){
            if(!isset($groupedData[$data['accountNumber']])){
                $groupedData[$data['accountNumber']]['order_id'] = $data['order_id'];
                $groupedData[$data['accountNumber']]['order_date'] = $data['order_date'];
                $groupedData[$data['accountNumber']]['customer_id'] = $data['customer_id'];
                $groupedData[$data['accountNumber']]['customerName'] = trim($data['firstName']." ".$data['lastName']);
            }
            //echo "<pre>";print_r($data);echo "</pre>";die('Call');
            $groupedData[$data['accountNumber']]['items'][] = [
                'customer_id' => $data['customer_id'],
                'order_meal_item_id' => $data['order_meal_item_id'],
                'order_meal_item_status' => $data['order_meal_item_status'],
                'order_meal_item_qty' => $data['order_meal_item_qty'],
                'product_title' => $data['product_title'],
                'product_id' => $data['product_id'],
                'product_unit_measurement_qty' => $data['product_unit_measurement_qty'],
                'product_uom' => (isset(\backend\components\GeneralHelper::$unitOfMeasurement[$data['product_uom']])) ? \backend\components\GeneralHelper::$unitOfMeasurement[$data['product_uom']] : 'N/A',
                'meal_title' => $data['meal_title'],
                'meal_type_id' => $data['meal_type_id'],
                'transaction_id' => $data['transaction_id'],
            ];

        }
        //echo "<pre>";print_r($groupedData);echo "</pre>";die('Call');
        return $this->render('process-generated-orders', [
            'meals' => $meals,
            'orderData' => $groupedData,
            'fileModel' => $fileModel,
            'dateModel' => $dateModel
        ]);
    }

    public function actionGetBarcodeData()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $barcode = Yii::$app->request->post();
        $type = $barcode['type'];
        $id = $barcode['id'];
        $meals = $barcode['meals'];

        switch ($type) {
            case 'CST':
                $data = Customer::findOne($id);
                if (empty($data)) {
                    return [
                        'status' => 0,
                        'data' => [],
                        'msg' => 'No Customer Found',
                    ];
                }
                /*
                 * Get meals of the days
                 * */

                $pkgOfTheDay = Package::find()->where([
                    'package_status' => 1,
                    'customer_id' => $data->id
                ])
                    ->andWhere([
                        '<=', 'package_start_date', date('Y-m-d')
                    ])
                    ->andWhere([
                        '>', 'package_expiration', date('Y-m-d')
                    ])->one();
                $pkgDetails = [];

                $pkgDetails['hasPkg'] = 0;
                //$pkgDetails['meals_count'] = 0;
                if (($pkgOfTheDay)) {
                    $pkgDetails['hasPkg'] = 1;
                    $pkgDetails['package_id'] = $pkgOfTheDay->id;
                    $pkgDetails['title'] = $pkgOfTheDay->package_title;
                    //$pkgDetails['meals_count'] = $pkgOfTheDay->number_of_meals;
                    $pkgDetails['meals_list']['meals'] = [];
                    $pkgMeals = PackageProducts::find()->where([
                        'package_id' => $pkgOfTheDay->id,
                        //'meal_id' => $meal,
                        'serve_date' => date('Y-m-d')
                    ])->andWhere(['in', 'meal_id', $meals])->with(['product', 'mealDetail'])->all();
                    //echo "<pre>";print_r($pkgMeals);echo "</pre>";die('Call');
                    foreach ($pkgMeals as $pkgMeal) {
                        $pkgDetails['meals_list']['meals'][$pkgMeal->meal_id]['meal_type'] = $pkgMeal->meal_id;
                        $pkgDetails['meals_list']['meals'][$pkgMeal->meal_id]['title'] = $pkgMeal->mealDetail->meal_title;
                        if (!isset($pkgDetails['meals_list']['meals'][$pkgMeal->meal_id]['items'])) {
                            $pkgDetails['meals_list']['meals'][$pkgMeal->meal_id]['items'] = [];
                        }
                        $pkgDetails['meals_list']['meals'][$pkgMeal->meal_id]['items'][$pkgMeal->product->id] = [
                            'pid' => $pkgMeal->product->id,
                            'title' => $pkgMeal->product->product_title,
                            'price' => $pkgMeal->product->price,
                        ];
                        /*$pkgDetails['meals_list']['meals'][$pkgMeal->mealDetail->meal_title][$pkgMeal->product->id] = [
                            'pid' => $pkgMeal->product->id,
                            'title' => $pkgMeal->product->product_title
                        ];*/
                    }
                }
                return [
                    'status' => 1,
                    'data' => [
                        'customer_account' => $data->accountNumber,
                        'customer_name' => trim($data->user->firstName . " " . $data->user->lastName),
                        'customer_contact' => $data->contactNumber,
                        'customer_address' => $data->address,
                        'package_of_the_day' => $pkgDetails
                    ],
                    'msg' => 'Customer found',
                ];
                break;
            default:
                return [
                    'status' => 0,
                    'data' => [],
                    'msg' => 'Invalid barcode',
                ];
                break;
        }
    }

    public function actionDraftTransaction()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        if (isset($data['scannedOrders']) && count($data['scannedOrders'])) {
            $draftData ['scannedOrders'] = $data['scannedOrders'];
            $draftData ['verifiedOrders'] = (isset($data['verifiedOrders'])) ? $data['verifiedOrders'] : [];
            $draftData ['selectedMeals'] = (isset($data['selectedMeals'])) ? $data['selectedMeals'] : [];

            $userDraftTransaction = DraftTransactions::find()->where(['user_id' => Yii::$app->user->id])->one();
            $userDraftTransactionData = [];
            if (empty($userDraftTransaction)) {
                $userDraftTransaction = new DraftTransactions();
            } else {
                $userDraftTransactionData = (json_decode($userDraftTransaction->transaction_data)) ? json_decode($userDraftTransaction->transaction_data) : [];
            }
            $userDraftTransaction->user_id = Yii::$app->user->id;
            $userDraftTransaction->transaction_data = json_encode($draftData);
            $userDraftTransaction->save();
        }
    }

    public function actionSaveTransaction()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";
        die('Call');*/


        if (isset($data['verifiedOrders']) && count($data['verifiedOrders'])) {
            $groupedData = [];
            foreach ($data['verifiedOrders'] as $verifiedOrders) {
                $groupedData[$verifiedOrders['customer_account']][$verifiedOrders['package_id']][$verifiedOrders['meal_id']][] = $verifiedOrders['item_id'];
            }


            $transactionTotalPrice = 0;

            $transaction = new Transactions([
                'user_id' => Yii::$app->user->id,
                'grandTotal' => $transactionTotalPrice
            ]);
            $transaction->save();
            foreach ($groupedData as $customerAccount => $customerData) {

                $customer = Customer::find()->where(['accountNumber' => $customerAccount])->one();


                foreach ($customerData as $package_id => $meals) {
                    $orderTotalPrice = 0;
                    $order = new Orders([
                        'customer_id' => $customer->id,
                        'package_id' => $package_id,
                        'transaction_id' => $transaction->id,
                        'grandTotal' => $orderTotalPrice,
                    ]);

                    $order->save();


                    foreach ($meals as $meal_id => $meal_items) {
                        $mealTotalPrice = 0;
                        $orderMeal = new OrderMeals([
                            'order_id' => $order->id,
                            'meal_id' => $meal_id,
                            'grandTotal' => $mealTotalPrice,
                        ]);
                        $orderMeal->save();
                        foreach ($meal_items as $meal_item) {

                            $itemDetail = Product::find()->where(['id' => $meal_item])->one();
                            $mealItem = new OrderMealItems([
                                'order_meal_id' => $orderMeal->id,
                                'product_id' => $meal_item,
                                'price' => $itemDetail->price,
                                'status' => 1,
                            ]);
                            $mealItem->save();
                            $mealTotalPrice += $itemDetail->price;
                            $orderTotalPrice += $itemDetail->price;
                            $transactionTotalPrice += $itemDetail->price;
                        }
                        $orderMeal->grandTotal = $mealTotalPrice;
                        $orderMeal->save();
                    }

                    $order->grandTotal = $orderTotalPrice;
                    $order->save();

                }
            }
            $transaction->grandTotal = $transactionTotalPrice;
            $transaction->save();
            DraftTransactions::deleteAll(['user_id' => Yii::$app->user->id]);


            return [
                'status' => 1,
                'data' => [],
                'msg' => 'Transaction saved successfully',
            ];


        } else {
            return [
                'status' => 0,
                'data' => [],
                'msg' => 'Invalid data',
            ];
        }

    }

    public function actionSaveGenerateOrder()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();




        if (isset($data['data']) && base64_decode($data['data'])) {
            $orders = json_decode(base64_decode($data['data']), 1);
            //echo "<pre>";print_r($orders);echo "</pre>";die('Call');

            $groupedData = [];

            $serveDate = null;
            //echo "<pre>";print_r($orders);echo "</pre>";die('Call');

            foreach ($orders as $verifiedOrders) {
                $serveDate = ($serveDate === null) ? $verifiedOrders['serve_date'] : $serveDate;
                $groupedData[$verifiedOrders['customer_account']][$verifiedOrders['package_id']][$verifiedOrders['meal_id']][] = $verifiedOrders['item_id'];
            }



            $transactionTotalPrice = 0;

            $transaction = new Transactions([
                'user_id' => Yii::$app->user->id,
                'grandTotal' => $transactionTotalPrice
            ]);
            $transaction->save();
            foreach ($groupedData as $customerAccount => $customerData) {
                $customer = Customer::find()->where(['accountNumber' => $customerAccount])->one();
                foreach ($customerData as $package_id => $meals) {
                    $orderTotalPrice = 0;
                    $order = new Orders([
                        'customer_id' => $customer->id,
                        'package_id' => $package_id,
                        'transaction_id' => $transaction->id,
                        'grandTotal' => $orderTotalPrice,
                        'order_date' => date('Y-m-d', strtotime($serveDate)),
                    ]);
                    $order->save();
                    foreach ($meals as $meal_id => $meal_items) {
                        $mealTotalPrice = 0;
                        $orderMeal = new OrderMeals([
                            'order_id' => $order->id,
                            'meal_id' => $meal_id,
                            'grandTotal' => $mealTotalPrice,
                        ]);
                        $orderMeal->save();
                        foreach ($meal_items as $meal_item) {

                            $itemDetail = Product::find()->where(['id' => $meal_item])->one();
                            $mealItem = new OrderMealItems([
                                'order_meal_id' => $orderMeal->id,
                                'product_id' => $meal_item,
                                'price' => $itemDetail->price,
                                'status' => 2,
                            ]);
                            $mealItem->save();
                            $mealTotalPrice += $itemDetail->price;
                            $orderTotalPrice += $itemDetail->price;
                            $transactionTotalPrice += $itemDetail->price;
                        }
                        $orderMeal->grandTotal = $mealTotalPrice;
                        $orderMeal->save();
                    }

                    $order->grandTotal = $orderTotalPrice;
                    $order->save();

                }
            }
            $transaction->grandTotal = $transactionTotalPrice;
            $transaction->save();

            return [
                'status' => 1,
                'data' => [],
                'msg' => 'Transaction saved successfully',
            ];
        } else {
            return [
                'status' => 0,
                'data' => [],
                'msg' => 'Invalid data',
            ];
        }
    }

    public function actionClearDraft()
    {
        DraftTransactions::deleteAll(['user_id' => Yii::$app->user->id]);
        $this->redirect("/order/process-new-orders");
    }

    public function actionGenerateOrder()
    {


        /*$test = OrderManager::setTransactionStatus(4);
        die('Call');*/

        if(!Yii::$app->user->can('Admin') && empty(Yii::$app->user->getIdentity()->city_id)){
            GeneralHelper::showErrorMsg('Your city is not set for operations. Please contact admin');
            return $this->goBack();
        }

        $dynamicModel = new DynamicModel([
            'customer_id',
            'selectedMeals',
            'date',
            'city_id'
        ]);
        $dynamicModel->addRule([
            'customer_id',
            'selectedMeals',
            'date',
            'city_id'
        ], 'safe');
        $pkgDetailList = null;
        $orderData = null;
        $customersListFetched = [];
        $cityList = ArrayHelper::map(Cities::find()->Where(['active'=>'1'])->all(), "id", "name");

        if (Yii::$app->request->get() && $dynamicModel->load(Yii::$app->request->get())) {
            $selectedMeals = explode(',', $dynamicModel->selectedMeals);
            $filterDate = (!empty($dynamicModel->date)) ? date('Y-m-d', strtotime($dynamicModel->date)) : date('Y-m-d');

            /*
             * Already generated order packages
             * */

            $generatedOrderPackages = Orders::find()->where(['order_date' => $filterDate])->select(['package_id'])->asArray()->all();
            $generatedOrderPackagesList = [];
            foreach ($generatedOrderPackages as $generatedOrderPackage){
                $generatedOrderPackagesList[] = $generatedOrderPackage['package_id'];
            }

            $pkgOfTheDay = Package::find()->where([
                '<=', 'package_start_date', $filterDate
            ])
                ->andWhere(['package_status' => 1])
                ->andWhere([
                    '>', 'package_expiration', $filterDate
                ]);
            if(count($generatedOrderPackagesList)){
                $pkgOfTheDay->andWhere(['not in','package.id',$generatedOrderPackagesList]);
            }
            if (!empty($dynamicModel->customer_id)) {
                $pkgOfTheDay->andWhere([
                    'customer_id' => $dynamicModel->customer_id
                ]);
            }
            if (!empty($dynamicModel->city_id)) {

                $pkgOfTheDay->join('INNER JOIN', 'customer', 'customer.id = package.customer_id');
                $pkgOfTheDay = $pkgOfTheDay->andWhere(['customer.city_id' => $dynamicModel->city_id]);

            }


            if(!Yii::$app->user->can('Admin')){
                $pkgOfTheDay->join('INNER JOIN', 'customer', 'customer.id = package.customer_id AND customer.city_id = '.Yii::$app->user->getIdentity()->city_id);
            }

            $pkgOfTheDayData = $pkgOfTheDay
                ->with(['customer'])
                ->all();

            $pkgDetailList = [];
            $orderData = [];


            if (count($pkgOfTheDayData)) {
                foreach ($pkgOfTheDayData as $pkgOfTheDaySingle) {
                    $pkgDetailList[$pkgOfTheDaySingle['id']]['package_id'] = $pkgOfTheDaySingle['id'];
                    $pkgDetailList[$pkgOfTheDaySingle['id']]['packageTitle'] = $pkgOfTheDaySingle['package_title'];
                    $pkgDetailList[$pkgOfTheDaySingle['id']]['customerTitle'] = trim($pkgOfTheDaySingle['customer']->user->firstName . " " . $pkgOfTheDaySingle['customer']->user->lastName);
                    //$pkgDetails['meals_count'] = $pkgOfTheDay->number_of_meals;
                    $pkgDetailList[$pkgOfTheDaySingle['id']]['meals_list']['meals'] = [];
                    $pkgMeals = PackageProducts::find()->where([

                        'package_id' => $pkgOfTheDaySingle['id'],
                        'serve_date' => $filterDate
                    ])
                        ->andWhere(['in', 'is_selected', [1,2]])
                        ->andWhere(['in', 'meal_id', $selectedMeals])->with(['product', 'mealDetail'])->all();
                    //echo "<pre>";print_r($pkgMeals);echo "</pre>";die('Call');
                    foreach ($pkgMeals as $pkgMeal) {
                        $pkgDetailList[$pkgOfTheDaySingle['id']]['meals_list']['meals'][$pkgMeal->meal_id]['meal_type'] = $pkgMeal->meal_id;
                        $pkgDetailList[$pkgOfTheDaySingle['id']]['meals_list']['meals'][$pkgMeal->meal_id]['title'] = $pkgMeal->mealDetail->meal_title;
                        if (!isset($pkgDetailList[$pkgOfTheDaySingle['id']]['meals_list']['meals'][$pkgMeal->meal_id]['items'])) {
                            $pkgDetailList[$pkgOfTheDaySingle['id']]['meals_list']['meals'][$pkgMeal->meal_id]['items'] = [];
                        }

                        $pkgDetailList[$pkgOfTheDaySingle['id']]['meals_list']['meals'][$pkgMeal->meal_id]['items'][$pkgMeal->product->id] = [

                            'pid' => $pkgMeal->product->id,
                            'title' => $pkgMeal->product->product_title,
                            'price' => $pkgMeal->product->price,
                            'serve_date' => $pkgMeal->serve_date,
                        ];

                        $orderDataTemp = [
                            'customer_account' => $pkgOfTheDaySingle['customer']->accountNumber,
                            'package_id' => $pkgOfTheDaySingle['id'],
                            'meal_id' => $pkgMeal->meal_id,
                            'item_id' => $pkgMeal->product->id,
                            'serve_date' => $pkgMeal->serve_date,
                        ];
                        $customersListFetched[$pkgOfTheDaySingle['customer']->accountNumber]  = $pkgOfTheDaySingle['customer']->accountNumber;
                        $orderData[] = $orderDataTemp;

                    }
                }

            }
        }


        $orderDataEncoded = '';
        if ($orderData !== null) {
            $orderDataEncoded = base64_encode(json_encode($orderData));
        }


        $customers = Customer::find()->innerJoinWith(['user' => function($query){
            return $query->where(['users.active' => 1]);

        }]);
        if(!Yii::$app->user->can('Admin')){

            $customers->where(['customer.city_id' => Yii::$app->user->getIdentity()->city_id]);
        }
        $customers = $customers->all();
        $customersList = ArrayHelper::map($customers, 'id', function ($model) {
            return trim($model->user->firstName . " " . $model->user->lastName);
        });
        $meals = ArrayHelper::map(Meals::find()->all(), "id", "meal_title");




        return $this->render('generate-order', [
            'meals' => $meals,
            'customersList' => $customersList,
            'cityList'=>$cityList,
            'dynamicModel' => $dynamicModel,
            'pkgDetailList' => $pkgDetailList,
            'orderDataEncoded' => $orderDataEncoded,
            'customersListFetched' => $customersListFetched,
        ]);
    }

    public function actionVerifyOrderMealItem(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $orderMealItem = OrderMealItems::find()->where(['id' => $data['orderMealItemId']])->one();
        if($orderMealItem){
            $orderMealItem->status = 2;
            $orderMealItem->save();
            OrderManager::setTransactionStatus($orderMealItem->orderMeal->order->transaction_id);
            return [
                'status' => 1
            ];
        }else{
            return [
                'status' => 0
            ];
        }
    }

    public function actionHoldOrderMealItem(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $orderMealItem = OrderMealItems::find()->where(['id' => $data['orderMealItemId']])->one();
        if($orderMealItem){
            $orderMealItem->status = ($data['hold'] ==  1) ? 4 : 1;
            $orderMealItem->save();
            OrderManager::setTransactionStatus($orderMealItem->orderMeal->order->transaction_id);
            return [
                'status' => 1
            ];
        }else{
            return [
                'status' => 0
            ];
        }
    }

    public function actionUpdateOrderMealItemQty(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        $orderMealItem = OrderMealItems::find()->where(['id' => $data['orderMealItemId']])->one();
        if($orderMealItem){
            $orderMealItem->qty = $data['qty'];
            $orderMealItem->save();
            return [
                'status' => 1
            ];
        }else{
            return [
                'status' => 0
            ];
        }
    }

    public function actionCompleteOrderMealItems(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = Yii::$app->request->post();
        //echo "<pre>";print_r($data);echo "</pre>";die('Call');

        $orderMealItemsData = json_decode(base64_decode($data['orderMealItems']), 1);
        $order_item_ids = [];
        $transaction_ids = [];

        foreach ($orderMealItemsData as $orderMealItem){
            //echo "<pre>";print_r($orderMealItem);echo "</pre>";

            $orderMealItemId = $orderMealItem['item_id'];
            //if(!isset($orderMealItem['qty'])){ echo "<pre>";print_r($orderMealItem);echo "</pre>";die('Call'); }
            $orderMealItemQty = (double)$orderMealItem['qty'];
            $order_item_ids[] = $orderMealItemId;
            $transaction_ids[] = $orderMealItem['transaction_id'];
        }

        $transaction_ids = array_unique($transaction_ids);
        $order_item_ids_string = implode(',', $order_item_ids);
        $updated = OrderMealItems::updateAll([
            'status' => 3,
        ], 'id in ('.$order_item_ids_string.')');

        foreach ($transaction_ids as $transaction_id){
            OrderManager::setTransactionStatus($transaction_id);
        }

        return [
            'status' => 1,
            'msg' => 'Verified orders marked completed successfully'
        ];
    }

    public function actionImportOrdersQty(){
        $fileModel = new DynamicModel(["import_orders"]);
        $fileModel->addRule(["import_orders"], "file", ["maxSize" => 1024 * 1024, "skipOnEmpty" => false, "extensions" => "xlsx"]);
        $post = Yii::$app->request->post();
        if ($fileModel->load($post)) {
            $requiredHeadings = [
                'ID',
                'Qty'
            ];
            $inputFileType = 'Xlsx';
            $path = Yii::getAlias("@backend") . "/web/uploads/bulkuploads/orders/qty-update/".date("Y")."/".date("m")."/".date("d")."/";
            if(!FileHelper::createDirectory($path, $mode = 0777, $recursive = true))
            {
                GeneralHelper::showErrorMsg('Permission issue while creating a directory');
                return $this->redirect('/order/process-generated-orders');
            }
            $path = $path."user-" . Yii::$app->user->id . "-" . date('YmdHis');


            $sheetname = 'Orders';
            $fileUpload = $this->uploadFile($fileModel, $path, "import_orders");
            if(!$fileUpload){
                GeneralHelper::showErrorMsg('An error occurred while uploading file');
                return $this->redirect('/order/process-generated-orders');
            }

            try {
                $reader = IOFactory::createReader($inputFileType);
                $spreadsheet = $reader->load($path . ".xlsx");
                $worksheet = $spreadsheet->getSheetByName($sheetname);
                $headings = [];
                $data = [];
                $errors = [];
                $allAttributes = [];
                $templateAttributes = [];
                $dataSize = $worksheet->getHighestRowAndColumn();
                $highestColumnIndex = Coordinate::columnIndexFromString($dataSize['column']);
            } catch (\Exception $e) {
                GeneralHelper::showErrorMsg('Error loading file: ' . $e->getMessage());
                return $this->redirect('/order/process-generated-orders');
            }

            /*
             * Check headings
             * */

            for ($col = 1; $col <= $highestColumnIndex; ++$col) {
                $cellValue = mb_convert_encoding($worksheet->getCellByColumnAndRow($col, 1)->getValue(), "UTF-8");
                $headings[] = $cellValue;
            }
            if(count(array_diff($requiredHeadings, $headings))){
                GeneralHelper::showErrorMsg('Invalid file pattern');
                return $this->redirect('/orders/process-generated-orders');
            }

            $idKey = array_search('ID', $headings);
            $qtyKey = array_search('Qty', $headings);


            $dataSet = [];
            for ($row = 2; $row <= $dataSize['row']; ++$row) {

                $itemId = mb_convert_encoding($worksheet->getCellByColumnAndRow(($idKey+1), $row)->getValue(), "UTF-8");
                $qty = mb_convert_encoding($worksheet->getCellByColumnAndRow(($qtyKey+1), $row)->getValue(), "UTF-8");
                $dataSet[] = [
                    'item_id' => $itemId,
                    'qty' => $qty,
                ];
            }

            foreach ($dataSet as $data){
                $orderMealItem = OrderMealItems::find()->where(['id' => $data['item_id']])->one();
                if(isset($orderMealItem->id)){
                    $orderMealItem->qty = $data['qty'];
                    $orderMealItem->save();
                }
            }

            GeneralHelper::showSuccessMsg('File imported successfully.');
            return $this->redirect('/order/process-generated-orders');
        }else{
            GeneralHelper::showErrorMsg('Invalid request');
            return $this->redirect('/order/process-generated-orders');
        }
    }

    private function uploadFile($mediaModel, $path, $type){
        try {

            $mediaModel = UploadedFile::getInstance($mediaModel, $type);

            return $mediaModel->saveAs($path . '.' . $mediaModel->extension);
        } catch (\Exception $e) {
            return false;
        }
    }

}
