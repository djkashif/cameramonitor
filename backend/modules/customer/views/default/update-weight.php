<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Update Weight ';
?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <!-- form start -->
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($customer, 'weight')->textInput(['type' => 'number', 'maxlength' => true, 'placeholder' => 'Weight']) ?>
                    </div>

                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="col-md-12">
        <h3>Weight History</h3>
    </div>
    <?php
    if(!empty($customer->weightHistory)){
        ?>
        <div class="col-md-12">
            <ul class="timeline">
                <?php
                foreach($customer->weightHistory as $weightHistory){
                    ?>
                    <!-- timeline time label -->
                    <li class="time-label">
                  <span class="bg-red">
                    <?= date('d M. Y', strtotime($weightHistory->date)) ?>
                  </span>
                    </li>
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <li>
                        <i class="fa fa-balance-scale bg-blue"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> <?= (!empty($weightHistory->modifiedOn)) ? date('h:i a', strtotime($weightHistory->modifiedOn)) : date('h:i a', strtotime($weightHistory->createdOn)) ?></span>

                            <h3 class="timeline-header">Weight : <?= $weightHistory->weight ?></h3>


                        </div>
                    </li>
                    <!-- END timeline item -->
                    <?php
                }
                ?>




            </ul>
        </div>
        <?php

    }else{
        ?>
        <div class="col-md-12">
            <h5>No history found...</h5>
        </div>
        <?php
    }
    ?>


</div>
