<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t("app", "Assign Permissions");

?>
<div class="box">
    <?php ActiveForm::begin(); ?>
    <div class="box-body with-border">
        <div class="row">
            <?php
            $i = 1;

            foreach ($permissionsList as $permission => $permissionRow) {
                $checked = ArrayHelper::keyExists($permission, $rolePermissionsList);
                echo "<div class=\"col-md-2\"><label>" . Html::checkbox("permissions[{$permission}]", $checked) . " {$permission}</label></div>";

                if ($i === 6) {
                    echo "</div><div class=\"row\">";
                } else {
                    $i++;
                }
            }
            ?>
        </div>
    </div>
    <div class="box-footer">
        <?php echo Html::a(Yii::t("app", "Back"), Url::to(["index"]), ["class" => "btn btn-default"]); ?>
        <?php echo Html::submitButton(Yii::t("app", "Assign Permissions"), ["class" => "btn btn-primary pull-right"]); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>