<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Vehicle */

$this->title = "Product Details";

\yii\web\YiiAsset::register($this);
?>
<div class="vehicle-view">
    <p>
        <?php
        if (Yii::$app->user->can("updateProduct")) {
            echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        }
        ?>

        <?php
        if (Yii::$app->user->can("deleteProduct")) {
            echo Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product_title', 'product_description',
            'createdOn',
            'modifiedOn',
        ],
    ]) ?>

</div>
