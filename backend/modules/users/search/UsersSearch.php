<?php

namespace backend\modules\users\search;

use backend\modules\users\models\Users;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

class UsersSearch extends Users
{
    public function rules()
    {
        return [
            [["firstName", "lastName"], "string", "max" => 100],
            [["email"], "string", "max" => 255]
        ];
    }

    public function search($params)
    {
        $this->load($params);

        if (!$this->validate()) {
            return new ArrayDataProvider();
        }

        $query = self::find()
            ->join('LEFT JOIN', 'auth_assignment', 'auth_assignment.user_id = id')
            ->where(['<>', 'auth_assignment.item_name' , 'Customer'])
            ->andWhere(['deleted' => 0])
            ->andFilterWhere(["like", "firstName", $this->firstName])
            ->andFilterWhere(["like", "lastName", $this->lastName])
            ->andFilterWhere(["email" => $this->email]);

        $dataProvider = new ActiveDataProvider([
            "query" => $query,
            "sort" => ["defaultOrder" => ["id" => SORT_DESC]]
        ]);

        return $dataProvider;
    }
}