<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "package_meals".
 *
 * @property int $id
 * @property int $package_id
 * @property int $meal_id
 * @property string $createdOn
 * @property string $modifiedOn
 */
class PackageMeals extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package_meals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['package_id', 'meal_id'], 'required'],
            [['package_id', 'meal_id'], 'integer'],
            [['createdOn', 'modifiedOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'package_id' => 'Package ID',
            'meal_id' => 'Meal ID',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }

    public function getMealDetail()
    {
        return $this->hasOne(Meals::className(), ["id" => "meal_id"]);
    }
}
