<?php
namespace backend\auth;

use common\models\Timezones;
use Yii;
use yii\base\Model;

class UsersForm extends Model
{
    public $email;
    public $password;
    public $rememberMe;
    
    public $_user;
    
    public function rules()
    {
        return [
            [["email", "password"], "required"]
        ];
    }

    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            if (!empty($user)) {
                $validatePassword = UserIdentity::validatePassword($this->password, $user->password);
                if (!$validatePassword) {
                    $this->addError("email", "Invalid Credentials");
                    return false;
                }
                $authManager = Yii::$app->authManager;
                Yii::$app->session->set("roles", $authManager->getRolesByUser($user->id));
                return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
            }
        }
        
        $this->addError("email", "Invalid Credentials");
        return false;
    }

    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = UserIdentity::findByUsername($this->email);
        }

        return $this->_user;
    }
}