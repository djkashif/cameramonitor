<?php

namespace backend\controllers;

use common\models\Cities;
use common\models\Customer;
use common\models\Package;
use common\models\PackageProducts;
use kartik\mpdf\Pdf;
use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use Yii;
class BarcodeController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Admin"],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['printLabels']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['prepare-barcode-pdf'],
                        'roles' => ['@']
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication");
                }
            ]
        ];
    }
    public function actionIndex()
    {
        $dynamicModel = new DynamicModel([
            'packageDate',
            'customer_id',
            'city_id'
        ]);
        $dynamicModel->addRule([
            'packageDate'
        ], 'required');
        $dynamicModel->addRule([
            'customer_id'
        ], 'safe');
        $dynamicModel->addRule([
            'city_id'
        ], 'safe');
        $dynamicModel->packageDate = date('d-M-Y');

        $customers = Customer::find()->innerJoinWith(['user' => function($query){
            return $query->where(['active' => 1]);

        }])->all();
        //echo "<pre>";print_r($customers);echo "</pre>";die('Call');

        $customersList = ArrayHelper::map($customers, 'id', function($model){
            return trim($model->user->firstName." ".$model->user->lastName);
        });
        $cityList = ArrayHelper::map(Cities::find()->Where(['active'=>'1'])->all(), "id", "name");

        return $this->render('print-package-barcodes', [
            'dynamicModel' => $dynamicModel,
            'cityList' => $cityList,
            'customersList' => $customersList
        ]);
    }

    public function actionPrepareBarcodePdf($date, $meals, $customer = '',$city=''){
        //ini_set("pcre.backtrack_limit", "5000000");
        $mealsArr = explode(',', $meals);

        $pkgProductsOfDay = PackageProducts::find()
            ->where(['serve_date' => date('Y-m-d', strtotime($date))])
            ->andWhere(['in', 'is_selected', [1,2]])
            ->andWhere(['in', 'meal_id', $mealsArr])
            //->andWhere(['package.customer_id' => $customer  ])
            ->innerJoinWith(['package' => function($query) use ($customer, $date){
                $query = $query->where('1=1');
                if($customer != ''){
                    $customers = explode(',', $customer);
                    $query = $query->where(['in', 'customer_id', $customers]);
                }

                //echo "<pre>";print_r(date('Y-m-d', strtotime($date)));echo "</pre>";die('Call');
                return $query->andWhere(['>', 'package_expiration', date('Y-m-d', strtotime($date))]);

            }, 'product', 'mealDetail']);
            //->asArray()
        $pkgProductsOfDay = $pkgProductsOfDay->andWhere(['package_status' => 1]);
        if($city!='' && !empty($city)){

            $pkgProductsOfDay->join('INNER JOIN', 'customer', 'customer.id = package.customer_id');
            $pkgProductsOfDay = $pkgProductsOfDay->andWhere(['customer.city_id' => $city]);

        }

        //echo "<pre>";print_r($pkgProductsOfDay->asArray()->all());echo "</pre>";die('Call');
        if(!Yii::$app->user->can('Admin')){
            $pkgProductsOfDay->join('INNER JOIN', 'customer', 'customer.id = package.customer_id AND customer.city_id = '.Yii::$app->user->getIdentity()->city_id);
        }

        $pkgProductsOfDay = $pkgProductsOfDay->all();
        //echo "<pre>";print_r($pkgProductsOfDay);echo "</pre>";die('Call');


        $groupedData = [];
        foreach ($pkgProductsOfDay as $pkgProductsOfDay){
            //echo "<pre>";print_r($pkgProductsOfDay);echo "</pre>";die('Call');
            $pkgValidMeals = array_values(ArrayHelper::map($pkgProductsOfDay->package->meals, 'id', 'meal_id'));
            //echo "<pre>";print_r($pkgProductsOfDay->meal_id);echo "</pre>";
            //echo "<pre>";print_r($pkgValidMeals);echo "</pre>";
            //continue;
            if(!in_array($pkgProductsOfDay->meal_id, $pkgValidMeals)){
                continue;
            }

            if(!isset($groupedData[$pkgProductsOfDay->package->id])){
                $groupedData[$pkgProductsOfDay->package->id] = [
                    'package' => [
                        'package_title' => $pkgProductsOfDay->package->package_title,
                        'barcode' => $pkgProductsOfDay->package->barcode,
                        'date' => date('Y-m-d', strtotime($date)),

                    ],
                    'customer' => [
                        'title' => trim($pkgProductsOfDay->package->customer->user->firstName . " ".$pkgProductsOfDay->package->customer->user->lastName),
                        'accountNumber' => $pkgProductsOfDay->package->customer->accountNumber,
                        'contactNumber' => $pkgProductsOfDay->package->customer->contactNumber,
                        'address' => $pkgProductsOfDay->package->customer->address,
                        'barcode' => $pkgProductsOfDay->package->customer->barcode,
                        'city' => (isset($pkgProductsOfDay->package->customer->city->name)) ? $pkgProductsOfDay->package->customer->city->name : 'N/A'
                    ],
                    'products' => [

                    ]
                ];
            }
            $meal_type_code = '';
            switch ($pkgProductsOfDay->mealDetail->id){
                case 1:
                    $meal_type_code = 'B';
                    break;
                case 2:
                    $meal_type_code = 'L';
                    break;
                case 3:
                    $meal_type_code = 'D';
                    break;

            }
            $groupedData[$pkgProductsOfDay->package->id]['products'][$pkgProductsOfDay->mealDetail->meal_title][] = [
                'product_title' => $pkgProductsOfDay->product->product_title,
                'calories' => $pkgProductsOfDay->product->calories,
                'fat' => $pkgProductsOfDay->product->fat,
                'protiens' => $pkgProductsOfDay->product->protiens,
                'carbohydrates' => $pkgProductsOfDay->product->carbohydrates,
                'barcode' => $pkgProductsOfDay->product->barcode,
                'meal_type' => $pkgProductsOfDay->mealDetail->meal_title,
                'meal_type_code' => $meal_type_code,
            ];
        }

        //echo "<pre>";print_r($groupedData);echo "</pre>";die('Call');
        //die('Call');

        $content = $this->renderPartial('_prepare-barcode', [
            'data' => $groupedData
        ]);
        //echo $content;die;
        try{
            $pdf = new Pdf([

                'mode' => Pdf::MODE_CORE,

                'format' => [101.6, 50.8],
                'marginLeft' => 1,
                'marginRight' => 1,
                'marginTop' => 1,
                'marginBottom' => 1,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_BROWSER,
                'content' => $content,
                'cssFile' => '@backend/web/css/bootstrap.min.css',
                'cssInline' => '.bar-container{
                                border: 2px solid;
                                border-radius: 12px;
                                height: 184px;
                                width: 396px;
                                overflow: hidden;
                            }
                            .heading-wrapper{
                                border-bottom: 4px solid;
                                padding: 0 0 0 8px;
                            }
                            .heading-wrapper2{
                                border-bottom: 1px solid;
                                padding: 0 0 0 8px;
                            }
                            .heading{
                                font-size: 18px;
                            }
                            .col-div{
                                padding: 4px;
                            }
                            .nut-heading{
                                font-size: 24px;
                                font-weight: bold;
                            }
                            .nut-value{
                                font-size: 24px;
                            }
                            .col-div p{
                                margin: 0;
                                padding: 0px 0;
                            }
                            .barcode-image img{
                                width: 100%;
                                height: 70px;
                            }
                            .content-div{
                                width: 30%;
                                float: left;
                            }
                            .blank-div{
                                width: 10%;
                                float: left;
                            }
                            .barcode-div{
                                width: 60%;
                                float: left;
                            }
                            .content-body-wrapper{
                                border-bottom: 2px solid;
                            }
                            .content-footer-wrapper{
                                text-align: center;
                            }
                            .content-footer-wrapper p{
                                margin: 0;
                                font-size: 13px;
                                font-weight: bold;
                            }',
                'options' => ['title' => 'Product Barcodes'],
                /*'methods' => [
                    'SetHeader'=>['Krajee Report Header'],
                    'SetFooter'=>['{PAGENO}'],
                ]*/
            ]);
            // return the pdf output as per the destination setting
            return $pdf->render();
        }catch (\Exception $e){
            return 'Content too large.. Please select meal types one by one';
        }
        // setup kartik\mpdf\Pdf component

    }



}
