<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VehicleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Support Tickets';
$this->params['breadcrumbs'][] = $this->title;
$this->params["headerButtons"][] = Html::a(Yii::t("app", "Advance Search"), "javascript:;", ["class" => "btn btn-primary", "data" => ["toggle" => "modal", "target" => "#searchModal"]]);
echo $this->render("_search", [
    "searchModel" => $searchModel
]);
?>
<div class="vehicle-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'ticket_title',
            'ticket_dept',
            [
                "label" => Yii::t("app", "Created By"),
                "format" => "raw",
                "value" => function ($data) {
                    return trim($data->createdBy->user->firstName . " " . $data->createdBy->user->lastName);
                }
            ],
            [
                "label" => Yii::t("app", "Status"),
                "format" => "raw",
                "value" => function ($data) {
                    return \backend\components\TicketsManager::createStatusLabel($data->ticket_status);
                }
            ],
            [
                "label" => Yii::t("app", "Action By"),
                "format" => "raw",
                "value" => function ($data) {
                    if(!empty($data->action_by)){
                        return trim($data->actionBy->firstName . " " . $data->actionBy->lastName);
                    }
                    return 'N/A';

                }
            ],

            'createdOn',
            [
                "class" => "yii\\grid\\ActionColumn",
                "headerOptions" => ["class" => "text-center", "style" => "width: 8%"],
                "contentOptions" => ["class" => "text-center"],


                "template" => "{view}{delete}",
                "buttons" => [
                    "view" => function ($url, $model) {
                        if (Yii::$app->user->can("viewCustomerSupportTicketDetails")) {
                            return Html::a("<span class=\"fa fa-eye\"></span>", $url."&dept=".$model->ticket_dept, ["title" => "View Ticket"]);
                        }
                    },
                    "delete" => function ($url, $model) {
                        if (Yii::$app->user->can("deleteCustomerSupportTicket")) {
                            return Html::a("<span class=\"glyphicon glyphicon-trash\"></span>", "javascript:;", ["title" => "Delete Product", "class" => "btn-confirm", "data" => ["href" => $url, "body" => Yii::t("app", "Are you sure you want to delete user #{$model->id}?") . "<br /><br /><strong>" . Yii::t("app", "THIS ACTION IS IRREVERSIBLE") . "</strong>"]]);
                        }
                    }
                ],
                "urlCreator" => function ($action, $model, $key, $index) {
                    if ($action === "view") {
                        return Url::to(["view-ticket", "id" => $model->id]);
                    }
                    if ($action === "delete") {
                        return Url::to(["delete", "id" => $model->id]);
                    }
                }
            ]
        ],
    ]); ?>


</div>
<?php
if (Yii::$app->user->can("deleteCustomerSupportTicket")) {
    $this->registerJs("
        $(\".btn-confirm\").click(function () {
            var btn = $(this);

            $.showModal({
                title: \"Confirmation\",
                body: btn.attr(\"data-body\"),
                buttons: \"<a href=\\\"\" + btn.attr(\"data-href\") + \"\\\" class=\\\"btn btn-primary btn-confirm\\\" data-method=\\\"post\\\">CONFIRM</button>\"
            });
        });
    ");
}

?>