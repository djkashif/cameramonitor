<?php
namespace backend\controllers;

use backend\auth\UserIdentity;
use backend\auth\UsersForm;

use backend\components\GeneralHelper;
use common\models\Users;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class AuthenticationController extends Controller
{
    public function actionIndex()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->redirectToHome();
        }
        $this->layout = "@app/views/layouts/login";

        $model = new UsersForm();
        if ($model->load(Yii::$app->request->post())) {
            if($model->login()){
                try {
                    if(!Yii::$app->user->can('Admin') && !Yii::$app->user->can('Customer') && empty(Yii::$app->user->getIdentity()->city_id)){
                        Yii::$app->user->logout();
                        GeneralHelper::showErrorMsg('Your city is not mapped for operations. Please contact admin');
                        return $this->goBack();
                    }
                    return $this->redirectToHome();
                } catch (Exception $e) {
                    GeneralHelper::showErrorMsg('Invalid login details. Please try again');
                    return $this->goBack();

                }
            }else{
                GeneralHelper::showErrorMsg('Invalid login details');
                return $this->goBack();
            }

        }

        return $this->render("index", [
            "model" => $model,
        ]);
    }

    private function redirectToHome(){

        if(Yii::$app->user->can('Customer')){
            return $this->redirect(Url::to(["/customer"]));
        }


        return $this->redirect(Url::to(["/dashboard"]));


    }

    public function actionSetAdmin(){
        $model = new Users([
            'firstName' => 'Admin',
            'lastName' => 'User',
            'email' => 'admin@admin.com',
            "password" => Yii::$app->security->generatePasswordHash("admin0101")
        ]);
        if (!$model->save()) {
            echo "<pre>";print_r($model->getErrors());echo "</pre>";die('Call');
        }
    }

    public function actionLogout()
    {
        if (Yii::$app->user->getId()) {
            $model = UserIdentity::findOne(Yii::$app->user->getId());
        }

        Yii::$app->user->logout();
        return $this->goHome();
    }
}