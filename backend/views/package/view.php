<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Vehicle */

$this->title = "Package Details";

\yii\web\YiiAsset::register($this);
?>
<div class="vehicle-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'package_title',
            'package_start_date',
            'package_expiration',
            'createdOn',
            'modifiedOn',
        ],
    ]) ?>
    <h3>Products</h3>
    <table class="table table-responsive table-striped">
        <tr>
            <th>Sr #</th>
            <th>Product Title</th>
        </tr>
        <?php
        $counter = 1;
        foreach ($model->products as $product){
            ?>
            <tr>
                <td><?= $counter ?></td>
                <td><?= $product->product->product_title ?></td>
            </tr>
            <?php
            $counter++;

        }
        ?>
    </table>



</div>
