<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php $form = ActiveForm::begin([
    "id" => "search-form",
    "action" => ["index"],
    "method" => "get"
]);
?>
    <div class="modal fade" id="searchModal" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><?php echo Yii::t("app", "Advanced Search"); ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, "package_title")->textInput(); ?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $form->field($searchModel, "customer")->textInput(); ?>
                        </div>
                        <div class="col-md-4">
                            <?=
                            $form->field($searchModel, 'customer_city_id')->widget(\kartik\depdrop\DepDrop::classname(), [
                                'type' => \kartik\depdrop\DepDrop::TYPE_DEFAULT,
                                'data' => $cityList,
                                'options' => ['id' => 'city-id', 'placeholder' => 'Select ...'],
                                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                'pluginOptions' => [
                                    'depends' => ['state-id'],
                                    'url' => \yii\helpers\Url::to(['/customers/state-cities']),
                                    'params' => ['city_id_pre']

                                ]
                            ])->label('City');

                            ?>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <?php echo Html::a(Yii::t("app", "Close"), "javascript:;", ["class" => "btn btn-default", "data" => ["dismiss" => "modal"]]); ?>
                    <?php echo Html::a(Yii::t("app", "Reset"), Url::to(["/package"]), ["class" => "btn btn-primary"]); ?>
                    <?php echo Html::submitButton(Yii::t("app", "Search"), ["class" => "btn btn-success"]); ?>
                </div>
            </div>
        </div>
    </div>
<?php Activeform::end(); ?>