<?php

use yii\helpers\Html;


$this->title = 'Create Template';

?>
<div class="customer-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
