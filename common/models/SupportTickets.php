<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "support_tickets".
 *
 * @property int $id
 * @property string $ticket_title
 * @property string $ticket_description
 * @property string $ticket_response
 * @property int $ticket_status 1 = Open, 2 = Hold, 3 = Closed
 * @property int $created_by
 * @property int $action_by
 * @property string $action_time
 * @property string $createdOn
 * @property string $modifiedOn
 */
class SupportTickets extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'support_tickets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ticket_title', 'ticket_dept', 'ticket_description', 'created_by'], 'required'],
            [['ticket_description', 'ticket_response', 'ticket_dept'], 'string'],
            [['ticket_status', 'created_by', 'action_by'], 'integer'],
            [['action_time', 'createdOn', 'modifiedOn'], 'safe'],
            [['ticket_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_title' => 'Ticket Title',
            'ticket_description' => 'Ticket Description',
            'ticket_response' => 'Ticket Response',
            'ticket_status' => 'Ticket Status',
            'created_by' => 'Created By',
            'action_by' => 'Action By',
            'ticket_dept' => 'Department',
            'action_time' => 'Action Time',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }

    public function getCreatedBy(){
        return $this->hasOne(Customer::className(), ['id' => 'created_by']);
    }

    public function getActionBy(){
        return $this->hasOne(Users::className(), ['id' => 'action_by']);
    }
}
