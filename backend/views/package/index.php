<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VehicleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Packages';
$this->params['breadcrumbs'][] = $this->title;
$this->params["headerButtons"][] = Html::a(Yii::t("app", "Advance Search"), "javascript:;", ["class" => "btn btn-primary", "data" => ["toggle" => "modal", "target" => "#searchModal"]]);
if (Yii::$app->user->can("createPackage")) {
    $this->params["headerButtons"][] = Html::a(Yii::t("app", "Create"), Url::to(["create"]), ["class" => "btn btn-success"]);
}
if (Yii::$app->user->can("exportPackage")) {
    $this->params["headerButtons"][] = Html::a(Yii::t("app", "Export Active Packages"), Url::to(["export-all-packages"]), ["class" => "btn btn-warning"]);
}
if (Yii::$app->user->can("deletePackage")) {
    $this->registerJs("    
        $(\".btn-confirm\").click(function () {
            var btn = $(this);

            $.showModal({
                title: \"Confirmation\",
                body: btn.attr(\"data-body\"),
                buttons: \"<a href=\\\"\" + btn.attr(\"data-href\") + \"\\\" class=\\\"btn btn-primary btn-confirm\\\" data-method=\\\"post\\\">CONFIRM</button>\"
            });
        });
    ");
}

echo $this->render("_search", [
    "searchModel" => $searchModel,
    'cityList' => $cityList
]);
?>
<div class="vehicle-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'package_title',


            [
                "label" => Yii::t("app", "Customer"),
                "format" => "raw",
                "value" => function ($data) {
                        return (!empty($data->customer->user->firstName)) ? trim($data->customer->user->firstName." ".$data->customer->user->lastName) : '';
                }
            ],
            [
                "label" => Yii::t("app", "City"),
                "format" => "raw",
                "value" => function ($data) {
                    return (!empty($data->customer->city->name)) ? trim($data->customer->city->name) : '';
                }
            ],
            [
                "attribute" => 'package_start_date',
                "format" => "raw",
                "value" => function ($data) {
                        return date('d-M-Y', strtotime($data->package_start_date));
                }
            ],
            [
                "attribute" => 'package_expiration',
                "format" => "raw",
                "value" => function ($data) {
                        return date('d-M-Y', strtotime($data->package_expiration));
                }
            ],
            [
                "label" => Yii::t("app", "CST CODE"),
                "format" => "raw",
                "value" => function ($data) {
                        return 'CST'.$data->customer->id;
                }
            ],
            [
                "label" => Yii::t("app", "PKG CODE"),
                "format" => "raw",
                "value" => function ($data) {
                        return 'PKG'.$data->id;
                }
            ],
            /*[
                "label" => Yii::t("app", "Barcode"),
                "format" => "raw",

                "value" => function ($model) {
                    return HTML::img('/barcodes/'.$model->barcode, ["width" => 200, "height" => 25]);
                }
            ],*/

            [
                "attribute" => 'package_status',
                "format" => "raw",
                "value" => function ($data) {
                    $class = ($data->package_status == 1) ? 'success' : (($data->package_status == 2) ? 'warning' :  'danger');
                    $label = ($data->package_status == 1) ? 'Activated' : (($data->package_status == 2) ? 'On Hold' :  'Deactivated');
                    return "<label class='label label-{$class}'>{$label}</label>";
                }
            ],
            'createdOn',
            //['class' => 'yii\grid\ActionColumn'],
            [
                "class" => "yii\\grid\\ActionColumn",
                "headerOptions" => ["class" => "text-center", "style" => "width: 8%"],
                "contentOptions" => ["class" => "text-center"],


                "template" => "{update}{delete}",
                "buttons" => [
                    "update" => function ($url, $model) {
                        if (Yii::$app->user->can("updatePackage")) {
                            return Html::a("<span class=\"glyphicon glyphicon-pencil\"></span>", $url, ["title" => "Update Package"]);
                        }
                    },
                    "delete" => function ($url, $model) {

                        if (Yii::$app->user->can("deletePackage")) {
                            return Html::a("<span class=\"glyphicon glyphicon-trash\"></span>", "javascript:;", ["title" => "Delete Package", "class" => "btn-confirm", "data" => ["href" => $url, "body" => Yii::t("app", "Are you sure you want to delete package #{$model->id}?") . "<br /><br /><strong>" . Yii::t("app", "THIS ACTION IS IRREVERSIBLE") . "</strong>"]]);
                        }
                    }
                ],
                "urlCreator" => function ($action, $model, $key, $index) {
                    if ($action === "update") {
                        return Url::to(["update", "id" => $model->id]);
                    }
                    if ($action === "delete") {
                        return Url::to(["delete", "id" => $model->id]);
                    }
                }
            ]
        ],
    ]); ?>


</div>
