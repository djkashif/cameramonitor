<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $package_id
 * @property int $transaction_id
 * @property double $grandTotal
 * @property string $order_date
 * @property int $status
 * @property string $createdOn
 * @property string $modifiedOn
 */
class Orders extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'package_id', 'transaction_id', 'grandTotal'], 'required'],
            [['customer_id', 'package_id', 'transaction_id', 'status'], 'integer'],
            [['grandTotal'], 'number'],
            [['order_date', 'createdOn', 'modifiedOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'package_id' => 'Package ID',
            'transaction_id' => 'Transaction ID',
            'grandTotal' => 'Grand Total',
            'order_date' => 'Order Date',
            'status' => 'Status',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }

    public function getCustomer(){
        return $this->hasOne(Customer::className(), ["id" => "customer_id"]);
    }

    public function getPackage(){
        return $this->hasOne(Package::className(), ["id" => "package_id"]);
    }

    public function getTransaction(){
        return $this->hasOne(Transactions::className(), ["id" => "transaction_id"]);
    }

    public function getOrderMeals(){
        return $this->hasMany(OrderMeals::className(), ["order_id" => "id"]);
    }
}
