<?php
use yii\helpers\Html;
use yii\helpers\Url;
use \kartik\grid\GridView;
$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
/*$this->params["headerButtons"][] = Html::a(Yii::t("app", "Advance Search"), "javascript:;", ["class" => "btn btn-primary", "data" => ["toggle" => "modal", "target" => "#searchModal"]]);
$this->params["headerButtons"][] = Html::a(Yii::t("app", "Create"), Url::to(["process-new-orders"]), ["class" => "btn btn-primary"]);
echo $this->render("_search", [
    "searchModel" => $searchModel
]);*/
$gridColumns        = [
    [
        'class'          => 'kartik\grid\SerialColumn',
        'contentOptions' => ['class' => 'kartik-sheet-style'],
        //'width'          => '36px',
        'header'         => '',
        'headerOptions'  => ['class' => 'kartik-sheet-style'],



    ],
    [
        'attribute' => 'transaction',
        //"headerOptions"  => ["style" => "width: 5%"],
        "contentOptions" => ["class" => "text-center"],
        'vAlign' => 'middle',
        //'width' => '3%',
        "label" => Yii::t("app", "Transaction #"),
        "format" => "raw",
        "value" => function ($model) {
            return trim($model->transaction_id);
        }
    ],
    [
        //"headerOptions"  => ["style" => "width: 5%"],
        "contentOptions" => ["class" => "text-center"],
        'vAlign' => 'middle',
        //'width' => '3%',
        "label" => Yii::t("app", "Order #"),
        "format" => "raw",
        'attribute' => 'id'
    ],
    [
        'attribute' => 'account',
        //"headerOptions"  => ["style" => "width: 5%"],
        "contentOptions" => ["class" => "text-center"],
        'vAlign' => 'middle',
        //'width' => '3%',
        "format" => "raw",
        "label" => Yii::t("app", "Account #"),
        "value" => function ($model) {
            return trim($model->customer->accountNumber);
        }
    ],
    [
        //"headerOptions"  => ["style" => "width: 5%"],
        'attribute' => 'customer',
        "contentOptions" => ["class" => "text-center"],
        'vAlign' => 'middle',
        'width' => '3%',
        "label" => Yii::t("app", "Customer"),
        "format" => "raw",

        "value" => function ($model) {
            return trim($model->customer->user->firstName . " " . $model->customer->user->lastName);
        }
    ],
    [
        //"headerOptions"  => ["style" => "width: 5%"],
        "contentOptions" => ["class" => "text-center"],
        'vAlign' => 'middle',
        'width' => '5%',
        "label" => Yii::t("app", "Package"),
        "format" => "raw",

        "value" => function ($model) {
            return trim($model->package->package_title);
        }
    ],
    [
        //"headerOptions"  => ["style" => "width: 5%"],
        "contentOptions" => ["class" => "text-center"],
        'vAlign' => 'middle',
        'width' => '5%',
        "label" => Yii::t("app", "Item Details"),
        "format" => "raw",

        "value" => function ($model) {
            $html = "";
            if($model->orderMeals){
                foreach ($model->orderMeals as $orderMeal){
                    foreach ($orderMeal->orderMealItems as $orderMealItem){
                        $html .= '<li><span class="label label-info">'.$orderMeal->meal->meal_title.'</span>'.$orderMealItem->product->product_title.'</li>';
                    }
                }
            }
            return "<ul>{$html}</ul>";
        }
    ],
    [
        //"headerOptions"  => ["style" => "width: 5%"],
        "contentOptions" => ["class" => "text-center"],
        'vAlign' => 'middle',
        //'width' => '3%',

        "format" => "raw",
        'attribute' => 'grandTotal'
    ],
    [
        //"headerOptions"  => ["style" => "width: 5%"],
        "contentOptions" => ["class" => "text-center"],
        'vAlign' => 'middle',
        'width' => '5%',
        "label" => Yii::t("app", "Status"),
        "format" => "raw",

        "value" => function ($model) {
            //echo "<pre>";print_r($model);echo "</pre>";die('Call');
            /*$labelClass = ($model->status == 3) ? 'label label-success' : (($model->status == 6 || $model->status == 7) ? 'label label-info' : 'label label-warning' );
            $labelText = (isset(\backend\components\OrderManager::$statusLables[$model->status])) ?   \backend\components\OrderManager::$statusLables[$model->status] :'N/A' ;*/
            return \backend\components\OrderManager::createStatusLabel($model->status);
            //return "<label class='{$labelClass}'>{$labelText}</label>";
        }
    ],
    [
        //"headerOptions"  => ["style" => "width: 5%"],
        "contentOptions" => ["class" => "text-center"],
        'vAlign' => 'middle',
        //'width' => '3%',

        "format" => "raw",
        'attribute' => 'createdOn'
    ],
    [
        //"headerOptions"  => ["style" => "width: 5%"],
        "contentOptions" => ["class" => "text-center"],
        'vAlign' => 'middle',
        'width' => '5%',
        "label" => Yii::t("app", "Actions"),
        "format" => "raw",
        "value" => function ($model) {
            return (!in_array($model->status, [3, 5]) ) ? '<a href="/order/process-generated-orders?id='.$model->id.'">Process</a>' : '';
        }
    ],
    /*[
        //"headerOptions"  => ["style" => "width: 5%"],
        "contentOptions" => ["class" => "text-center"],
        'vAlign' => 'middle',
        //'width' => '3%',

        "format" => "raw",
        'attribute' => 'modifiedOn'
    ],*/

];

echo GridView::widget([
    'id' => 'kv-grid-orders',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns, // check the configuration for grid columns by clicking button above
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => true, // pjax is set to always true
    'toolbar' =>  [
        ['content' =>
            ((Yii::$app->user->can("orderGenerate")) ?  Html::a('<i class="glyphicon glyphicon-plus"></i>', ['generate-order'], ['data-pjax' => 0, 'class' => 'btn btn-success', 'title' => Yii::t('app', 'Generate New Order')])  : '') . ' '.
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
        ],
        '{export}',
        '{toggleData}',
    ],
    // set export properties
    'export' => [
        'fontAwesome' => true
    ],
    // parameters from the demo form
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    'showPageSummary' => false,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '',
    ],
    'persistResize' => false,
    'toggleDataOptions' => ['minCount' => 10],
    'exportConfig' => [
        \kartik\grid\GridView::EXCEL => [
            'label' => 'Export to Excel',
            'filename' => Yii::t('app', 'Orders'),
        ],

    ],
    'itemLabelSingle' => 'Order',
    'itemLabelPlural' => 'Orders'
]);

$this->registerJs("
    $('a.sidebar-toggle').click();
   ");

?>

