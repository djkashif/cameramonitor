
<?php foreach ($data as $pkgData): ?>
    <div class="container bar-container">
        <div class="row heading-wrapper"  style="text-align: center">
            <span class="heading"><?= $pkgData['package']['package_title'] ?></span>
        </div>
        <div class="row heading-wrapper2">
            <span class="heading"><?= $pkgData['customer']['title'] ?></span>
        </div>
        <div class="row">
            <div class="">
                <table style="width: 100%;">
                    <tbody>
                    <tr style="">
                        <td colspan="2" style="width: 100%;height: 73px">
                            <table style="width: 100%">
                                <?php
                                $rowTotal = ceil(count($pkgData['products'])/3);
                                /*$rowRem = count($pkgData['products'])%3;
                                $rowTotal = ($rowRem > 0) ? $rowTotal+1 : $rowTotal;*/
                                $indexCounter = 0;
                                $productTitleStyle = "font-size: 0.65em;padding: 0px 0 0 5px;";
                                for($i=1;$i<=5; $i++){
                                    ?>
                                    <tr>
                                        <td style="<?= $productTitleStyle ?>">
                                            <span><?= (isset($pkgData['products'][$indexCounter])) ? ''.'('.$pkgData['products'][$indexCounter]['meal_type_code'].') '.$pkgData['products'][$indexCounter]['product_title'] : ''?></span>
                                        </td>
                                        <td style="<?= $productTitleStyle ?>">
                                            <span><?= (isset($pkgData['products'][$indexCounter+1])) ? ''.'('.$pkgData['products'][$indexCounter+1]['meal_type_code'].') '.$pkgData['products'][$indexCounter+1]['product_title'] : ''?></span>

                                        </td>
                                        <td style="<?= $productTitleStyle ?>">
                                            <span><?= (isset($pkgData['products'][$indexCounter+2])) ? ''.'('.$pkgData['products'][$indexCounter+2]['meal_type_code'].') '.$pkgData['products'][$indexCounter+2]['product_title'] : ''?></span>
                                        </td>
                                    </tr>
                                    <?php
                                    $indexCounter = $indexCounter+3;
                                }
                                ?>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center;border-top: 2px solid;">
                            <p style="margin: 0;font-size: 0.6em;font-weight: bold;">
                                Store in a clean, dry, cool and odorless place.
                                Use within three days (if stored properly)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center;border-top: 2px solid;">
                            <p style="margin: 0;font-size: 14px;font-weight: bold;">
                                <img src="/barcodes/<?= $pkgData['package']['barcode'] ?>" alt="" style="vertical-align: middle;width: 80%;padding: 5px 10px 0px 10px;height: 30px;">
                            </p>
                        </td>
                    </tr>

                    </tbody>
                </table>

            </div>

        </div>
    </div>
    <div style="page-break-after: auto;"></div>
    <?php endforeach; ?>




