<?php
namespace backend\modules\users\controllers;

use backend\modules\rbac\models\AuthItem;
use backend\modules\users\models\Users;
use backend\modules\users\search\UsersSearch;
use common\models\Cities;
use Exception;
use Yii;
use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;

class ManageController extends Controller
{
    public function behaviors()
    {
        return [
            "access" => [
                "class" => AccessControl::className(),
                "rules" => [
                    [
                        "allow" => true,
                        "roles" => ["Admin", "Manager"],
                    ]
                ],
                "denyCallback" => function ($rule, $action) {
                    $this->redirect("/authentication/logout");
                }
            ],
            "verbs" => [
                "class" => VerbFilter::className(),
                "actions" => [
                    "changestatus" => ["POST"],
                    "delete" => ["POST"]
                ]
            ]
        ];
    }
    
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 20;

        return $this->render("index", [
            "searchModel" => $searchModel,
            "dataProvider" => $dataProvider
        ]);
    }
    
    public function actionCreate()
    {
        $model = new Users(["password" => Yii::$app->security->generatePasswordHash("abc1234")]);
        $rolesModel = new DynamicModel(["role"]);
        $rolesModel->addRule("role", "required");
        $rolesList = ArrayHelper::map(AuthItem::findAll(["type" => 1]), "name", "name");
        if(isset($rolesList['Customer'])){
            unset($rolesList['Customer']);
        }
        
        if ($model->load(Yii::$app->request->post()) && $rolesModel->load(Yii::$app->request->post())) {
            if ($model->validate() && $rolesModel->validate()) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    if (!$model->save()) {
                        throw new Exception(Yii::t("app", "An error occurred while saving user data"));
                    }
                    
                    $authManager = Yii::$app->authManager;
                    $role = $authManager->getRole($rolesModel->role);
                    $authManager->assign($role, $model->id);

                    $transaction->commit();
                    Yii::$app->session->setFlash("success", Yii::t("app", "Data saved successfully"));
                    return $this->redirect(Url::to(["index"]));
                } catch (Exception $e) {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash("error", Yii::t("app", "An error occurred while inserting data"));
                }
            }
        }
        $cityList = ArrayHelper::map(Cities::find()->all(), "id", "name");
        return $this->render("form", [
            "model" => $model,
            "rolesModel" => $rolesModel,
            "rolesList" => $rolesList,
            "cityList" => $cityList,
            "isUpdate" => false
        ]);
    }
    
    public function actionUpdate($id)
    {
        $model = $this->__findModel($id);
        
        if ($model === null) {
            Yii::$app->session->setFlash("error", Yii::t("app", "The requested url could not be found"));
            return $this->redirect(Url::to(["index"]));
        }
        
        $authManager = Yii::$app->authManager;
        $userRoles = $authManager->getRolesByUser($id);
        $rolesList = ArrayHelper::map(AuthItem::findAll(["type" => 1]), "name", "name");
        if(isset($rolesList['Customer'])){
            unset($rolesList['Customer']);
        }

        $rolesModel = new DynamicModel(["role" => key($userRoles)]);
        $rolesModel->addRule("role", "required");
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $rolesModel->validate()) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    if (!$model->save()) {
                        throw new Exception(Yii::t("app", "An error occurred while saving user data"));
                    }

                    $transaction->commit();

                    Yii::$app->session->setFlash("success", Yii::t("app", "Data saved successfully"));
                    return $this->redirect(Url::to(["index"]));
                } catch (Exception $e) {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash("error", Yii::t("app", "An error occurred while inserting data"));
                }
            }
        }
        $cityList = ArrayHelper::map(Cities::find()->all(), "id", "name");
        
        return $this->render("form", [
            "model" => $model,
            "rolesModel" => $rolesModel,
            "rolesList" => $rolesList,
            "cityList" => $cityList,
            "isUpdate" => true
        ]);
    }
    
    public function actionChangestatus($id)
    {
        $model = $this->__findModel($id);
        
        if ($model === null) {
            Yii::$app->session->setFlash("error", Yii::t("app", "The requested url could not be found"));
            return $this->redirect(Url::to(["index"]));
        }

        if ($model->active === 1) {
            $model->active = 0;
            $message = "User #{$id} deactivated successfully.";
        } else {
            $model->active = 1;
            $message = "User #{$id} activated successfully.";
        }
        
        if ($model->save()) {
            Yii::$app->session->setFlash("success", Yii::t("app", $message));
        } else {
            Yii::$app->session->setFlash("error", Yii::t("app", "An error occurred while changing user's status"));
        }
        
        return $this->redirect(Url::to(["index"]));
    }
    
    public function actionDelete($id)
    {
        $model = $this->__findModel($id);
        
        if ($model === null || $model->deleted === 1) {
            Yii::$app->session->setFlash("error", Yii::t("app", "The requested url could not be found"));
            return $this->redirect(Url::to(["index"]));
        }
        
        $model->deleted = 1;
        
        if ($model->save()) {
            Yii::$app->session->setFlash("success", Yii::t("app", "User deleted successfully"));
        } else {
            Yii::$app->session->setFlash("error", Yii::t("app", "An error occurred while deleting user"));
        }
        
        return $this->redirect(Url::to(["index"]));
    }
    
    private function __findModel($id)
    {
        return Users::findOne($id);
    }
}