<?php

use yii\helpers\Html;


$this->title = 'Create Package';

?>
<div class="customer-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dynamicModel' => $dynamicModel,
        'products' => $products,
        'customers' => $customers,
        'packageMealDetails' => $packageMealDetails,
        'packageDateList' => [],
        'meals' => $meals,
    ]) ?>

</div>
