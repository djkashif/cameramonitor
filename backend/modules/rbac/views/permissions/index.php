<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t("app", "Permissions");
$this->params["headerButtons"][] = Html::a(Yii::t("app", "Advance Search"), "javascript:;", ["class" => "btn btn-primary", "data" => ["toggle" => "modal", "target" => "#searchModal"]]);
$this->params["headerButtons"][] = Html::a(Yii::t("app", "Create"), Url::to(["create"]), ["class" => "btn btn-primary"]);

echo $this->render("_search", [
    "searchModel" => $searchModel
]);

echo GridView::widget([
    "layout" => "
        <div class=\"pull-right\">{pager}</div>
        {summary}{items}
    ",
    "dataProvider" => $dataProvider,
    "columns" => [
        [
            "class" => "yii\grid\CheckboxColumn",
            "headerOptions" => ["class" => "text-center", "style" => "width: 3%"],
            "contentOptions" => ["class" => "text-center"]
        ],
        "name"
    ]
]);