<?php

namespace backend\components;

use common\models\Customer;
use common\models\Transactions;
use common\models\WeightHistory;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class CustomerManager
{
    public static $genderTitles = [
        1 => 'Male',
        2 => 'Female'
    ];

    public static $activityLevelsLabel = [
        '1' => [
            'id' => 1,
            'title' => 'Sedantary',
            'description' => 'Not much meal daily activity, little to no exercise',
        ],
        '2' => [
            'id' => 2,
            'title' => 'Light active',
            'description' => 'Daytime walking, 1 - 3 hours a week of light exercise',
        ],
        '3' => [
            'id' => 3,
            'title' => 'Moderately active',
            'description' => 'Active day job. Exercise 3 - 5 hours a week',
        ],
        '4' => [
            'id' => 4,
            'title' => 'Very active',
            'description' => 'Intense exercise 6 - 7 days a week',
        ],
        '5' => [
            'id' => 5,
            'title' => 'Extremely active',
            'description' => 'Training twice a day',
        ]
    ];

    public static $goals = [
        1 => 'Lose weight',
        2 => 'Maintain weight',
        3 => 'Gain Mass'
    ];

    public static function saveWeightHistory($customer){
        $weightHistory  = WeightHistory::find()->where(['customer_id' => $customer->id, 'date' => date('Y-m-d')])->one();
        if(!$weightHistory ){
            $weightHistory = new WeightHistory();
        }

        $weightHistory->customer_id = $customer->id;
        $weightHistory->weight = $customer->weight;
        $weightHistory->date = date('Y-m-d');
        $weightHistory->save();
    }
}

