<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "weight_history".
 *
 * @property int $id
 * @property int $customer_id
 * @property double $weight
 * @property string $date
 * @property string $createdOn
 * @property string $modifiedOn
 */
class WeightHistory extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'weight_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'date'], 'required'],
            [['customer_id'], 'integer'],
            [['weight'], 'number'],
            [['date', 'createdOn', 'modifiedOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'weight' => 'Weight',
            'date' => 'Date',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }
}
