<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <!-- form start -->
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">

                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($userModel, 'firstName')->textInput(['maxlength' => true, 'placeholder' => 'First Name']) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($userModel, 'lastName')->textInput(['maxlength' => true, 'placeholder' => 'Last Name']) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($customerModel, 'fatherName')->textInput(['maxlength' => true, 'placeholder' => 'Father Name']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($userModel, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($dynamicModel, 'password')->textInput(['maxlength' => true, 'placeholder' => 'Password']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($customerModel, 'cnic')->textInput(['maxlength' => true, 'placeholder' => 'CNIC']) ?>

                    </div>
                    <div class="col-md-6">
                        <?= $form->field($customerModel, 'contactNumber')->textInput(['maxlength' => true, 'placeholder' => 'Contact']) ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($customerModel, 'address')->textInput(['maxlength' => true, 'placeholder' => 'Address']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($customerModel, 'weight')->textInput(['type' => 'number', 'maxlength' => true, 'placeholder' => 'Weight']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($customerModel, 'accountNumber')->textInput(['maxlength' => true, 'placeholder' => 'Account Number']) ?>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">

                        <?=
                        $form->field($dynamicModel, 'country_id')->widget(\kartik\depdrop\DepDrop::classname(), [
                            'type' => \kartik\depdrop\DepDrop::TYPE_SELECT2,
                            'data' => $countryList,
                            'options' => ['id' => 'country-id', 'placeholder' => 'Select ...'],
                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                            'pluginOptions' => [
                                'depends' => ['abcdef'],
                                'url' => \yii\helpers\Url::to(['/customers/']),


                            ]

                        ])->label('Country');

                        ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::hiddenInput('state_id', $dynamicModel->state_id, ['id' => 'state_id_pre']); ?>
                        <?=
                        $form->field($dynamicModel, 'state_id')->widget(\kartik\depdrop\DepDrop::classname(), [
                            'type' => \kartik\depdrop\DepDrop::TYPE_SELECT2,
                            //'value' => $dynamicModel->state_id,
                            'data' => [],
                            'options' => ['id' => 'state-id', 'placeholder' => 'Select ...'],
                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                            'pluginOptions' => [
                                'depends' => ['country-id'],
                                'url' => \yii\helpers\Url::to(['/customers/country-states']),
                                'params' => ['state_id_pre']

                            ]
                        ])->label('State');

                        ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::hiddenInput('city_id', $customerModel->city_id, ['id' => 'city_id_pre']); ?>
                        <?=
                        $form->field($customerModel, 'city_id')->widget(\kartik\depdrop\DepDrop::classname(), [
                            'type' => \kartik\depdrop\DepDrop::TYPE_SELECT2,
                            'data' => [],
                            'options' => ['id' => 'city-id', 'placeholder' => 'Select ...'],
                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                            'pluginOptions' => [
                                'depends' => ['state-id'],
                                'url' => \yii\helpers\Url::to(['/customers/state-cities']),
                                'params' => ['city_id_pre']

                            ]
                        ])->label('City');

                        ?>
                    </div>

                </div>

                <div class="box-footer">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
        <!-- /.box -->
    </div>
    <!--/.col (left) -->

    <div class="col-md-12">
        <h3>Weight History</h3>
    </div>
    <?php
    if(!empty($customerModel->weightHistory)){
        ?>
        <div class="col-md-12">
            <ul class="timeline">
                <?php
                foreach($customerModel->weightHistory as $weightHistory){
                    ?>
                    <!-- timeline time label -->
                    <li class="time-label">
                  <span class="bg-red">
                    <?= date('d M. Y', strtotime($weightHistory->date)) ?>
                  </span>
                    </li>
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <li>
                        <i class="fa fa-balance-scale bg-blue"></i>

                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> <?= (!empty($weightHistory->modifiedOn)) ? date('h:i a', strtotime($weightHistory->modifiedOn)) : date('h:i a', strtotime($weightHistory->createdOn)) ?></span>

                            <h3 class="timeline-header">Weight : <?= $weightHistory->weight ?></h3>


                        </div>
                    </li>
                    <!-- END timeline item -->
                    <?php
                }
                ?>
            </ul>
        </div>
        <?php

    }else{
        ?>
        <div class="col-md-12">
            <h5>No history found...</h5>
        </div>
        <?php
    }
    ?>

</div>

<?php
$this->registerJs("
        $('[id=\"country-id\"]').trigger('depdrop:change');
        
");

?>
