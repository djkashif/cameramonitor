<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Shopper's Insight</h1>
                <h6>Sensor Status Live View</h6>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">

        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>150</h3>

                        <p>Total Devices</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-camera"></i>
                    </div>

                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>100</h3>

                        <p>Up</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-thumbs-up"></i>
                    </div>

                </div>
            </div>
            <!-- ./col -->

            <!-- ./col -->
            <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3>50</h3>

                        <p>Down</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-thumbs-down"></i>
                    </div>

                </div>
            </div>
            <!-- ./col -->
        </div>

        <div class="row">

            <!-- /.col-md-6 -->

            <div class="col-lg-12">


                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="m-0">Sensors</h5>
                    </div>
                    <div class="card-body">

                        <div class="card-body table-responsive p-0" style="height: auto; overflow-x: visible;">
                            <table class="table table-head-fixed text-nowrap table-bordered no-topbottom-border">
                                <thead>
                                <tr>
                                    <th>Up since (3 hours)</th>
                                    <th>Up</th>
                                    <th>Down Since (2 Hours)</th>
                                    <th>Down Since (6 hours)</th>
                                    <th>Down Since (6 Days)</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr >
                                    <td style="border-top: none;">
                                        <div class="card bg-gradient-success collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Bahawalpur</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 1
                                                <br>
                                                <i class="fa fa-link"></i> Up Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-success collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Multan</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 123
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(60 min)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-warning collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Multan</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 123
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(60 min)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-maroon collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Lahore</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 15
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-danger collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Lahore</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 15
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                </tr>


                                <tr >
                                    <td style="border-top: none;">
                                        <div class="card bg-gradient-success collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Bahawalpur</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 1
                                                <br>
                                                <i class="fa fa-link"></i> Up Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-success collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Multan</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 123
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(60 min)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-warning collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Multan</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 123
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(60 min)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-maroon collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Lahore</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 15
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-danger collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Lahore</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 15
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                </tr>


                                <tr >
                                    <td style="border-top: none;">
                                        <div class="card bg-gradient-success collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Bahawalpur</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 1
                                                <br>
                                                <i class="fa fa-link"></i> Up Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-success collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Multan</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 123
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(60 min)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-warning collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Multan</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 123
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(60 min)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-maroon collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Lahore</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 15
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-danger collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Lahore</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 15
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                </tr>


                                <tr >
                                    <td style="border-top: none;">
                                        <div class="card bg-gradient-success collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Bahawalpur</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 1
                                                <br>
                                                <i class="fa fa-link"></i> Up Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-success collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Multan</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 123
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(60 min)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-warning collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Multan</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 123
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(60 min)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-maroon collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Lahore</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 15
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                    <td>
                                        <div class="card bg-gradient-danger collapsed-card" style="width: 200px; margin-bottom: 0px;">
                                            <div class="card-header">
                                                <h3 class="card-title">Lahore</h3>

                                                <div class="card-tools">
                                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <i class="fa fa-server"></i> 15
                                                <br>
                                                <i class="fa fa-link"></i> Down Since <span>(3 Hours)</span>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>





                    </div>
                </div>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->