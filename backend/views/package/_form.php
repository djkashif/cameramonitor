<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>
<style>
    li.select2-selection__choice.selected {
        background-color: #00a65a !important;
        border-color: #204d74  !important;;
        color: #FFF  !important;;
        background-image : none  !important;
    }

    li.select2-selection__choice.permanent-selected {
        background-color: #ef3434 !important;
        border-color: #204d74  !important;;
        color: #FFF  !important;;
        background-image : none  !important;
    }
</style>
<div class="row">
    <!-- left column -->
    <div class="col-md-3">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Create Package</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">


                <?=
                $form->field($dynamicModel, 'customer_id')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $customers,
                    'options' => ['placeholder' => 'Select Customer ...', 'multiple' => false],

                ])->label('Customer');

                ?>

                <?= $form->field($model, 'package_title')->textInput(['maxlength' => true, 'placeholder' => 'Title']) ?>

                <?=
                $form->field($dynamicModel, 'package_meals')->widget(\kartik\select2\Select2::classname(), [
                    'data' => $meals,
                    'options' => ['placeholder' => 'Select Meals ...', 'multiple' => true],
                    'pluginOptions' => [
                        'tags' => true,
                        'tokenSeparators' => [','],
                        'maximumInputLength' => 10
                    ],
                ])->label('Meals');

                ?>
<?php // var_dump(empty($model->actualStartDate));  die;?>

                <?php $model->actualStartDate = (empty($model->actualStartDate)) ? date('Y-m-d') : $model->actualStartDate; ?>
                <?=
                $form->field($model, 'package_start_date')->widget(\kartik\date\DatePicker::classname(), [

                    'options' => ['placeholder' => 'Enter start date ...','value'=>$model->actualStartDate],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
                ?>
                <?php
                if(empty($model->id)){
                    echo $form->field($model, 'package_duration')->textInput(['maxlength' => true, 'placeholder' => 'Duration in days']);
                }else{
                    ?>
                    <div class="form-group">
                        <a class="btn btn-app">
                            <span class="badge bg-green">Expiry Date</span>
                            <i class="fa fa-expeditedssl"></i> <?= date('M d, Y', strtotime($model->actualExpirationDate)) ?>
                        </a>
                        <a class="btn btn-app">
                            <span class="badge bg-green">Duration (in days)</span>
                            <i class="fa fa-gg-circle"></i> <?= $model->package_duration ?>
                        </a>
                    </div>

                    <?php
                    echo $form->field($dynamicModel, 'extend_package_duration')->textInput(['maxlength' => true, 'placeholder' => 'Extend Duration in days']);
                }
                ?>


                <?=
                $form->field($model, 'package_status')->widget(\kartik\select2\Select2::classname(), [
                    'value' => (empty($model->package_status)) ? 1 : $model->package_status,
                    'data' => [
                        1 => 'Activated',
                        2 => 'On Hold',
                        3 => 'Deactivated',
                    ],
                    'options' => ['multiple' => false],

                ])->label('Package Status');

                ?>

                <div class="box-footer">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
        <!-- /.box -->
    </div>
    <!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-9">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Meal Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php $form = ActiveForm::begin(['action' => '/package/meals']); ?>
            <input type="hidden" value="<?= $model->id ?>" name="package">
            <div class="box-body no-padding">
                <table class="table table-striped meals-table">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Date</th>
                        <?php
                        if (!empty($model->meals)) {
                            foreach ($model->meals as $meal) {

                                ?>
                                <th>
                                    <?= $meal->mealDetail->meal_title ?>
                                </th>
                                <?php
                            }
                        }
                        ?>
                        <th style="width: 40px">Action</th>
                    </tr>
                    <?php

                    foreach ($packageDateList as $indexPkgDate => $packageDate): ?>
                        <tr>
                            <td><?= ($indexPkgDate + 1) ?>.</td>
                            <td><?= $packageDate ?></td>
                            <?php
                            if (!empty($model->meals)) {
                                foreach ($model->meals as $meal) {
                                    ?>
                                    <td>
                                        <?php
                                        echo Html::hiddenInput('package_date_meals[selected_products][' . $packageDate . '][' . $meal->meal_id . ']', (isset($packageMealDetails['selected_products'][$packageDate][$meal->meal_id])) ? implode(',', $packageMealDetails['selected_products'][$packageDate][$meal->meal_id]) : '', [
                                                'class' => 'selected_items_input'
                                        ]);
                                        echo Html::hiddenInput('package_date_meals[permanent_products][' . $packageDate . '][' . $meal->meal_id . ']', (isset($packageMealDetails['permanent_products'][$packageDate][$meal->meal_id])) ? implode(',', $packageMealDetails['permanent_products'][$packageDate][$meal->meal_id]) : '', [
                                                'class' => 'permanent_items_input'
                                        ]);

                                        echo \kartik\select2\Select2::widget([
                                            'name' => 'package_date_meals[products][' . $packageDate . '][' . $meal->meal_id . ']',
                                            'value' => (isset($packageMealDetails['products'][$packageDate][$meal->meal_id])) ? $packageMealDetails['products'][$packageDate][$meal->meal_id] : '',
                                            'data' => $products,
                                            'options' => [
                                                'multiple' => true,
                                                'placeholder' => 'Select item',
                                                'allowClear'=>false,
                                                'dropdownAutoWidth'=>true,
                                                //'options' => $productsOptions

                                            ],
                                            'pluginEvents' => [
                                                "change" => "function() { setOnChange(this); setPermanentOnChange(this); }",
                                            ]
                                        ]);
                                        ?>
                                    </td>
                                    <?php
                                }
                            }
                            ?>
                            <td>
                                <select name="package_date_meals[status][<?= $packageDate ?>]" id="">
                                    <option value="1" <?= (isset($packageMealDetails['status'][$packageDate]) && $packageMealDetails['status'][$packageDate] == 1) ? 'selected="selected"' : '' ?>>
                                        Active
                                    </option>
                                    <option value="2" <?= (isset($packageMealDetails['status'][$packageDate]) && $packageMealDetails['status'][$packageDate] == 2) ? 'selected="selected"' : '' ?>>
                                        Inactive
                                    </option>
                                </select>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-default">Reset</button>
                <?= Html::submitButton('Save', ['class' => 'btn btn-info pull-right']) ?>

            </div>
            <!-- /.box-footer -->
            <?php ActiveForm::end(); ?>
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
</div>
<script>
    function setSelectedMealsValue(obj){


        var inputName = $(obj).closest('td').find('input[type="hidden"].selected_items_input').attr('name')
        var itemsArray ={};
        $(obj).closest('td').find('select').find('option:selected').each(function(i, v){
            itemsArray[$(v).text().trim()] = $(v).val();
        });

        var selectedItemsArray = [];
        $(obj).closest('td').find('li.selected').each(function(i, innerObj){
            var itemTitle = $(innerObj).text().replace(/[^a-z0-9\s]/gi, '').trim()
            console.log(itemTitle)
            console.log(itemsArray[itemTitle])
            if(itemsArray[itemTitle]){
                selectedItemsArray.push(itemsArray[itemTitle]);
            }
        })
        $('input[name="'+inputName+'"]').val(selectedItemsArray.join(','));
    }

    function setPreSelectedMealsValues(){
        $('input.selected_items_input').each(function(i, ele){
            setSinglePreSelectedMealsValues(ele);
        });
        $('input.permanent_items_input').each(function(i, ele){
            setSinglePrePermanentSelectedMealsValues(ele);
        });
    }
    function setSinglePreSelectedMealsValues(ele, isFromChange){
        isFromChange = (typeof  isFromChange !== 'undefined') ? isFromChange : false;
        //alert('OK')
        if($(ele).val().trim() != '' || isFromChange){
            var itemsArray = [];

            $(ele).closest('td').find('select').find('option:selected').each(function(i, v){
                itemsArray[$(v).text().trim()] = $(v).val();
            });

            if(isFromChange){

            }
            var values = $(ele).val().trim().split(',');





            $(ele).closest('td').find('li.select2-selection__choice').each(function(i, tagObject){
                var itemTitle = $(tagObject).attr('title').trim();
                if(itemsArray[itemTitle] && $.inArray(itemsArray[itemTitle], values) > -1){
                    $(tagObject).addClass('selected')
                }
            })
        }
    }
    function setSinglePrePermanentSelectedMealsValues(ele, isFromChange){
        isFromChange = (typeof  isFromChange !== 'undefined') ? isFromChange : false;
        //alert('OK')
        if($(ele).val().trim() != '' || isFromChange){
            var itemsArray = [];

            $(ele).closest('td').find('select').find('option:selected').each(function(i, v){
                itemsArray[$(v).text().trim()] = $(v).val();
            });

            if(isFromChange){

            }
            var values = $(ele).val().trim().split(',');





            $(ele).closest('td').find('li.select2-selection__choice').each(function(i, tagObject){
                var itemTitle = $(tagObject).attr('title').trim();
                if(itemsArray[itemTitle] && $.inArray(itemsArray[itemTitle], values) > -1){
                    $(tagObject).addClass('permanent-selected')
                }
            })
        }
    }

    function setOnChange(obj){
        var hdnObj = $(obj).closest('td').find('input.selected_items_input');
        var itemsArray ={};
        var itemsArrayWithValueKey ={};
        var itemsArrayValues =[];
        $(obj).closest('td').find('select').find('option:selected').each(function(i, v){
            itemsArray[$(v).text()] = $(v).val();
            itemsArrayWithValueKey[$(v).val()] = $(v).text();
            itemsArrayValues.push($(v).val());
        });


        var preOptionalSelectedValues = hdnObj.val().split(',');
        var commonValues = itemsArrayValues.filter(value => -1 !== preOptionalSelectedValues.indexOf(value))
        $.each(commonValues, function(i, v){
            $(obj).closest('td').find('li.select2-selection__choice[title="'+itemsArrayWithValueKey[v]+'"]').click();
        });
        console.log(commonValues)

        /*var selectedItemsArray = [];
        $(obj).closest('td').find('li.selected').each(function(i, innerObj){
            var itemTitle = $(innerObj).text().replace(/[^a-z0-9\s]/gi, '').trim()
            console.log(itemTitle)
            console.log(itemsArray[itemTitle])
            if(itemsArray[itemTitle]){
                selectedItemsArray.push(itemsArray[itemTitle]);
            }
        });
        //hdnObj.val(itemsArrayValues.join(','))
        console.log(itemsArrayValues);*/



        //setSinglePreSelectedMealsValues(hdnObj, true);
    }
    function setPermanentOnChange(obj){
        var hdnObj = $(obj).closest('td').find('input.permanent_items_input');
        var itemsArray ={};
        var itemsArrayWithValueKey ={};
        var itemsArrayValues =[];
        $(obj).closest('td').find('select').find('option:selected').each(function(i, v){
            itemsArray[$(v).text()] = $(v).val();
            itemsArrayWithValueKey[$(v).val()] = $(v).text();
            itemsArrayValues.push($(v).val());
        });


        var preOptionalSelectedValues = hdnObj.val().split(',');
        var commonValues = itemsArrayValues.filter(value => -1 !== preOptionalSelectedValues.indexOf(value))
        $.each(commonValues, function(i, v){
            $(obj).closest('td').find('li.select2-selection__choice[title="'+itemsArrayWithValueKey[v]+'"]').addClass('permanent-selected');
        });
        console.log(commonValues)

        /*var selectedItemsArray = [];
        $(obj).closest('td').find('li.selected').each(function(i, innerObj){
            var itemTitle = $(innerObj).text().replace(/[^a-z0-9\s]/gi, '').trim()
            console.log(itemTitle)
            console.log(itemsArray[itemTitle])
            if(itemsArray[itemTitle]){
                selectedItemsArray.push(itemsArray[itemTitle]);
            }
        });
        //hdnObj.val(itemsArrayValues.join(','))
        console.log(itemsArrayValues);*/



        //setSinglePreSelectedMealsValues(hdnObj, true);
    }
</script>
<?php
$this->registerJs("$('a.sidebar-toggle').click();");
$this->registerJs("
   $( \".meals-table\" ).on( \"click\", \".select2-selection__choice\", function() {
        if($(this).hasClass('permanent-selected')){
            return false;
         }
        $(this).toggleClass('selected')
        setSelectedMealsValue(this);
    });
    setPreSelectedMealsValues();
    
    
    
");
//$this->registerJs("swal('Deleted!','Your file has been deleted.','confirm')");
?>



