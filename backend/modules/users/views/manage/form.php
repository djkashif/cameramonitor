<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

if (!$isUpdate) {
    $this->title = Yii::t("app", "Create User");
} else {
    $this->title = Yii::t("app", "Update User: {$model->firstName} {$model->lastName}");
}

?>
<div class="box">
    <?php $form = ActiveForm::begin(); ?>
        <div class="box-body with-border">
            <div class="row">
                <div class="col-md-4">
                    <?php echo $form->field($model, "firstName")->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->field($model, "lastName")->textInput(); ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->field($model, "email")->textInput(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?php
                        $roleAttributes = [];
                        
                        if ($isUpdate) {
                            $roleAttributes["disabled"] = true;
                        }
                    
                        echo $form->field($rolesModel, "role")->dropDownList($rolesList, $roleAttributes);
                    ?>
                </div>
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'city_id')->widget(\kartik\select2\Select2::classname(), [
                        'data' => $cityList,
                        'options' => ['placeholder' => 'Select Customer ...', 'multiple' => false],

                    ])->label('City');

                    ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <?php echo Html::a(Yii::t("app", "Back"), Url::to(["index"]), ["class" => "btn btn-default"]); ?>
            <?php echo Html::submitButton(Yii::t("app", "Submit"), ["class" => "btn btn-primary pull-right"]); ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>