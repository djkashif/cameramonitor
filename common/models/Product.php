<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $product_title
 * @property string $product_description
 * @property int $unit_measurement_qty
 * @property string $uom
 * @property string $fat
 * @property string $carbohydrates
 * @property string $protiens
 * @property int $calories
 * @property string $barcode
 * @property double $price
 * @property int $status
 * @property string $createdOn
 * @property string $modifiedOn
 */
class Product extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_title', 'product_description', 'uom', 'fat', 'carbohydrates', 'protiens', 'price'], 'required'],
            [['product_description'], 'string'],
            [['unit_measurement_qty', 'calories', 'status'], 'integer'],
            [['price'], 'number'],
            [['createdOn', 'modifiedOn'], 'safe'],
            [['product_title', 'barcode'], 'string', 'max' => 255],
            [['uom', 'fat', 'carbohydrates', 'protiens'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_title' => 'Product Title',
            'product_description' => 'Product Description',
            'unit_measurement_qty' => 'Unit Measurement Qty',
            'uom' => 'Uom',
            'fat' => 'Fat',
            'carbohydrates' => 'Carbohydrates',
            'protiens' => 'Protiens',
            'calories' => 'Calories',
            'barcode' => 'Barcode',
            'price' => 'Price',
            'status' => 'Status',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }
}
