<?php
namespace common\models;

use common\models\ActiveRecord;


class Users extends ActiveRecord
{
    public static function tableName()
    {
        return "{{%users}}";
    }

    public function rules()
    {
        return [
            [["firstName", "lastName", "email", "password"], "required"],
            [["createdOn", "modifiedOn"], "safe"],
            [["firstName", "lastName"], "string", "max" => 100],
            [["email", "password", "token"], "string", "max" => 255],
            [["active", "deleted"], "integer"],
            [["email"], "unique"],
            ["email", "email"]
        ];
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['token' => $token, 'active' => 1]);
    }

    public function getCustomer(){
        die('Call');
    }
}