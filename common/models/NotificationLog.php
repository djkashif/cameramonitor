<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notification_log".
 *
 * @property int $id
 * @property int $customer_id
 * @property string $notification
 * @property int $status
 * @property int $read_status
 * @property string $response
 * @property string $createdOn
 * @property string $modifiedOn
 */
class NotificationLog extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'notification'], 'required'],
            [['customer_id', 'status', 'read_status'], 'integer'],
            [['notification'], 'string'],
            [['createdOn', 'modifiedOn'], 'safe'],
            [['response'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'notification' => 'Notification',
            'status' => 'Status',
            'read_status' => 'Read Status',
            'response' => 'Response',
            'createdOn' => 'Created On',
            'modifiedOn' => 'Modified On',
        ];
    }

    public function getCustomer(){
        return $this->hasOne(Customer::className(), ["id" => "customer_id"]);
    }
}
