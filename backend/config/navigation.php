<?php
$adminMenu = [
    [
        "title" => Yii::t("app", "Dashboard"),
        "fontawesomeIcon" => "dashboard",
        "controller" => "dashboard",
        "module" => "",
        "action" => "",
        "permissions" => [
            'viewDashboard'
        ]
    ],
    [
        "title" => Yii::t("app", "Users"),
        "fontawesomeIcon" => "user",
        "module" => "users",
        "controller" => "manage",
        "action" => "index",
        "permissions" => [
            'viewUsers',
            'updateUsers',
            'deleteUsers',
            'createUsers',
            'changeUsersStatus',
        ]
    ],
    [
        "title" => Yii::t("app", "RBAC"),
        "fontawesomeIcon" => "universal-access",
        "module" => "rbac",
        "children" => [
            [
                "title" => Yii::t("app", "Roles"),
                "module" => "rbac",
                "controller" => "roles",
                "action" => "index",
                "permissions" => [
                    "Admin"
                ]
            ],
            [
                "title" => Yii::t("app", "Permissions"),
                "module" => "rbac",
                "controller" => "permissions",
                "action" => "index",
                "permissions" => [
                    "Admin"
                ]
            ]
        ],
        "permissions" => [
            "Admin"
        ]
    ],
    [
        "title" => Yii::t("app", "Customers"),
        "fontawesomeIcon" => "user",
        "controller" => "customers",
        "module" => "",
        "action" => "index",
        "permissions" => [
            "viewCustomers",
            "viewCustomerDashboard",
            "updateCustomer",
            "deleteCustomer",
            "createCustomer",
        ]
    ],
    [
        "title" => Yii::t("app", "Products"),
        "fontawesomeIcon" => "cube",
        "controller" => "product",
        "module" => "",
        "action" => "index",
        "permissions" => [
            "viewProducts",
            "updateProduct",
            "printProductLabels",
            "deleteProduct",
            "createProduct",
        ]
    ],
    [
        "title" => Yii::t("app", "Packages"),
        "fontawesomeIcon" => "cubes",
        "module" => "",
        "children" => [
            [
                "title" => Yii::t("app", "Create Package"),
                "module" => "",
                "controller" => "package",
                "action" => "create",
                "permissions" => [
                    "createPackage"
                ]
            ],
            [
                "title" => Yii::t("app", "View All Packages"),
                "module" => "",
                "controller" => "package",
                "action" => "index",
                "permissions" => [
                    "viewPackages"
                ]
            ],
            [
                "title" => Yii::t("app", "Bulk Update"),
                "module" => "",
                "controller" => "package",
                "action" => "bulk-update",
                "permissions" => [
                    "bulkUpdatePackage"
                ]
            ],
            [
                "title" => Yii::t("app", "Generate Packing List"),
                "module" => "",
                "controller" => "package",
                "action" => "packing-list",
                "permissions" => [
                    "packingList"
                ]
            ]
        ],
        "permissions" => [
            "viewPackages",
            "updatePackage",
            "exportPackage",
            "deletePackage",
            "createPackage",
            "bulkUpdatePackage",

        ]
    ],

    [
        "title" => Yii::t("app", "Labels"),
        "fontawesomeIcon" => "barcode",
        "module" => "",
        "children" => [
            [
                "title" => Yii::t("app", "Print Labels"),
                "fontawesomeIcon" => "print",
                "module" => "",
                "controller" => "barcode",
                "action" => "index",
                "permissions" => [
                    "printLabels",
                ]
            ]
        ],
        "permissions" => [
            "printLabels",
            "labels",
        ]
    ],
    [
        "title" => Yii::t("app", "Orders"),
        "fontawesomeIcon" => "reorder",
        "module" => "",
        "children" => [
            [
                "title" => Yii::t("app", "Generate Order"),
                "fontawesomeIcon" => "plus-square",
                "module" => "",
                "controller" => "order",
                "action" => "generate-order",
                "permissions" => [
                    "orderGenerate"
                ]
            ],
            /*[
                "title" => Yii::t("app", "Process New Order"),
                "fontawesomeIcon" => "calculator",
                "module" => "",
                "controller" => "order",
                "action" => "process-new-orders",
                "permissions" => [
                    "orderProcessingOld"
                ]
            ],*/
            [
                "title" => Yii::t("app", "Process Orders"),
                "fontawesomeIcon" => "calculator",
                "module" => "",
                "controller" => "order",
                "action" => "process-generated-orders",
                "permissions" => [
                    "processingGeneratedOrders"
                ]
            ],
            [
                "title" => Yii::t("app", "View Orders"),
                "fontawesomeIcon" => "reorder",
                "module" => "",
                "controller" => "order",
                "action" => "index",
                "permissions" => [
                    "viewOrders"
                ]
            ]
        ],
        "permissions" => [
            "orders",
            "viewOrders",
            "orderProcessing",
            "orderGenerate",
            "processingGeneratedOrders",
        ]
    ],
    [
        "title" => Yii::t("app", "Customer Support"),
        "fontawesomeIcon" => "question-circle ",
        "module" => "",
        "children" => [
            [
                "title" => Yii::t("app", "View Tickets"),
                "fontawesomeIcon" => "ticket",
                "module" => "",
                "controller" => "customer-support",
                "action" => "index",
                "permissions" => [
                    "viewCustomerSupportTickets",
                ]
            ]
        ],
        "permissions" => [
            "customerSupport",
            "viewCustomerSupportTickets",
            "deleteCustomerSupportTicket",
            "viewCustomerSupportTicketDetails",
        ]
    ],
    [
        "title" => Yii::t("app", "Notifications"),
        "fontawesomeIcon" => "comments",
        "module" => "",
        "children" => [
            [
                "title" => Yii::t("app", "New Template"),
                "fontawesomeIcon" => "plus-square",
                "module" => "",
                "controller" => "notifications",
                "action" => "create",
                "permissions" => [
                    "createNotificationTemplate",
                ]
            ],
            [
                "title" => Yii::t("app", "View All Templates"),
                "fontawesomeIcon" => "eye",
                "module" => "",
                "controller" => "notifications",
                "action" => "index",
                "permissions" => [
                    "viewAllTemplates",
                ]
            ],
            [
                "title" => Yii::t("app", "Send Realtime Notifications"),
                "fontawesomeIcon" => "paper-plane-o",
                "module" => "",
                "controller" => "notifications",
                "action" => "realtime-notification",
                "permissions" => [
                    "sendNotification",
                ]
            ],
            [
                "title" => Yii::t("app", "Notifications Log"),
                "fontawesomeIcon" => "history",
                "module" => "",
                "controller" => "notifications",
                "action" => "notification-log",
                "permissions" => [
                    "notificationsLog",
                ]
            ]
        ],
        "permissions" => [
            "createNotificationTemplate",
            "deleteNotificationTemplate",
            "updateNotificationTemplate",
            "viewAllTemplates",
            "viewTemplates",
            "notificationsLog",
            "sendNotification"
        ]
    ],

];

$customerMenu = [
    [
        "title" => Yii::t("app", "Dashboard"),
        "fontawesomeIcon" => "user",
        "module" => "customer",
        "controller" => "default",
        "action" => "index"
    ],
    [
        "title" => Yii::t("app", "My Package"),
        "fontawesomeIcon" => "user",
        "module" => "customer",
        "controller" => "package",
        "action" => "index"
    ],
    [
        "title" => Yii::t("app", "Update Weight"),
        "fontawesomeIcon" => "user",
        "module" => "customer",
        "controller" => "default",
        "action" => "update-weight"
    ],
];

$menu = (
    Yii::$app->user->can('Customer')
) ? $customerMenu :  $adminMenu;


return $menu;