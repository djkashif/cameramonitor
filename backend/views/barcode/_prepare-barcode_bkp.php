<?php for($i=1;$i<=1; $i++): ?>
<?php foreach ($data as $pkgData): ?>
    <div class="container bar-container">
        <div class="row heading-wrapper">
            <span class="heading"><?= $pkgData['package']['package_title'] ?></span>
        </div>
        <div class="row heading-wrapper2">
            <span class="heading">Package Details</span>
        </div>
        <div class="row">
            <div class="">
                <table>
                    <tr style="height: ">
                        <td style="width: 35%; padding: 5px;height: 150px">
                            <table>
                                <tr>
                                    <td><span class="nut-heading">Date: </span></td>
                                    <td><span class="nut-value"><?= $pkgData['package']['date'] ?></span></td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 48%; padding: 5px;">
                            <div class="barcode-image1">

                                <img src="/barcodes/<?= $pkgData['package']['barcode'] ?>"
                                     style="width: 98%;height: 120px;"
                                     alt="">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center;border-top: 2px solid;">
                            <p style="margin: 0;font-size: 24px;font-weight: bold;">
                                Packages Note (If Any)
                            </p>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
    <div style="page-break-after: auto;"></div>


    <!--
    Customer Label
    -->
    <div class="container bar-container">
        <div class="row heading-wrapper">
            <span class="heading"><?= $pkgData['customer']['title'] ?></span>
        </div>
        <div class="row heading-wrapper2">
            <span class="heading">CUSTOMER DETAILS</span>
        </div>
        <div class="row">
            <div class="">
                <table>
                    <tr style="height: ">
                        <td style="width: 35%; padding: 5px;height: 150px">
                            <table>
                                <tr>
                                    <td><span class="nut-heading">Account Number: </span></td>
                                    <td><span class="nut-value"><?= $pkgData['customer']['accountNumber'] ?></span></td>
                                </tr>
                                <tr>
                                    <td><span class="nut-heading">Contact: </span></td>
                                    <td><span class="nut-value"><?= $pkgData['customer']['contactNumber'] ?></span></td>
                                </tr>
                                <!--<tr>
                                    <td><span class="nut-heading">Address: </span></td>
                                    <td><span class="nut-value"><?/*= $pkgData['customer']['address'] */?></span></td>
                                </tr>-->
                                <tr>
                                    <td><span class="nut-heading">Region:</span></td>
                                    <td><span class="nut-value"><?= $pkgData['customer']['city'] ?></span></td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 48%; padding: 5px;">
                            <div class="barcode-image1">

                                <img src="/barcodes/<?= $pkgData['customer']['barcode'] ?>"
                                     style="width: 98%;height: 120px;"
                                     alt="">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center;border-top: 2px solid;">
                            <p style="margin: 0;font-size: 24px;font-weight: bold;">
                                Customer Note (If Any)
                            </p>
                        </td>
                    </tr>
                </table>

            </div>

        </div>
    </div>
    <div style="page-break-after: auto;"></div>


    <?php foreach ($pkgData['products'] as $product): ?>
        <div class="container bar-container">
            <div class="row heading-wrapper">
                <span class="heading"><?= $product['product_title'] ?></span>
            </div>
            <div class="row heading-wrapper2">
                <span class="heading">NUTRITIONAL FACTS</span>
            </div>
            <div class="row">
                <div class="">
                    <table>
                        <tr style="height: ">
                            <td style="width: 35%; padding: 5px;height: 150px">
                                <table>
                                    <tr>
                                        <td><span class="nut-heading">Calories: </span></td>
                                        <td>
                                            <span class="nut-value"><?= (!empty($product['calories'])) ? $product['calories'] : 'N/A' ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="nut-heading">FATS: </span></td>
                                        <td>
                                            <span class="nut-value"><?= (!empty($product['fat'])) ? $product['fat'] : 'N/A' ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="nut-heading">PROTEINS: </span></td>
                                        <td>
                                            <span class="nut-value"><?= (!empty($product['protiens'])) ? $product['protiens'] : 'N/A' ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><span class="nut-heading">CARBS: </span></td>
                                        <td>
                                            <span class="nut-value"><?= (!empty($product['carbohydrates'])) ? $product['carbohydrates'] : 'N/A' ?></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 48%; padding: 5px;">
                                <div class="barcode-image1">

                                    <img src="/barcodes/<?= $product['barcode'] ?>"
                                         style="width: 98%;height: 120px;"
                                         alt="">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;border-top: 2px solid;">
                                <p style="margin: 0;font-size: 24px;font-weight: bold;">
                                    Store in a clean, dry, cool and odorless place.<br>
                                    Use within three days (if stored properly)
                                </p>
                            </td>
                        </tr>
                    </table>

                </div>

            </div>
        </div>
        <div style="page-break-after: auto;"></div>
    <?php endforeach; ?>


<?php endforeach; ?>
<?php endfor; ?>
